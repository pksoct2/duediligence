<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMastercompanysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mastercompanys', function (Blueprint $table) {
            $table->id('master_company_id');
            $table->bigInteger('user_id');
            $table->string('users_name');
            $table->string('pan_income_tax_img');
            $table->string('tan_traces_img');
            $table->string('certificate_of_incorp_img');
            $table->string('memo_of_association_img');
            $table->string('articles_of_association_img');
            $table->string('gst_tax_cert_img');
            $table->string('lut_cert_img');
            $table->string('gst_e_com_cert_img');
            $table->string('prof_tax_enrollment_cert_img');
            $table->string('prof_tax_register_cert_img');
            $table->string('pf_establish_cert_img');
            $table->string('esic_register_cert_img');
            $table->string('shops_establish_cert_trade_licence_img');
            $table->string('iec_img');
            $table->string('msmed_cert_img');
            $table->string('startup_cert_img');
            $table->string('lower_tax_deduction_cert_img');
            $table->string('tax_exemption_cert_us12a_img');
            $table->string('tax_exemption_cert_us80g_img');
            $table->string('any_other_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mastercompanys');
    }
}
