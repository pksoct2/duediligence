<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateROCReturnsTable extends Migration
{
    /**
     * Run the migrations.
     *'company_name',
    'aoc4',
    'mgt7',
    'srn',
    'date_filing',
     * @return void
     */
    public function up()
    {
        Schema::create('r_o_c_returns', function (Blueprint $table) {
            $table->id();
            $table->string('company_name');
            $table->string('aoc4');
            $table->string('mgt7');
            $table->string('srn');
            $table->string('date_filing');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_o_c_returns');
    }
}
