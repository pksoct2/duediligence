<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComplianceTrackerBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compliance_tracker_branches', function (Blueprint $table) {
            $table->id();
            $table->date('parent_company');
            $table->date('branch_name');
            $table->date('select_month_year');
            $table->date('due_date');
            $table->string('category');
            $table->string('details');
            $table->date('compliance_date');
            $table->string('documents');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compliance_tracker_branches');
    }
}
