<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRocCompliancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roc_compliances', function (Blueprint $table) {
            $table->id();
            $table->string('company_name');
            $table->string('type_meeting');
            $table->string('date_meeting');
            $table->string('agenda');
            $table->string('attendance');
            $table->string('remarks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roc_compliances');
    }
}
