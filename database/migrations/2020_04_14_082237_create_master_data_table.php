<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_data', function (Blueprint $table) {
            $table->id();
            $table->string('company_name');
            $table->string('company_prt__llp_individual');
            $table->date('date_incorporation');
            $table->string('pan');
            $table->string('tan');
            $table->string('pf_reg');
            $table->string('esi_reg');
            $table->string('pt_reg');
            $table->string('other_applicable_law_reg');
            $table->string('certificate_incorporation');
            $table->string('aoa');
            $table->string('moa');
            $table->string('cin');
            $table->string('authorized_capital');
            $table->string('paid_up_capital');
            $table->string('reg_office');
            $table->string('objects');
            $table->string('email');
            $table->string('telephone');
            $table->string('director_details');
            $table->string('shareholder_details');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_data');
    }
}
