<?php $__env->startSection('content'); ?>
    <?php $ROCCompliance = 'ROCCompliance';?>
    <div class="content">

        <div class="row justify-content-center">
            <div class="col-lg-8 col-sm-9 col-md-9">

                <div class="panel panel-default">
                    <?php if(session('success')): ?>
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <?php echo e(session('success')); ?>

                        </div>
                    <?php endif; ?>
                    <div class="panel-heading h3 p-2 bg-light font-weight-bold border breadcrumb">
                        ROC Compliances
                    </div>
                    <div class="panel-body p-2">

                        <form action="<?php echo e(route("rocComplianceSubmit")); ?>" method="POST" enctype="multipart/form-data">
                            <?php echo csrf_field(); ?>
                            <div class="form-group <?php echo e($errors->has('type_meeting') ? 'has-error' : ''); ?>">
                                <label for="type_meeting">Types of Meeting*</label>
                                <input type="text" id="type_meeting" name="type_meeting" class="form-control" value="<?php echo e(old('type_meeting', isset($user) ? $user->type_meeting : '')); ?>" placeholder="Enter Types of Meeting*" required>
                                <?php if($errors->has('type_meeting')): ?>
                                    <p class="help-block">
                                        <?php echo e($errors->first('type_meeting')); ?>

                                    </p>
                                <?php endif; ?>
                                <p class="helper-block">
                                    <?php echo e(trans('cruds.user.fields.name_helper')); ?>

                                </p>
                            </div>

                            <div class="form-group <?php echo e($errors->has('date_meeting') ? 'has-error' : ''); ?>">
                                <label for="date_meeting">Date of Meeting*</label>
                                <input type="date" id="date_meeting" name="date_meeting" class="form-control" value="<?php echo e(old('date_meeting', isset($user) ? $user->date_meeting : '')); ?>" placeholder="Enter Date of Meeting*" required>
                                <?php if($errors->has('date_meeting')): ?>
                                    <p class="help-block">
                                        <?php echo e($errors->first('date_meeting')); ?>

                                    </p>
                                <?php endif; ?>
                                <p class="helper-block">
                                    <?php echo e(trans('cruds.user.fields.email_helper')); ?>

                                </p>
                            </div>

                            <div class="form-group <?php echo e($errors->has('agenda') ? 'has-error' : ''); ?>">
                                <label for="agenda">Agenda*</label>
                                <input type="text" id="agenda" name="agenda" class="form-control" value="<?php echo e(old('agenda', isset($user) ? $user->agenda : '')); ?>" placeholder="Enter Agenda*" required>
                                <?php if($errors->has('agenda')): ?>
                                    <p class="help-block">
                                        <?php echo e($errors->first('agenda')); ?>

                                    </p>
                                <?php endif; ?>
                                <p class="helper-block">
                                    <?php echo e(trans('cruds.user.fields.name_helper')); ?>

                                </p>
                            </div>
                            <div class="form-group <?php echo e($errors->has('attendance') ? 'has-error' : ''); ?>">
                                <label for="attendance">Attendance*</label>
                                <input type="text" id="attendance" name="attendance" class="form-control" value="<?php echo e(old('attendance', isset($user) ? $user->attendance : '')); ?>" placeholder="Enter Attendance" required>
                                <?php if($errors->has('attendance')): ?>
                                    <p class="help-block">
                                        <?php echo e($errors->first('attendance')); ?>

                                    </p>
                                <?php endif; ?>
                                <p class="helper-block">
                                    <?php echo e(trans('cruds.user.fields.name_helper')); ?>

                                </p>
                            </div>
                            <div class="form-group <?php echo e($errors->has('remarks') ? 'has-error' : ''); ?>">
                                <label for="remarks">Remarks*</label>
                                <input type="text" id="remarks" name="remarks" class="form-control" value="<?php echo e(old('remarks', isset($user) ? $user->remarks : '')); ?>" placeholder="Enter Remarks*" required>
                                <?php if($errors->has('remarks')): ?>
                                    <p class="help-block">
                                        <?php echo e($errors->first('remarks')); ?>

                                    </p>
                                <?php endif; ?>
                                <p class="helper-block">
                                    <?php echo e(trans('cruds.user.fields.name_helper')); ?>

                                </p>
                            </div>
                            <div>
                                <button class="btn btn-success" type="submit" value="<?php echo e(trans('global.save')); ?>"><?php echo e(trans('global.save')); ?></button>
                            </div>
                        </form>


                    </div>
                </div>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.user', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\devmantra\resources\views/ROCCompliance.blade.php ENDPATH**/ ?>