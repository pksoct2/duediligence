<?php $__env->startSection('content'); ?>
    <?php $rocReturn1 = 'rocReturn';?>
    <div class="content">

        <div class="row justify-content-center">
            <div class="col-lg-8 col-sm-9 col-md-9">

                <div class="panel panel-default">
                    <?php if(session('success')): ?>
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <?php echo e(session('success')); ?>

                        </div>
                    <?php endif; ?>
                    <div class="panel-heading h3 p-2 bg-light font-weight-bold border breadcrumb">
                        ROC Return
                    </div>
                    <div class="panel-body p-2">

                        <form action="<?php echo e(route("rocReturnSubmit")); ?>" method="POST" enctype="multipart/form-data">
                            <?php echo csrf_field(); ?>
                            <div class="form-group <?php echo e($errors->has('aoc4') ? 'has-error' : ''); ?>">
                                <label for="aoc4">AOC4*</label>
                                <input type="text" id="aoc4" name="aoc4" class="form-control" value="<?php echo e(old('aoc4', isset($user) ? $user->aoc4 : '')); ?>" placeholder="Enter AOC4*" required>
                                <?php if($errors->has('aoc4')): ?>
                                    <p class="help-block">
                                        <?php echo e($errors->first('aoc4')); ?>

                                    </p>
                                <?php endif; ?>
                                <p class="helper-block">
                                    <?php echo e(trans('cruds.user.fields.name_helper')); ?>

                                </p>
                            </div>

                            <div class="form-group <?php echo e($errors->has('mgt7') ? 'has-error' : ''); ?>">
                                <label for="mgt7">MGT7*</label>
                                <input type="text" id="mgt7" name="mgt7" class="form-control" value="<?php echo e(old('mgt7', isset($user) ? $user->mgt7 : '')); ?>" placeholder="Enter MGT7*" required>
                                <?php if($errors->has('mgt7')): ?>
                                    <p class="help-block">
                                        <?php echo e($errors->first('mgt7')); ?>

                                    </p>
                                <?php endif; ?>
                                <p class="helper-block">
                                    <?php echo e(trans('cruds.user.fields.email_helper')); ?>

                                </p>
                            </div>

                            <div class="form-group <?php echo e($errors->has('srn') ? 'has-error' : ''); ?>">
                                <label for="srn">SRN*</label>
                                <input type="text" id="srn" name="srn" class="form-control" value="<?php echo e(old('srn', isset($user) ? $user->srn : '')); ?>" placeholder="Enter SRN*" required>
                                <?php if($errors->has('srn')): ?>
                                    <p class="help-block">
                                        <?php echo e($errors->first('srn')); ?>

                                    </p>
                                <?php endif; ?>
                                <p class="helper-block">
                                    <?php echo e(trans('cruds.user.fields.name_helper')); ?>

                                </p>
                            </div>
                            <div class="form-group <?php echo e($errors->has('date_filing') ? 'has-error' : ''); ?>">
                                <label for="date_filing">Date of Filing*</label>
                                <input type="date" id="date_filing" name="date_filing" class="form-control" value="<?php echo e(old('date_filing', isset($user) ? $user->date_filing : '')); ?>" placeholder="Enter Date of Filing" required>
                                <?php if($errors->has('date_filing')): ?>
                                    <p class="help-block">
                                        <?php echo e($errors->first('date_filing')); ?>

                                    </p>
                                <?php endif; ?>
                                <p class="helper-block">
                                    <?php echo e(trans('cruds.user.fields.name_helper')); ?>

                                </p>
                            </div>
                            <div>
                                <button class="btn btn-success" type="submit" value="<?php echo e(trans('global.save')); ?>"><?php echo e(trans('global.save')); ?></button>
                            </div>
                        </form>


                    </div>
                </div>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.user', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\devmantra\resources\views/rocReturn.blade.php ENDPATH**/ ?>