<?php $__env->startSection('content'); ?>
    <?php $employeeAgreement = 'employeeAgreement';?>
    <div class="content">

        <div class="row justify-content-center">
            <div class="col-lg-8 col-sm-9 col-md-9">

                <div class="panel panel-default">
                    <?php if(session('success')): ?>
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <?php echo e(session('success')); ?>

                        </div>
                    <?php endif; ?>
                    <div class="panel-heading h3 p-2 bg-light font-weight-bold border breadcrumb">
                        Employee Agreements Data File
                    </div>
                    <div class="panel-body p-2">

                        <form action="<?php echo e(route("employeeAgreementSubmit")); ?>" method="POST" enctype="multipart/form-data">
                            <?php echo csrf_field(); ?>


                            <div class="form-group <?php echo e($errors->has('employee_agreement') ? 'has-error' : ''); ?>">

                                <label for="employee_agreement">Employee Agreements*</label>
                                <input id="employee_agreement" type="file" value="" class="form-control <?php $__errorArgs = ['employee_agreement'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="employee_agreement" >
                                <?php if($errors->has('employee_agreement')): ?>
                                    <p class="help-block">
                                        <?php echo e($errors->first('employee_agreement')); ?>

                                    </p>
                                <?php endif; ?>
                                <p class="helper-block">
                                    <?php echo e(trans('cruds.user.fields.email_helper')); ?>

                                </p>
                                <p class="text-danger">Rename_Name_Dateofupload*</p>

                            </div>

                            

                            <div>
                                <button class="btn btn-success" type="submit" value="<?php echo e(trans('global.save')); ?>"><?php echo e(trans('global.save')); ?></button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.user', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\devmantra\resources\views/employeeAgreement.blade.php ENDPATH**/ ?>