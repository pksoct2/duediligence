<?php $__env->startSection('content'); ?>
    <?php $esiChallanFile = 'esiChallanFile';?>
    <div class="content">

        <div class="row justify-content-center">
            <div class="col-lg-8 col-sm-9 col-md-9">

                <div class="panel panel-default">
                    <?php if(session('success')): ?>
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <?php echo e(session('success')); ?>

                        </div>
                    <?php endif; ?>
                    <div class="panel-heading h3 p-2 bg-light font-weight-bold border breadcrumb">
                        Client ESI Challan File
                    </div>
                    <div class="panel-body p-2">

                        <form action="<?php echo e(route("esiChallanSubmit")); ?>" method="POST" enctype="multipart/form-data">
                            <?php echo csrf_field(); ?>

                            <div class="form-group <?php echo e($errors->has('year_month') ? 'has-error' : ''); ?>">
                                <label for="year_month">Year/Month*</label>
                                <input type="month" id="year_month" name="year_month" class="form-control" value="<?php echo e(old('year_month', isset($user) ? $user->year_month : '')); ?>" required>
                                <?php if($errors->has('year_month')): ?>
                                    <p class="help-block">
                                        <?php echo e($errors->first('year_month')); ?>

                                    </p>
                                <?php endif; ?>
                                <p class="helper-block">
                                    <?php echo e(trans('cruds.user.fields.name_helper')); ?>

                                </p>
                            </div>

                            <div class="form-group <?php echo e($errors->has('esi_challan') ? 'has-error' : ''); ?>">

                                <label for="esi_challan">ESI Challan*</label>
                                <input id="esi_challan" type="file" name="esi_challan" value="" class="form-control <?php $__errorArgs = ['esi_challan'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>"  >
                                <?php if($errors->has('esi_challan')): ?>
                                    <p class="help-block">
                                        <?php echo e($errors->first('esi_challan')); ?>

                                    </p>
                                <?php endif; ?>
                                <p class="helper-block">
                                    <?php echo e(trans('cruds.user.fields.email_helper')); ?>

                                </p>
                                <p class="text-danger">Rename File as month_Dateofupload*</p>

                            </div>

                            

                            <div>
                                <button class="btn btn-success" type="submit" value="<?php echo e(trans('global.save')); ?>"><?php echo e(trans('global.save')); ?></button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.user', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\devmantra\resources\views/esiChallanFile.blade.php ENDPATH**/ ?>