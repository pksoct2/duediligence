<style>
    table.dataTable tbody th, table.dataTable tbody td {
        padding: 0px 10px;
    }
</style>

<?php $__env->startSection('content'); ?>
    <?php $payrollFile1 = 'payrollView';?>
    <div class="content">

        <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-default">
                    <div class="panel-heading breadcrumb p-2 font-weight-bold h2 mb-5">
                        Payroll Data List
                    </div>
                    <div class="panel-body">

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover datatable datatable-User">
                                <thead>
                                <tr>
                                    <th width="8">

                                    </th>

                                    <th>
                                        Company Name
                                    </th>
                                    <th>
                                        Month / Year
                                    </th>
                                    <th>
                                        Payroll
                                    </th>
                                    <th>
                                        Edit
                                    </th>


                                </tr>
                                </thead>
                                <tbody>
                                <?php //print_r($clientBranch->branch_id); ?>
                                <?php $__currentLoopData = $payroll; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $userData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr data-entry-id="">
                                        <td class="">

                                        </td>

                                        <td>
                                            <?php echo e($userData->company_name ?? ''); ?>

                                        </td>
                                        <td>
                                            <?php echo e($userData->year_month ?? ''); ?>

                                        </td>

                                        <td>
                                            <button type="button" data-src="/payroll/<?php echo e($userData->payroll); ?>" class="btn btn-outline-secondary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                        </td>

                                        <td class="d-flex flex-row bd-highlight">
                                            <a class="btn btn-outline-primary" href="<?php echo e(route('editPayrollStore', $userData->id)); ?>">
                                                <i class="fa-fw fas fa-edit"></i>
                                            </a>
                                        </td>
                                        
                                        
                                        
                                        
                                        

                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>

                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="modal fade bd-example1-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold text-capitalize text-center" id="exampleModalLabel">User Recent Image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <a id="myAnchor" href="" onclick="openwindow()" download>
                        <img src="" class="newmodalImageAppend img-fluid" height="550px" width="550px">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <script !src="">
        $(".bd-example-modal-lg").on('click', function(e) {
            var self = $(this);
            var imagePath = self.attr("data-src");
            var modal = $(".bd-example1-modal-lg");
            var imageElm = $(".newmodalImageAppend");

            imageElm.attr('src', imagePath);
            modal.modal('show');

        });

    </script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    ##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##

    <script>
        $(function () {
            let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('user_delete')): ?>
            let deleteButtonTrans = '<?php echo e(trans('global.datatables.delete')); ?>'
            let deleteButton = {
                text: deleteButtonTrans,
                url: "<?php echo e(route('destroyUser')); ?>",
                className: 'btn-danger',
                action: function (e, dt, node, config) {
                    var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
                        return $(entry).data('entry-id')
                    });

                    if (ids.length === 0) {
                        alert('<?php echo e(trans('global.datatables.zero_selected')); ?>')

                        return
                    }

                    if (confirm('<?php echo e(trans('global.areYouSure')); ?>')) {
                        $.ajax({
                            headers: {'x-csrf-token': _token},
                            method: 'POST',
                            url: config.url,
                            data: { ids: ids, _method: 'DELETE' }})
                            .done(function () { location.reload() })
                    }
                }
            }
            dtButtons.push(deleteButton)
            <?php endif; ?>

            $.extend(true, $.fn.dataTable.defaults, {
                order: [[ 1, 'desc' ]],
                pageLength: 100,
            });
            $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
                $($.fn.dataTable.tables(true)).DataTable()
                    .columns.adjust();
            });
        })

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.user', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\devmantra\resources\views/payrollView.blade.php ENDPATH**/ ?>