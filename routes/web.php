<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.adminlogin');
})->name('adminlogin');

Auth::routes();

    Route::post('/login', ['uses'=>'UserController@userLogin'])->name('login');
    Route::get('/clientDashboard', ['uses'=>'UserController@clientDashboard'])->name('clientDashboard');
    //Route::get('/home', 'UserController@index')->name('home');
    Route::get('/userSideView', 'UserController@userSideView')->name('userSideView');
    //Route::get('/clientComplianceTracker', 'UserController@clientComplianceTracker')->name('clientComplianceTracker');
    //Route::get('/clientBranch', 'UserController@clientBranch')->name('clientBranch');
    Route::get('footer', 'UserController@quickInfoView');

//    Route::get('/clientBranchMasterCompany/{company}', 'UserController@clientBranchMasterCompany')->name('clientBranchMasterCompany');
//    Route::get('/clientBranchComplianceTracker', 'UserController@clientBranchComplianceTracker')->name('clientBranchComplianceTracker');

    Route::get('/master_Data', 'UserController@master_DataStore')->name('master_Data');
Route::post('/master_Data', 'UserController@master_Data')->name('master_Data');
Route::get('/accountFile', 'UserController@accountFileStore')->name('accountFile');
Route::post('/accountFile', 'UserController@accountFile')->name('accountFile');
Route::get('/masterDataView', 'UserController@masterDataView')->name('masterDataView');
Route::get('/accountView', 'UserController@accountView')->name('accountView');
Route::get('/editMasterData/{id}', 'UserController@editMasterData')->name('editMasterData');
Route::post('/updateMasterData/{id}', 'UserController@updateMasterData')->name('updateMasterData');
Route::get('/editAccountData/{id}', 'UserController@editAccountData')->name('editAccountData');
Route::post('/updateAccountData/{id}', 'UserController@updateAccountData')->name('updateAccountData');
Route::get('/masterDataDelete/{id}', 'UserController@masterDataDelete')->name('masterDataDelete');
Route::get('/accountDataDelete/{id}', 'UserController@accountDataDelete')->name('accountDataDelete');
Route::get('/statutoryAdd', 'UserController@statutoryAddStore')->name('statutoryAdd');
Route::post('/statutoryAdd', 'UserController@statutoryAdd')->name('statutoryAdd');
Route::get('/statutoryView', 'UserController@statutoryStore')->name('statutoryView');
Route::get('/editStatutory/{id}', 'UserController@editStatutory')->name('editStatutory');
Route::post('/updateStatutory/{id}', 'UserController@updateStatutory')->name('updateStatutory');
Route::get('/tdsFile', 'UserController@tdsFileStore')->name('tdsFile');
Route::post('/tdsFile', 'UserController@tdsFile')->name('tdsFile');
Route::get('/tdsFileView', 'UserController@tdsFileView')->name('tdsFileView');
Route::get('/editTdsData/{id}', 'UserController@editTdsData')->name('editTdsData');
Route::post('/updateTdsData/{id}', 'UserController@updateTdsData')->name('updateTdsData');
Route::get('/tdsReturn', 'UserController@tdsReturnStore')->name('tdsReturn');
Route::post('/tdsReturn', 'UserController@tdsReturn')->name('tdsReturn');
Route::get('/tdsReturnView', 'UserController@tdsReturnView')->name('tdsReturnView');
Route::get('/editTdsReturnData/{id}', 'UserController@editTdsReturnData')->name('editTdsReturnData');
Route::post('/updateTdsReturnData/{id}', 'UserController@updateTdsReturnData')->name('updateTdsReturnData');

Route::get('/gstr1File', 'UserController@gstr1Store')->name('gstr1File');
Route::post('/gstr1File', 'UserController@gstr1Submit')->name('gstr1Submit');
Route::get('/gstr1FileView', 'UserController@gstr1FileView')->name('gstr1FileView');
Route::get('/editGstr1File/{id}', 'UserController@editGstr1File')->name('editGstr1File');
Route::post('/updateGstr1File/{id}', 'UserController@updateGstr1File')->name('updateGstr1File');

Route::get('/gstr2aFile', 'UserController@gstr2aStore')->name('gstr2aFile');
Route::post('/gstr2aFile', 'UserController@gstr2aSubmit')->name('gstr2aSubmit');
Route::get('/gstr2aFileView', 'UserController@gstr2aFileView')->name('gstr2aFileView');
Route::get('/editGstr2aFile/{id}', 'UserController@editGstr2aFile')->name('editGstr2aFile');
Route::post('/updateGstr2aFile/{id}', 'UserController@updateGstr2aFile')->name('updateGstr2aFile');

Route::get('/gstr3bFile', 'UserController@gstr3bStore')->name('gstr3bFile');
Route::post('/gstr3bFile', 'UserController@gstr3bSubmit')->name('gstr3bSubmit');
Route::get('/gstr3bFileView', 'UserController@gstr3bFileView')->name('gstr3bFileView');
Route::get('/editGstr3bFile/{id}', 'UserController@editGstr3bFile')->name('editGstr3bFile');
Route::post('/updateGstr3bFile/{id}', 'UserController@updateGstr3bFile')->name('updateGstr3bFile');

Route::get('/gstr9cFile', 'UserController@gstr9cStore')->name('gstr9cFile');
Route::post('/gstr9cFile', 'UserController@gstr9cSubmit')->name('gstr9cSubmit');
Route::get('/gstr9cFileView', 'UserController@gstr9cFileView')->name('gstr9cFileView');
Route::get('/editGstr9cFile/{id}', 'UserController@editGstr9cFile')->name('editGstr9cFile');
Route::post('/updateGstr9cFile/{id}', 'UserController@updateGstr9cFile')->name('updateGstr9cFile');

Route::get('/gstr9File', 'UserController@gstr9Store')->name('gstr9File');
Route::post('/gstr9File', 'UserController@gstr9Submit')->name('gstr9Submit');
Route::get('/gstr9FileView', 'UserController@gstr9FileView')->name('gstr9FileView');
Route::get('/editGstr9File/{id}', 'UserController@editGstr9File')->name('editGstr9File');
Route::post('/updateGstr9File/{id}', 'UserController@updateGstr9File')->name('updateGstr9File');

Route::get('/gstrChallan', 'UserController@gstrChallanStore')->name('gstrChallan');
Route::post('/gstrChallan', 'UserController@gstrChallanSubmit')->name('gstrChallanSubmit');
Route::get('/gstChallanView', 'UserController@gstChallanView')->name('gstChallanView');
Route::get('/editGstChallan/{id}', 'UserController@editGstChallan')->name('editGstChallan');
Route::post('/updateGstChallan/{id}', 'UserController@updateGstChallan')->name('updateGstChallan');

Route::get('/registrationGst', 'UserController@gstRegistration')->name('registrationGst');
Route::post('/gstRegistration', 'UserController@gstRegistrationSubmit')->name('gstRegistrationSubmit');
Route::get('/gstRegistrationView', 'UserController@gstRegistrationView')->name('gstRegistrationView');
Route::get('/editGstRegistration/{id}', 'UserController@editGstRegistration')->name('editGstRegistration');
Route::post('/updateGstRegistration/{id}', 'UserController@updateGstRegistration')->name('updateGstRegistration');

Route::get('/lut_certificate', 'UserController@lutCertificateStore')->name('lut_certificate');
Route::post('/lut_certificate', 'UserController@lutCertificateSubmit')->name('lutCertificateSubmit');
Route::get('/lutCertificateView', 'UserController@lutCertificateView')->name('lutCertificateView');
Route::get('/editLutCertificate/{id}', 'UserController@editLutCertificate')->name('editLutCertificate');
Route::post('/updateLutCertificate/{id}', 'UserController@updateLutCertificate')->name('updateLutCertificate');

Route::get('/notice_order', 'UserController@notice_orderStore')->name('notice_order');
Route::post('/notice_order', 'UserController@notice_orderSubmit')->name('notice_orderSubmit');
Route::get('/noticeOrderView', 'UserController@noticeOrderView')->name('noticeOrderView');
Route::get('/editNoticeOrder/{id}', 'UserController@editNoticeOrder')->name('editNoticeOrder');
Route::post('/updateNoticeOrder/{id}', 'UserController@updateNoticeOrder')->name('updateNoticeOrder');

Route::get('/pfReturnFile', 'UserController@pfReturnStore')->name('pfReturnFile');
Route::post('/pfReturnFile', 'UserController@pfReturnSubmit')->name('pfReturnSubmit');
Route::get('/pfReturnView', 'UserController@pfReturnView')->name('pfReturnView');
Route::get('/editPfReturnStore/{id}', 'UserController@editPfReturnStore')->name('editPfReturnStore');
Route::post('/updatePfReturn/{id}', 'UserController@updatePfReturn')->name('updatePfReturn');

Route::get('/pfChallanFile', 'UserController@pfChallanStore')->name('pfChallanFile');
Route::post('/pfChallanFile', 'UserController@pfChallanSubmit')->name('pfChallanSubmit');
Route::get('/pfChallanView', 'UserController@pfChallanView')->name('pfChallanView');
Route::get('/editPfChallanStore/{id}', 'UserController@editPfChallanStore')->name('editPfChallanStore');
Route::post('/updatePfChallan/{id}', 'UserController@updatePfChallan')->name('updatePfChallan');

Route::get('/pfPaymentFile', 'UserController@pfPaymentStore')->name('pfPaymentFile');
Route::post('/pfPaymentFile', 'UserController@pfPaymentSubmit')->name('pfPaymentSubmit');
Route::get('/pfPaymentView', 'UserController@pfPaymentView')->name('pfPaymentView');
Route::get('/editPfPaymentStore/{id}', 'UserController@editPfPaymentStore')->name('editPfPaymentStore');
Route::post('/updatePfPayment/{id}', 'UserController@updatePfPayment')->name('updatePfPayment');

Route::get('/esiReturnFile', 'UserController@esiReturnStore')->name('esiReturnFile');
Route::post('/esiReturnFile', 'UserController@esiReturnSubmit')->name('esiReturnSubmit');
Route::get('/esiReturnView', 'UserController@esiReturnView')->name('esiReturnView');
Route::get('/editEsiReturnStore/{id}', 'UserController@editEsiReturnStore')->name('editEsiReturnStore');
Route::post('/updateEsiReturn/{id}', 'UserController@updateEsiReturn')->name('updateEsiReturn');

Route::get('/esiChallanFile', 'UserController@esiChallanStore')->name('esiChallanFile');
Route::post('/esiChallanFile', 'UserController@esiChallanSubmit')->name('esiChallanSubmit');
Route::get('/esiChallanView', 'UserController@esiChallanView')->name('esiChallanView');
Route::get('/editEsiChallanStore/{id}', 'UserController@editEsiChallanStore')->name('editEsiChallanStore');
Route::post('/updateEsiChallan/{id}', 'UserController@updateEsiChallan')->name('updateEsiChallan');

Route::get('/esiPaymentFile', 'UserController@esiPaymentStore')->name('esiPaymentFile');
Route::post('/esiPaymentFile', 'UserController@esiPaymentSubmit')->name('esiPaymentSubmit');
Route::get('/esiPaymentView', 'UserController@esiPaymentView')->name('esiPaymentView');
Route::get('/editEsiPaymentStore/{id}', 'UserController@editEsiPaymentStore')->name('editEsiPaymentStore');
Route::post('/updateEsiPayment/{id}', 'UserController@updateEsiPayment')->name('updateEsiPayment');

Route::get('/payrollFile', 'UserController@payrollFileStore')->name('payrollFile');
Route::post('/payrollFile', 'UserController@payrollSubmit')->name('payrollSubmit');
Route::get('/payrollView', 'UserController@payrollView')->name('payrollView');
Route::get('/editPayrollStore/{id}', 'UserController@editPayrollStore')->name('editPayrollStore');
Route::post('/updatePayroll/{id}', 'UserController@updatePayroll')->name('updatePayroll');

Route::get('/form26asFile', 'UserController@form26asFileStore')->name('form26asFile');
Route::post('/form26asFile', 'UserController@form26asSubmit')->name('form26asSubmit');
Route::get('/form26asView', 'UserController@form26asView')->name('form26asView');
Route::get('/editForm26asStore/{id}', 'UserController@editForm26asStore')->name('editForm26asStore');
Route::post('/updateForm26as/{id}', 'UserController@updateForm26as')->name('updateForm26as');

Route::get('/ROCCompliance', 'UserController@rocCompliance')->name('ROCCompliance');
Route::post('/ROCCompliance', 'UserController@rocComplianceSubmit')->name('rocComplianceSubmit');
Route::get('/ROCComplianceView', 'UserController@ROCComplianceView')->name('ROCComplianceView');
Route::get('/editROCCompliance/{id}', 'UserController@editROCCompliance')->name('editROCCompliance');
Route::post('/updateROCCompliance/{id}', 'UserController@updateROCCompliance')->name('updateROCCompliance');

Route::get('/rocReturn', 'UserController@rocReturn')->name('rocReturn');
Route::post('/rocReturn', 'UserController@rocReturnSubmit')->name('rocReturnSubmit');
Route::get('/rocReturnView', 'UserController@rocReturnView')->name('rocReturnView');
Route::get('/editRocReturn/{id}', 'UserController@editRocReturn')->name('editRocReturn');
Route::post('/updateRocReturn/{id}', 'UserController@updateRocReturn')->name('updateRocReturn');

Route::get('/customerAgreement', 'UserController@customerAgreement')->name('customerAgreement');
Route::post('/customerAgreement', 'UserController@customerAgreementSubmit')->name('customerAgreementSubmit');
Route::get('/customerAgreementView', 'UserController@customerAgreementView')->name('customerAgreementView');
Route::get('/customerAgreementEdit/{id}', 'UserController@customerAgreementEdit')->name('customerAgreementEdit');
Route::post('/customerAgreementUpdate/{id}', 'UserController@customerAgreementUpdate')->name('customerAgreementUpdate');

Route::get('/customerInvoiceBill', 'UserController@customerInvoiceBill')->name('customerInvoiceBill');
Route::post('/customerInvoiceBill', 'UserController@customerInvoiceBillSubmit')->name('customerInvoiceBillSubmit');
Route::get('/customerInvoiceBillView', 'UserController@customerInvoiceBillView')->name('customerInvoiceBillView');
Route::get('/customerInvoiceBillEdit/{id}', 'UserController@customerInvoiceBillEdit')->name('customerInvoiceBillEdit');
Route::post('/customerInvoiceBillUpdate/{id}', 'UserController@customerInvoiceBillUpdate')->name('customerInvoiceBillUpdate');

Route::get('/employeeAgreement', 'UserController@employeeAgreement')->name('employeeAgreement');
Route::post('/employeeAgreement', 'UserController@employeeAgreementSubmit')->name('employeeAgreementSubmit');
Route::get('/employeeAgreementView', 'UserController@employeeAgreementView')->name('employeeAgreementView');
Route::get('/employeeAgreementEdit/{id}', 'UserController@employeeAgreementEdit')->name('employeeAgreementEdit');
Route::post('/employeeAgreementUpdate/{id}', 'UserController@employeeAgreementUpdate')->name('employeeAgreementUpdate');

Route::get('/employeeFFDetail', 'UserController@employeeFFDetail')->name('employeeFFDetail');
Route::post('/employeeFFDetail', 'UserController@employeeFFDetailSubmit')->name('employeeFFDetailSubmit');
Route::get('/employeeFFDetailView', 'UserController@employeeFFDetailView')->name('employeeFFDetailView');
Route::get('/employeeFFDetailEdit/{id}', 'UserController@employeeFFDetailEdit')->name('employeeFFDetailEdit');
Route::post('/employeeFFDetailUpdate/{id}', 'UserController@employeeFFDetailUpdate')->name('employeeFFDetailUpdate');

Route::get('/employeeHRPolicy', 'UserController@employeeHRPolicy')->name('employeeHRPolicy');
Route::post('/employeeHRPolicy', 'UserController@employeeHRPolicySubmit')->name('employeeHRPolicySubmit');
Route::get('/employeeHRPolicyView', 'UserController@employeeHRPolicyView')->name('employeeHRPolicyView');
Route::get('/employeeHRPolicyEdit/{id}', 'UserController@employeeHRPolicyEdit')->name('employeeHRPolicyEdit');
Route::post('/employeeHRPolicyUpdate/{id}', 'UserController@employeeHRPolicyUpdate')->name('employeeHRPolicyUpdate');

Route::get('/employeePayroll', 'UserController@employeePayroll')->name('employeePayroll');
Route::post('/employeePayroll', 'UserController@employeePayrollSubmit')->name('employeePayrollSubmit');
Route::get('/employeePayrollView', 'UserController@employeePayrollView')->name('employeePayrollView');
Route::get('/employeePayrollEdit/{id}', 'UserController@employeePayrollEdit')->name('employeePayrollEdit');
Route::post('/employeePayrollUpdate/{id}', 'UserController@employeePayrollUpdate')->name('employeePayrollUpdate');

Route::get('/fixedAssetsRegister', 'UserController@fixedAssetsRegister')->name('fixedAssetsRegister');
Route::post('/fixedAssetsRegister', 'UserController@fixedAssetsRegisterSubmit')->name('fixedAssetsRegisterSubmit');
Route::get('/fixedAssetsRegisterView', 'UserController@fixedAssetsRegisterView')->name('fixedAssetsRegisterView');
Route::get('/fixedAssetsRegisterEdit/{id}', 'UserController@fixedAssetsRegisterEdit')->name('fixedAssetsRegisterEdit');
Route::post('/fixedAssetsRegisterUpdate/{id}', 'UserController@fixedAssetsRegisterUpdate')->name('fixedAssetsRegisterUpdate');

Route::get('/bankReconcilliation', 'UserController@bankReconcilliation')->name('bankReconcilliation');
Route::post('/bankReconcilliation', 'UserController@bankReconcilliationSubmit')->name('bankReconcilliationSubmit');
Route::get('/bankReconcilliationView', 'UserController@bankReconcilliationView')->name('bankReconcilliationView');
Route::get('/bankReconcilliationEdit/{id}', 'UserController@bankReconcilliationEdit')->name('bankReconcilliationEdit');
Route::post('/bankReconcilliationUpdate/{id}', 'UserController@bankReconcilliationUpdate')->name('bankReconcilliationUpdate');

Route::get('/vendorAgreement', 'UserController@vendorAgreement')->name('vendorAgreement');
Route::post('/vendorAgreement', 'UserController@vendorAgreementSubmit')->name('vendorAgreementSubmit');
Route::get('/vendorAgreementView', 'UserController@vendorAgreementView')->name('vendorAgreementView');
Route::get('/vendorAgreementView', 'UserController@vendorAgreementView')->name('vendorAgreementView');
Route::get('/vendorAgreementEdit/{id}', 'UserController@vendorAgreementEdit')->name('vendorAgreementEdit');
Route::post('/vendorAgreementUpdate/{id}', 'UserController@vendorAgreementUpdate')->name('vendorAgreementUpdate');

Route::get('/vendorBill', 'UserController@vendorBill')->name('vendorBill');
Route::post('/vendorBill', 'UserController@vendorBillSubmit')->name('vendorBillSubmit');
Route::get('/vendorBillView', 'UserController@vendorBillView')->name('vendorBillView');
Route::get('/vendorBillEdit/{id}', 'UserController@vendorBillEdit')->name('vendorBillEdit');
Route::post('/vendorBillUpdate/{id}', 'UserController@vendorBillUpdate')->name('vendorBillUpdate');


Route::group(['prefix'=>'admin'], function(){


    Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
    Route::get('/adminlogin', 'Auth\AdminLoginController@showLoginForm');
    Route::post('/adminlogin', 'Auth\AdminLoginController@login')->name('admin.login');
    Route::get('adminS/','AdminController@index')->name('adminS');
   // exit();
    Route::post('/storeUser','AdminController@storeUser')->name('storeUser');
    Route::get('/storeUser','AdminController@store')->name('storeUser');

    Route::get('/showUser','AdminController@showUser')->name('showUser');
    Route::get('/showUserData/{id}','AdminController@showUserData')->name('showUserData');

    Route::get('/editUser/{id}','AdminController@editUser')->name('editUser');

    Route::post('/update/{id}','AdminController@update')->name('update');
    Route::put('/update/{id}','AdminController@update')->name('update');

    Route::get('/delete/{id}','AdminController@destroyUser')->name('delete');

//    Route::get('/masterCompanyUpload','AdminController@masterCompanyUploadStore')->name('masterCompanyUpload');
//    Route::post('/masterCompanyUpload','AdminController@masterCompanyUpload')->name('masterCompanyUpload');
//
//    Route::get('/complianceTrackers','AdminController@complianceTrackerStore')->name('complianceTrackerStore');
//    Route::post('/complianceTrackers','AdminController@complianceTracker')->name('complianceTracker');

    Route::get('/quickInfo','AdminController@quickInfoStore')->name('quickInfoStore');
    Route::post('/quickInfo','AdminController@quickInfo')->name('quickInfo');

//    Route::get('/branchesCompany','AdminController@branchesCompanyStore')->name('branchesCompany');
//    Route::post('/branchesCompany','AdminController@branchesCompany')->name('branchesCompany');


    //Route::get('/branchCompanyMaster','AdminController@branchCompanyMasterStore')->name('branchCompanyMaster');
    //Route::post('/branchCompanyMaster','AdminController@branchCompanyMaster')->name('branchCompanyMaster');

    //Route::get('/branchComplianceTracker','AdminController@branchComplianceTrackerStore')->name('branchComplianceTracker ');
    //Route::post('/branchComplianceTracker','AdminController@branchComplianceTracker')->name('branchComplianceTracker');


    Route::get('/adminMasterView','AdminController@adminMasterView')->name('adminMasterView');
    Route::get('/adminAccountView','AdminController@adminAccountView')->name('adminAccountView');
    Route::get('/adminStatutoryView','AdminController@adminStatutoryView')->name('adminStatutoryView');
    Route::get('/adminTdsView','AdminController@adminTdsView')->name('adminTdsView');
    Route::get('/adminTdsReturnView','AdminController@adminTdsReturnView')->name('adminTdsReturnView');
    Route::get('/adminGstr1View','AdminController@adminGstr1View')->name('adminGstr1View');
    Route::get('/adminGstr2aView','AdminController@adminGstr2aView')->name('adminGstr2aView');
    Route::get('/adminGstr3bView','AdminController@adminGstr3bView')->name('adminGstr3bView');
    Route::get('/adminGstr9View','AdminController@adminGstr9View')->name('adminGstr9View');
    Route::get('/adminGstr9cView','AdminController@adminGstr9cView')->name('adminGstr9cView');
    Route::get('/adminGstChallanView','AdminController@adminGstChallanView')->name('adminGstChallanView');
    Route::get('/adminGstRegistrationView','AdminController@adminGstRegistrationView')->name('adminGstRegistrationView');
    Route::get('/adminLutCertificateView','AdminController@adminLutCertificateView')->name('adminLutCertificateView');
    Route::get('/adminNoticeOrderView','AdminController@adminNoticeOrderView')->name('adminNoticeOrderView');

    Route::get('/adminPfReturnView','AdminController@adminPfReturnView')->name('adminPfReturnView');
    Route::get('/adminPfPaymentView','AdminController@adminPfPaymentView')->name('adminPfPaymentView');
    Route::get('/adminPfChallanView','AdminController@adminPfChallanView')->name('adminPfChallanView');
    Route::get('/adminEsiReturnView','AdminController@adminEsiReturnView')->name('adminEsiReturnView');
    Route::get('/adminEsiPaymentView','AdminController@adminEsiPaymentView')->name('adminEsiPaymentView');
    Route::get('/adminEsiChallanView','AdminController@adminEsiChallanView')->name('adminEsiChallanView');

    Route::get('/adminForm26asView','AdminController@adminForm26asView')->name('adminForm26asView');
    Route::get('/adminPayrollView','AdminController@adminPayrollView')->name('adminPayrollView');
    Route::get('/adminRocComplianceView','AdminController@adminRocComplianceView')->name('adminRocComplianceView');
    Route::get('/adminRocReturnView','AdminController@adminRocReturnView')->name('adminRocReturnView');

    Route::get('/adminCustomerAgreementView','AdminController@adminCustomerAgreementView')->name('adminCustomerAgreementView');
    Route::get('/adminCustomerInvoiceBillView','AdminController@adminCustomerInvoiceBillView')->name('adminCustomerInvoiceBillView');
    Route::get('/adminVendorAgreementView','AdminController@adminVendorAgreementView')->name('adminVendorAgreementView');
    Route::get('/adminVendorBillView','AdminController@adminVendorBillView')->name('adminVendorBillView');
    Route::get('/adminEmployeeAgreementView','AdminController@adminEmployeeAgreementView')->name('adminEmployeeAgreementView');
    Route::get('/adminEmployeePayrollView','AdminController@adminEmployeePayrollView')->name('adminEmployeePayrollView');
    Route::get('/adminEmployeeFFDetailView','AdminController@adminEmployeeFFDetailView')->name('adminEmployeeFFDetailView');
    Route::get('/adminEmployeeHRPolicyView','AdminController@adminEmployeeHRPolicyView')->name('adminEmployeeHRPolicyView');
    Route::get('/adminFixedAssetsView','AdminController@adminFixedAssetsView')->name('adminFixedAssetsView');
    Route::get('/adminBankReconcilliationView','AdminController@adminBankReconcilliationView')->name('adminBankReconcilliationView');


    Route::get('/home','AdminController@home')->name('home');
//    Route::get('/admin-login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
////    Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
//    Route::post('/admin-login', 'Auth\AdminLoginController@login')->name('admin.login.submit');

});
