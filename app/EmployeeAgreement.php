<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeAgreement extends Model
{
    //
    protected $fillable = [

        'company_name',
        'employee_agreement',
    ];
}
