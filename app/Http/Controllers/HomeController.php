<?php

namespace App\Http\Controllers;

use App\Mastercompanies;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function userSideView(){

        $showUserEntry = User::find(Auth::id());
        //ini_set('memory_limit', '-1');
        $masterComapnyEntry = Mastercompanies::where('user_id', Auth::id())->first();

        return view('userSideView')->with('showUserEntry', $showUserEntry)->with('masterComapnyEntry', $masterComapnyEntry);
    }
}
