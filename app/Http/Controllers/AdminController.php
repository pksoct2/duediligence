<?php

namespace App\Http\Controllers;

use App\Account;
use App\Agreement;
use App\BankReconcilliation;
use App\Branch;
use App\ComplianceTracker;
use App\ComplianceTrackerBranch;
use App\EmployeeAgreement;
use App\EmployeeFFdetail;
use App\EmployeeHRPolicy;
use App\EmployeePayroll;
use App\EsiChallan;
use App\EsiPayment;
use App\EsiReturn;
use App\FixedAsset;
use App\Form26as;
use App\GstChallan;
use App\Gstr1;
use App\Gstr2a;
use App\Gstr3b;
use App\Gstr9;
use App\Gstr9c;
use App\GstRegistration;
use App\Invoicebill;
use App\LutCertificate;
use App\Mastercompanies;
use App\Mastercompany;
use App\MasterCompanyBranch;
use App\Mastercompanys;
use App\MasterData;
use App\NoticeOrderReceived;
use App\Payroll;
use App\PfChallan;
use App\PfPayment;
use App\PfReturn;
use App\QuickInfo;
use App\RocCompliance;
use App\ROCReturns;
use App\Statutory;
use App\Tds;
use App\TdsReturn;
use App\User;
use App\VendorAgreement;
use App\Vendorbill;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Session\Session;
use Illuminate\Validation\ValidationException;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     *
     */
    public function index()
    {
        return view('admin.adminS');
    }

    public function home(){

        return view('admin.home');
    }

//    public function complianceTrackerStore(){
//        $users = Mastercompanies::all();
//        $showAllUser = Mastercompanies::all();
//
//        return view('admin.complianceTrackers')->with('users', $users)->with('showAllUser',$showAllUser);
//    }

    public function quickInfoStore(){
        $info = QuickInfo::all()->last();
        return view('admin.quickInfo')->with('info', $info);
    }

    public function quickInfo( Request $request){


        $this->validate($request, [
            'quick_info' => ['required'],
            'tds_rates' => ['required'],
            'gst_rates' => ['required'],
            'quick_glances' => ['required'],

        ]);

        $id = 1;
        $quickInfo = QuickInfo::find($id);
        $quickInfo->quick_info = $request->quick_info;
        $quickInfo->tds_rates = $request->tds_rates;
        $quickInfo->gst_rates = $request->gst_rates;
        $quickInfo->quick_glances = $request->quick_glances;
        $quickInfo->save();

        $request->session()->flash('success', 'Quick Info Updated successfully.');

        return view('admin.quickInfo');

    }


//    public function complianceTracker(Request $request){
//
//
//        $this->validate($request, [
//            'users_name' => ['required', 'unique:compliance_trackers'],
//            'select_month_year' => ['required'],
//            'due_date' => ['required'],
//            'category' => ['required'],
//            'details' => ['required'],
//            'compliance_date' => ['required'],
//            'documents' => 'required | image|mimes:jpeg,png,jpg,pdf'
//
//        ]);
//
//        $documents_img = $request->documents;
//
//        $documents_img_new_name = time() . $documents_img->getClientOriginalName();
//
//        $documents_img->move(public_path('complianceTrackersimg'),$documents_img_new_name);
//
//
//        if ($request->hasFile('documents')){
//
//            $complianceTracker = ComplianceTracker::create([
//                'users_name' => $request['users_name'],
//                'select_month_year' => $request['select_month_year'],
//                'due_date' => $request['due_date'],
//                'category' => $request['category'],
//                'details' => $request['details'],
//                'compliance_date' => $request['compliance_date'],
//                'documents' => $documents_img_new_name,
//            ]);
//        }
//
//
//        $request->session()->flash('success', 'Compliance Tracker Added successfully.');
//
//        return view('admin.complianceTrackers');
//    }

    public function store(){

        return view('admin.storeUser');
    }
    public function showUser(){

        $userShow = User::all();
        //print_r($userShow);
        return view('admin.showUser')->with('userShow', $userShow);

    }

    public function storeUser(Request $request){

        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'max:255','unique:users'],
            'password' => ['required', 'string', 'min:8'],
            'spoc_dev'=> ['required']
        ]);

        $storeUser = User::create([
            'name' => $request['name'],
            'username' => $request['username'],
            'password' => Hash::make($request['password']),
            'spoc_dev' => $request['spoc_dev']
        ]);

        $request->session()->flash('success', 'User Added successfully.');

        return view('admin.storeUser');
    }

    public function showUserData($id){
        $showUserEntry = User::find($id);
        $showAllUser = User::all();
        //ini_set('memory_limit', '-1');
        $masterComapnyEntry = Mastercompanies::where('user_id', $id)->first();

        return view('admin.showUserData')->with('showUserEntry', $showUserEntry)->with('masterComapnyEntry', $masterComapnyEntry);
    }

    public function editUser($id){

        $user = User::find($id);

        return view('admin.editUser')->with('user',$user);
    }

    public function update($id, Request $request){

        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'max:255','unique:users'],
            'password' => [''],
            'spoc_dev'=> ['required']
        ]);

        $user = User::find($id);
            $user->name = $request->name;
            $user->username = $request->username;
            $user->password = Hash::make($request['password']);
            $user->spoc_dev = $request->spoc_dev;
            $user->save();

        $request->session()->flash('success', 'User Updated successfully.');

        return redirect('admin/showUser');

    }
    public function delete($id){

        $user = User::find($id);

        $user->delete();

        Session::flash('success','the user deleted successfully.');

        return redirect()->back();

    }

//    public function masterCompanyUploadStore(){
//        $users = User::all();
//        $showAllUser = User::all();
//        return view('admin.masterCompanyUpload')->with('users', $users)->with('showAllUser', $showAllUser);
//
//    }
//
//    public function masterCompanyUpload( Request $request){
//
//        $this->validate($request, [
//            'users_name' => 'required | unique:mastercompanies',
//            'pan_income_tax_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'tan_traces_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'certificate_of_incorp_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'memo_of_association_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'articles_of_association_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'gst_tax_cert_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'lut_cert_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'gst_e_com_cert_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'prof_tax_enrollment_cert_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'prof_tax_register_cert_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'pf_establish_cert_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'esic_register_cert_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'shops_establish_cert_trade_licence_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'iec_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'msmed_cert_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'startup_cert_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'lower_tax_deduction_cert_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'tax_exemption_cert_us12a_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'tax_exemption_cert_us80g_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'any_other_details' => 'image|mimes:jpeg,png,jpg,pdf',
//            'pan_income_tax' => 'required',
//            'tan_traces' => 'required',
//            'certificate_of_incorp' => 'required',
//            'memo_of_association' => 'required',
//            'articles_of_association' => 'required',
//            'gst_tax_cert' => 'required',
//            'lut_cert' => 'required',
//            'gst_e_com_cert' => 'required',
//            'prof_tax_enrollment_cert' => 'required',
//            'prof_tax_register_cert' => 'required',
//            'pf_establish_cert' => 'required',
//            'esic_register_cert' => 'required',
//            'shops_establish_cert_trade_licence' => 'required',
//            'iec' => 'required',
//            'msmed_cert' => 'required',
//            'startup_cert' => 'required',
//            'lower_tax_deduction_cert' => 'required',
//            'tax_exemption_cert_us12a' => 'required',
//            'tax_exemption_cert_us80g' => 'required',
//            'any_other_details_etc' => 'required'
//        ]);
//
//
//
//            $pan_income_tax_img = $request->pan_income_tax_img;
//
//            $pan_income_tax_img_new_name = time() . $pan_income_tax_img->getClientOriginalName();
//
//            $pan_income_tax_img->move(public_path('userdoc'),$pan_income_tax_img_new_name);
//
//
//            $tan_traces_img = $request->tan_traces_img;
//
//            $tan_traces_img_new_name = time() . $tan_traces_img->getClientOriginalName();
//
//            $tan_traces_img->move(public_path('userdoc'),$tan_traces_img_new_name);
//
//
//
//            $certificate_of_incorp_img = $request->certificate_of_incorp_img;
//
//            $certificate_of_incorp_img_new_name = time() . $certificate_of_incorp_img->getClientOriginalName();
//
//            $certificate_of_incorp_img->move(public_path('userdoc'),$certificate_of_incorp_img_new_name);
//
//
//
//
//            $memo_of_association_img = $request->memo_of_association_img;
//
//            $memo_of_association_img_new_name = time() . $memo_of_association_img->getClientOriginalName();
//
//            $memo_of_association_img->move(public_path('userdoc'),$memo_of_association_img_new_name);
//
//
//
//
//            $articles_of_association_img = $request->articles_of_association_img;
//
//            $articles_of_association_img_new_name = time() . $articles_of_association_img->getClientOriginalName();
//
//            $articles_of_association_img->move(public_path('userdoc'),$articles_of_association_img_new_name);
//
//
//
//            $gst_tax_cert_img = $request->gst_tax_cert_img;
//
//            $gst_tax_cert_img_new_name = time() . $gst_tax_cert_img->getClientOriginalName();
//
//            $gst_tax_cert_img->move(public_path('userdoc'),$gst_tax_cert_img_new_name);
//
//
//            $lut_cert_img = $request->lut_cert_img;
//
//            $lut_cert_img_new_name = time() . $lut_cert_img->getClientOriginalName();
//
//            $lut_cert_img->move(public_path('userdoc'),$lut_cert_img_new_name);
//
//
//            $gst_e_com_cert_img = $request->gst_e_com_cert_img;
//
//            $gst_e_com_cert_img_new_name = time() . $gst_e_com_cert_img->getClientOriginalName();
//
//            $gst_e_com_cert_img->move(public_path('userdoc'),$gst_e_com_cert_img_new_name);
//
//
//
//            $prof_tax_enrollment_cert_img = $request->prof_tax_enrollment_cert_img;
//
//            $prof_tax_enrollment_cert_img_new_name = time() . $prof_tax_enrollment_cert_img->getClientOriginalName();
//
//            $prof_tax_enrollment_cert_img->move(public_path('userdoc'),$prof_tax_enrollment_cert_img_new_name);
//
//
//
//            $prof_tax_register_cert_img = $request->prof_tax_register_cert_img;
//
//            $prof_tax_register_cert_img_new_name = time() . $prof_tax_register_cert_img->getClientOriginalName();
//
//            $prof_tax_register_cert_img->move(public_path('userdoc'),$prof_tax_register_cert_img_new_name);
//
//
//
//            $pf_establish_cert_img = $request->pf_establish_cert_img;
//
//            $pf_establish_cert_img_new_name = time() . $pf_establish_cert_img->getClientOriginalName();
//
//            $pf_establish_cert_img->move(public_path('userdoc'),$pf_establish_cert_img_new_name);
//
//
//
//            $esic_register_cert_img = $request->esic_register_cert_img;
//
//            $esic_register_cert_img_new_name = time() . $esic_register_cert_img->getClientOriginalName();
//
//            $esic_register_cert_img->move(public_path('userdoc'),$esic_register_cert_img_new_name);
//
//
//
//            $shops_establish_cert_trade_licence_img = $request->shops_establish_cert_trade_licence_img;
//
//            $shops_establish_cert_trade_licence_img_new_name = time() . $shops_establish_cert_trade_licence_img->getClientOriginalName();
//
//            $shops_establish_cert_trade_licence_img->move(public_path('userdoc'),$shops_establish_cert_trade_licence_img_new_name);
//
//
//
//            $iec_img = $request->iec_img;
//
//            $iec_img_new_name = time() . $iec_img->getClientOriginalName();
//
//            $iec_img->move(public_path('userdoc'),$iec_img_new_name);
//
//
//
//            $msmed_cert_img = $request->msmed_cert_img;
//
//            $msmed_cert_img_new_name = time() . $msmed_cert_img->getClientOriginalName();
//
//            $msmed_cert_img->move(public_path('userdoc'),$msmed_cert_img_new_name);
//
//
//
//            $startup_cert_img = $request->startup_cert_img;
//
//            $startup_cert_img_new_name = time() . $startup_cert_img->getClientOriginalName();
//
//            $startup_cert_img->move(public_path('userdoc'),$startup_cert_img_new_name);
//
//
//
//            $lower_tax_deduction_cert_img = $request->lower_tax_deduction_cert_img;
//
//            $lower_tax_deduction_cert_img_new_name = time() . $lower_tax_deduction_cert_img->getClientOriginalName();
//
//            $lower_tax_deduction_cert_img->move(public_path('userdoc'),$lower_tax_deduction_cert_img_new_name);
//
//
//
//            $tax_exemption_cert_us12a_img = $request->tax_exemption_cert_us12a_img;
//
//            $tax_exemption_cert_us12a_img_new_name = time() . $tax_exemption_cert_us12a_img->getClientOriginalName();
//
//            $tax_exemption_cert_us12a_img->move(public_path('userdoc'),$tax_exemption_cert_us12a_img_new_name);
//
//
//
//            $tax_exemption_cert_us80g_img = $request->tax_exemption_cert_us80g_img;
//
//            $tax_exemption_cert_us80g_img_new_name = time() . $tax_exemption_cert_us80g_img->getClientOriginalName();
//
//            $tax_exemption_cert_us80g_img->move(public_path('userdoc'),$tax_exemption_cert_us80g_img_new_name);
//
//
//
//            $any_other_details = $request->any_other_details;
//
//            $any_other_details_new_name = time() . $any_other_details->getClientOriginalName();
//
//            $any_other_details->move(public_path('userdoc'),$any_other_details_new_name);
//
////       $userdata = User::find($request['users_name']);
////        print_r($userdata."name");
////        exit();
//
//
//
//        if ($request->hasFile('pan_income_tax_img') && $request->hasFile('tan_traces_img') && $request->hasFile('tan_traces_img') && $request->hasFile('certificate_of_incorp_img') &&
//            $request->hasFile('memo_of_association_img') && $request->hasFile('articles_of_association_img') && $request->hasFile('gst_tax_cert_img') && $request->hasFile('lut_cert_img') &&
//            $request->hasFile('gst_e_com_cert_img') && $request->hasFile('prof_tax_enrollment_cert_img') && $request->hasFile('prof_tax_register_cert_img') &&
//            $request->hasFile('pf_establish_cert_img') && $request->hasFile('esic_register_cert_img') && $request->hasFile('shops_establish_cert_trade_licence_img') &&
//            $request->hasFile('iec_img') && $request->hasFile('msmed_cert_img') && $request->hasFile('startup_cert_img') && $request->hasFile('lower_tax_deduction_cert_img') &&
//            $request->hasFile('tax_exemption_cert_us12a_img') && $request->hasFile('tax_exemption_cert_us80g_img') && $request->hasFile('any_other_details')
//        ){
//
//
//            $masterCompany = Mastercompanies::create([
//
//                'user_id' => $request[''],
//                'users_name' =>$request['users_name'],
//                'pan_income_tax_img' => $pan_income_tax_img_new_name,
//                'tan_traces_img' => $tan_traces_img_new_name,
//                'tax_exemption_cert_us80g_img' => $tax_exemption_cert_us80g_img_new_name,
//                'tax_exemption_cert_us12a_img' => $tax_exemption_cert_us12a_img_new_name,
//                'lower_tax_deduction_cert_img' => $lower_tax_deduction_cert_img_new_name,
//                'startup_cert_img' => $startup_cert_img_new_name,
//                'msmed_cert_img' => $msmed_cert_img_new_name,
//                'iec_img' => $iec_img_new_name,
//                'shops_establish_cert_trade_licence_img' => $shops_establish_cert_trade_licence_img_new_name,
//                'esic_register_cert_img' => $esic_register_cert_img_new_name,
//                'pf_establish_cert_img' => $pf_establish_cert_img_new_name,
//                'prof_tax_register_cert_img' => $prof_tax_register_cert_img_new_name,
//                'prof_tax_enrollment_cert_img' => $prof_tax_enrollment_cert_img_new_name,
//                'gst_e_com_cert_img' => $gst_e_com_cert_img_new_name,
//                'lut_cert_img' => $lut_cert_img_new_name,
//                'gst_tax_cert_img' => $gst_tax_cert_img_new_name,
//                'articles_of_association_img' => $articles_of_association_img_new_name,
//                'memo_of_association_img' => $memo_of_association_img_new_name,
//                'certificate_of_incorp_img' => $certificate_of_incorp_img_new_name,
//                'any_other_details' => $any_other_details_new_name,
//                'pan_income_tax' => $request['pan_income_tax'],
//                'tan_traces' => $request['tan_traces'],
//                'tax_exemption_cert_us80g' => $request['tax_exemption_cert_us80g'],
//                'tax_exemption_cert_us12a' => $request['tax_exemption_cert_us12a'],
//                'lower_tax_deduction_cert' => $request['lower_tax_deduction_cert'],
//                'startup_cert' => $request['startup_cert'],
//                'msmed_cert' => $request['msmed_cert'],
//                'iec' => $request['iec'],
//                'shops_establish_cert_trade_licence' => $request['shops_establish_cert_trade_licence'],
//                'esic_register_cert' => $request['esic_register_cert'],
//                'pf_establish_cert' => $request['pf_establish_cert'],
//                'prof_tax_register_cert' => $request['prof_tax_register_cert'],
//                'prof_tax_enrollment_cert' => $request['prof_tax_enrollment_cert'],
//                'gst_e_com_cert' => $request['gst_e_com_cert'],
//                'lut_cert' => $request['lut_cert'],
//                'gst_tax_cert' => $request['gst_tax_cert'],
//                'articles_of_association' => $request['articles_of_association'],
//                'memo_of_association' => $request['memo_of_association'],
//                'certificate_of_incorp' => $request['certificate_of_incorp'],
//                'any_other_details_etc' => $request['any_other_details_etc']
//
//            ]);
//
//
//
//        }
//
//
//        $request->session()->flash('success', 'User Documents Uploaded successfully.');
//
//        return redirect('admin/masterCompanyUpload');
//
//    }

//    public function branchCompanyMasterStore(){
//        $users = User::all();
//        $showAllUser = Branch::all();
//        return view('admin.branchCompanyMaster')->with('users', $users)->with('showAllUser', $showAllUser);
//
//    }
//
//    public function branchCompanyMaster(Request $request){
//
//
//        $this->validate($request, [
//            'user_id' => '',
//            'parent_company_name' => 'required',
//            'branch_name' => 'required | unique:master_company_branches',
//            'pan_income_tax_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'tan_traces_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'certificate_of_incorp_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'memo_of_association_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'articles_of_association_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'gst_tax_cert_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'lut_cert_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'gst_e_com_cert_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'prof_tax_enrollment_cert_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'prof_tax_register_cert_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'pf_establish_cert_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'esic_register_cert_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'shops_establish_cert_trade_licence_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'iec_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'msmed_cert_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'startup_cert_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'lower_tax_deduction_cert_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'tax_exemption_cert_us12a_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'tax_exemption_cert_us80g_img' => 'image|mimes:jpeg,png,jpg,pdf',
//            'any_other_details' => 'image|mimes:jpeg,png,jpg,pdf',
//            'pan_income_tax' => 'required',
//            'tan_traces' => 'required',
//            'certificate_of_incorp' => 'required',
//            'memo_of_association' => 'required',
//            'articles_of_association' => 'required',
//            'gst_tax_cert' => 'required',
//            'lut_cert' => 'required',
//            'gst_e_com_cert' => 'required',
//            'prof_tax_enrollment_cert' => 'required',
//            'prof_tax_register_cert' => 'required',
//            'pf_establish_cert' => 'required',
//            'esic_register_cert' => 'required',
//            'shops_establish_cert_trade_licence' => 'required',
//            'iec' => 'required',
//            'msmed_cert' => 'required',
//            'startup_cert' => 'required',
//            'lower_tax_deduction_cert' => 'required',
//            'tax_exemption_cert_us12a' => 'required',
//            'tax_exemption_cert_us80g' => 'required',
//            'any_other_details_etc' => 'required'
//        ]);
//
//
//
//        $pan_income_tax_img = $request->pan_income_tax_img;
//
//        $pan_income_tax_img_new_name = time() . $pan_income_tax_img->getClientOriginalName();
//
//        $pan_income_tax_img->move(public_path('branchesDocs'),$pan_income_tax_img_new_name);
//
//
//        $tan_traces_img = $request->tan_traces_img;
//
//        $tan_traces_img_new_name = time() . $tan_traces_img->getClientOriginalName();
//
//        $tan_traces_img->move(public_path('branchesDocs'),$tan_traces_img_new_name);
//
//
//
//        $certificate_of_incorp_img = $request->certificate_of_incorp_img;
//
//        $certificate_of_incorp_img_new_name = time() . $certificate_of_incorp_img->getClientOriginalName();
//
//        $certificate_of_incorp_img->move(public_path('branchesDocs'),$certificate_of_incorp_img_new_name);
//
//
//
//
//        $memo_of_association_img = $request->memo_of_association_img;
//
//        $memo_of_association_img_new_name = time() . $memo_of_association_img->getClientOriginalName();
//
//        $memo_of_association_img->move(public_path('branchesDocs'),$memo_of_association_img_new_name);
//
//
//
//
//        $articles_of_association_img = $request->articles_of_association_img;
//
//        $articles_of_association_img_new_name = time() . $articles_of_association_img->getClientOriginalName();
//
//        $articles_of_association_img->move(public_path('branchesDocs'),$articles_of_association_img_new_name);
//
//
//
//        $gst_tax_cert_img = $request->gst_tax_cert_img;
//
//        $gst_tax_cert_img_new_name = time() . $gst_tax_cert_img->getClientOriginalName();
//
//        $gst_tax_cert_img->move(public_path('branchesDocs'),$gst_tax_cert_img_new_name);
//
//
//        $lut_cert_img = $request->lut_cert_img;
//
//        $lut_cert_img_new_name = time() . $lut_cert_img->getClientOriginalName();
//
//        $lut_cert_img->move(public_path('branchesDocs'),$lut_cert_img_new_name);
//
//
//        $gst_e_com_cert_img = $request->gst_e_com_cert_img;
//
//        $gst_e_com_cert_img_new_name = time() . $gst_e_com_cert_img->getClientOriginalName();
//
//        $gst_e_com_cert_img->move(public_path('branchesDocs'),$gst_e_com_cert_img_new_name);
//
//
//
//        $prof_tax_enrollment_cert_img = $request->prof_tax_enrollment_cert_img;
//
//        $prof_tax_enrollment_cert_img_new_name = time() . $prof_tax_enrollment_cert_img->getClientOriginalName();
//
//        $prof_tax_enrollment_cert_img->move(public_path('branchesDocs'),$prof_tax_enrollment_cert_img_new_name);
//
//
//
//        $prof_tax_register_cert_img = $request->prof_tax_register_cert_img;
//
//        $prof_tax_register_cert_img_new_name = time() . $prof_tax_register_cert_img->getClientOriginalName();
//
//        $prof_tax_register_cert_img->move(public_path('branchesDocs'),$prof_tax_register_cert_img_new_name);
//
//
//
//        $pf_establish_cert_img = $request->pf_establish_cert_img;
//
//        $pf_establish_cert_img_new_name = time() . $pf_establish_cert_img->getClientOriginalName();
//
//        $pf_establish_cert_img->move(public_path('branchesDocs'),$pf_establish_cert_img_new_name);
//
//
//
//        $esic_register_cert_img = $request->esic_register_cert_img;
//
//        $esic_register_cert_img_new_name = time() . $esic_register_cert_img->getClientOriginalName();
//
//        $esic_register_cert_img->move(public_path('branchesDocs'),$esic_register_cert_img_new_name);
//
//
//
//        $shops_establish_cert_trade_licence_img = $request->shops_establish_cert_trade_licence_img;
//
//        $shops_establish_cert_trade_licence_img_new_name = time() . $shops_establish_cert_trade_licence_img->getClientOriginalName();
//
//        $shops_establish_cert_trade_licence_img->move(public_path('branchesDocs'),$shops_establish_cert_trade_licence_img_new_name);
//
//
//
//        $iec_img = $request->iec_img;
//
//        $iec_img_new_name = time() . $iec_img->getClientOriginalName();
//
//        $iec_img->move(public_path('branchesDocs'),$iec_img_new_name);
//
//
//
//        $msmed_cert_img = $request->msmed_cert_img;
//
//        $msmed_cert_img_new_name = time() . $msmed_cert_img->getClientOriginalName();
//
//        $msmed_cert_img->move(public_path('branchesDocs'),$msmed_cert_img_new_name);
//
//
//
//        $startup_cert_img = $request->startup_cert_img;
//
//        $startup_cert_img_new_name = time() . $startup_cert_img->getClientOriginalName();
//
//        $startup_cert_img->move(public_path('branchesDocs'),$startup_cert_img_new_name);
//
//
//
//        $lower_tax_deduction_cert_img = $request->lower_tax_deduction_cert_img;
//
//        $lower_tax_deduction_cert_img_new_name = time() . $lower_tax_deduction_cert_img->getClientOriginalName();
//
//        $lower_tax_deduction_cert_img->move(public_path('branchesDocs'),$lower_tax_deduction_cert_img_new_name);
//
//
//
//        $tax_exemption_cert_us12a_img = $request->tax_exemption_cert_us12a_img;
//
//        $tax_exemption_cert_us12a_img_new_name = time() . $tax_exemption_cert_us12a_img->getClientOriginalName();
//
//        $tax_exemption_cert_us12a_img->move(public_path('branchesDocs'),$tax_exemption_cert_us12a_img_new_name);
//
//
//
//        $tax_exemption_cert_us80g_img = $request->tax_exemption_cert_us80g_img;
//
//        $tax_exemption_cert_us80g_img_new_name = time() . $tax_exemption_cert_us80g_img->getClientOriginalName();
//
//        $tax_exemption_cert_us80g_img->move(public_path('branchesDocs'),$tax_exemption_cert_us80g_img_new_name);
//
//
//
//        $any_other_details = $request->any_other_details;
//
//        $any_other_details_new_name = time() . $any_other_details->getClientOriginalName();
//
//        $any_other_details->move(public_path('branchesDocs'),$any_other_details_new_name);
//
////       $userdata = User::find($request['users_name']);
////        print_r($userdata."name");
////        exit();
//
//
//
//        if ($request->hasFile('pan_income_tax_img') && $request->hasFile('tan_traces_img') && $request->hasFile('tan_traces_img') && $request->hasFile('certificate_of_incorp_img') &&
//            $request->hasFile('memo_of_association_img') && $request->hasFile('articles_of_association_img') && $request->hasFile('gst_tax_cert_img') && $request->hasFile('lut_cert_img') &&
//            $request->hasFile('gst_e_com_cert_img') && $request->hasFile('prof_tax_enrollment_cert_img') && $request->hasFile('prof_tax_register_cert_img') &&
//            $request->hasFile('pf_establish_cert_img') && $request->hasFile('esic_register_cert_img') && $request->hasFile('shops_establish_cert_trade_licence_img') &&
//            $request->hasFile('iec_img') && $request->hasFile('msmed_cert_img') && $request->hasFile('startup_cert_img') && $request->hasFile('lower_tax_deduction_cert_img') &&
//            $request->hasFile('tax_exemption_cert_us12a_img') && $request->hasFile('tax_exemption_cert_us80g_img') && $request->hasFile('any_other_details')
//        ){
//
//
//            $masterCompany = MasterCompanyBranch::create([
//
//                'user_id' => $request[''],
//                'parent_company_name' => $request['parent_company_name'],
//                'branch_name' =>$request['branch_name'],
//                'pan_income_tax_img' => $pan_income_tax_img_new_name,
//                'tan_traces_img' => $tan_traces_img_new_name,
//                'tax_exemption_cert_us80g_img' => $tax_exemption_cert_us80g_img_new_name,
//                'tax_exemption_cert_us12a_img' => $tax_exemption_cert_us12a_img_new_name,
//                'lower_tax_deduction_cert_img' => $lower_tax_deduction_cert_img_new_name,
//                'startup_cert_img' => $startup_cert_img_new_name,
//                'msmed_cert_img' => $msmed_cert_img_new_name,
//                'iec_img' => $iec_img_new_name,
//                'shops_establish_cert_trade_licence_img' => $shops_establish_cert_trade_licence_img_new_name,
//                'esic_register_cert_img' => $esic_register_cert_img_new_name,
//                'pf_establish_cert_img' => $pf_establish_cert_img_new_name,
//                'prof_tax_register_cert_img' => $prof_tax_register_cert_img_new_name,
//                'prof_tax_enrollment_cert_img' => $prof_tax_enrollment_cert_img_new_name,
//                'gst_e_com_cert_img' => $gst_e_com_cert_img_new_name,
//                'lut_cert_img' => $lut_cert_img_new_name,
//                'gst_tax_cert_img' => $gst_tax_cert_img_new_name,
//                'articles_of_association_img' => $articles_of_association_img_new_name,
//                'memo_of_association_img' => $memo_of_association_img_new_name,
//                'certificate_of_incorp_img' => $certificate_of_incorp_img_new_name,
//                'any_other_details' => $any_other_details_new_name,
//                'pan_income_tax' => $request['pan_income_tax'],
//                'tan_traces' => $request['tan_traces'],
//                'tax_exemption_cert_us80g' => $request['tax_exemption_cert_us80g'],
//                'tax_exemption_cert_us12a' => $request['tax_exemption_cert_us12a'],
//                'lower_tax_deduction_cert' => $request['lower_tax_deduction_cert'],
//                'startup_cert' => $request['startup_cert'],
//                'msmed_cert' => $request['msmed_cert'],
//                'iec' => $request['iec'],
//                'shops_establish_cert_trade_licence' => $request['shops_establish_cert_trade_licence'],
//                'esic_register_cert' => $request['esic_register_cert'],
//                'pf_establish_cert' => $request['pf_establish_cert'],
//                'prof_tax_register_cert' => $request['prof_tax_register_cert'],
//                'prof_tax_enrollment_cert' => $request['prof_tax_enrollment_cert'],
//                'gst_e_com_cert' => $request['gst_e_com_cert'],
//                'lut_cert' => $request['lut_cert'],
//                'gst_tax_cert' => $request['gst_tax_cert'],
//                'articles_of_association' => $request['articles_of_association'],
//                'memo_of_association' => $request['memo_of_association'],
//                'certificate_of_incorp' => $request['certificate_of_incorp'],
//                'any_other_details_etc' => $request['any_other_details_etc']
//
//            ]);
//
//
//
//        }
//
//
//        $request->session()->flash('success', 'Client Branch  Documents Uploaded successfully.');
//
//        return redirect('admin/masterCompanyUpload');
//
//
//    }

//    public function branchesCompanyStore(){
//
//        $users = User::all();
//        $showAllUser = Branch::all();
//        return view('admin.branchesCompany');
//
//    }
//
//    public function branchesCompany(Request $request){
//
//        $this->validate($request, [
//            'parent_company_name' => ['required', 'string', 'max:255'],
//            'branch_name' => ['required', 'string', 'max:255'],
//            'email' => ['required', 'string', 'email', 'max:255', 'unique:branches'],
//            'mobile' => ['required'],
//            'bank_name' => ['required'],
//            'bank_account' => ['required'],
//            'date_of_incorp' => ['required'],
//            'org_address' => ['required'],
//            'org_website' => ['required'],
//            'spoc_dev'=> ['required']
//        ]);
//
//        $branch = Branch::create([
//            'parent_company_name' => $request['parent_company_name'],
//            'branch_name' => $request['branch_name'],
//            'email' => $request['email'],
//            'mobile' => $request['mobile'],
//            'bank_name' => $request['bank_name'],
//            'bank_account' => $request['bank_account'],
//            'date_of_incorp' => $request['date_of_incorp'],
//            'org_address' => $request['org_address'],
//            'org_website' => $request['org_website'],
//            'spoc_dev' => $request['spoc_dev']
//        ]);
//
//        $request->session()->flash('success', 'Branch Added successfully.');
//
//        return view('admin.branchesCompany');
//    }
//
//    public function  branchComplianceTracker(Request $request){
//
//
//
//            $this->validate($request, [
//                'parent_company_name' => ['required'],
//                'branch_name' => ['required', 'unique:compliance_tracker_branches'],
//                'select_month_year' => ['required'],
//                'due_date' => ['required'],
//                'category' => ['required'],
//                'details' => ['required'],
//                'compliance_date' => ['required'],
//                'documents' => 'required | image|mimes:jpeg,png,jpg,pdf'
//
//            ]);
//
//        $documents_img = $request->documents;
//
//        $documents_img_new_name = time() . $documents_img->getClientOriginalName();
//
//        $documents_img->move(public_path('BranchComplianceTrackersimg'),$documents_img_new_name);
//
//
//        if ($request->hasFile('documents')){
//
//            $complianceTracker = ComplianceTrackerBranch::create([
//                'parent_company_name' => $request['parent_company_name'],
//                'branch_name' => $request['branch_name'],
//                'select_month_year' => $request['select_month_year'],
//                'due_date' => $request['due_date'],
//                'category' => $request['category'],
//                'details' => $request['details'],
//                'compliance_date' => $request['compliance_date'],
//                'documents' => $documents_img_new_name,
//            ]);
//        }
//
//
//        $request->session()->flash('success', 'Compliance Tracker Added successfully.');
//
//        return view('admin.branchComplianceTracker');
//    }
//    public function branchComplianceTrackerStore(){
//        $users = Mastercompanies::all();
//        $showAllUser = Mastercompanies::all();
//
//        return view('admin.branchComplianceTracker')->with('users', $users)->with('showAllUser',$showAllUser);
//    }

    public function adminMasterView(){

        $adminMasterView = MasterData::all();

        return view('admin.adminMasterView')->with('adminMasterView', $adminMasterView);
    }

    public function adminAccountView(){

        $adminAccountView = Account::all();

        return view('admin.adminAccountView')->with('adminAccountView', $adminAccountView);
    }

    public function adminStatutoryView(){

        $adminStatutoryView = Statutory::all();

        return view('admin.adminStatutoryView')->with('adminStatutoryView', $adminStatutoryView);
    }

    public function adminTdsView(){

        $adminTdsView = Tds::all();

        return view('admin.adminTdsView')->with('adminTdsView', $adminTdsView);
    }

    public function adminTdsReturnView(){

        $adminTdsReturnView = TdsReturn::all();

        return view('admin.adminTdsReturnView')->with('adminTdsReturnView', $adminTdsReturnView);
    }

    public function adminGstr1View(){

        $adminGstr1View = Gstr1::all();

        return view('admin.adminGstr1View')->with('adminGstr1View', $adminGstr1View);
    }

    public function adminGstr2aView(){

        $adminGstr2aView = Gstr2a::all();

        return view('admin.adminGstr2aView')->with('adminGstr2aView', $adminGstr2aView);
    }

    public function adminGstr3bView(){

        $adminGstr3bView = Gstr3b::all();

        return view('admin.adminGstr3bView')->with('adminGstr3bView', $adminGstr3bView);
    }

    public function adminGstr9View(){

        $adminGstr9View = Gstr9::all();

        return view('admin.adminGstr9View')->with('adminGstr9View', $adminGstr9View);
    }

    public function adminGstr9cView(){

        $adminGstr9cView = Gstr9c::all();

        return view('admin.adminGstr9cView')->with('adminGstr9cView', $adminGstr9cView);
    }

    public function adminGstChallanView(){

        $adminGstChallanView = GstChallan::all();

        return view('admin.adminGstChallanView')->with('adminGstChallanView', $adminGstChallanView);
    }

    public function adminGstRegistrationView(){

        $adminGstRegistrationView = GstRegistration::all();

        return view('admin.adminGstRegistrationView')->with('adminGstRegistrationView', $adminGstRegistrationView);
    }

    public function adminLutCertificateView(){

        $adminLutCertificateView = LutCertificate::all();

        return view('admin.adminLutCertificateView')->with('adminLutCertificateView', $adminLutCertificateView);
    }

    public function adminNoticeOrderView(){

        $adminNoticeOrderView = NoticeOrderReceived::all();

        return view('admin.adminNoticeOrderView')->with('adminNoticeOrderView', $adminNoticeOrderView);
    }

    public function adminPfReturnView(){

        $adminPfReturnView = PfReturn::all();

        return view('admin.adminPfReturnView')->with('adminPfReturnView', $adminPfReturnView);
    }

    public function adminPfPaymentView(){

        $adminPfPaymentView = PfPayment::all();

        return view('admin.adminPfPaymentView')->with('adminPfPaymentView', $adminPfPaymentView);
    }

    public function adminPfChallanView(){

        $adminPfChallanView = PfChallan::all();

        return view('admin.adminPfChallanView')->with('adminPfChallanView', $adminPfChallanView);
    }

    public function adminEsiReturnView(){

        $adminEsiReturnView = EsiReturn::all();

        return view('admin.adminEsiReturnView')->with('adminEsiReturnView', $adminEsiReturnView);
    }

    public function adminEsiPaymentView(){

        $adminEsiPaymentView = EsiPayment::all();

        return view('admin.adminEsiPaymentView')->with('adminEsiPaymentView', $adminEsiPaymentView);
    }

    public function adminEsiChallanView(){

        $adminEsiChallanView = EsiChallan::all();

        return view('admin.adminEsiChallanView')->with('adminEsiChallanView', $adminEsiChallanView);
    }

    public function adminForm26asView(){

        $adminForm26asView = Form26as::all();

        return view('admin.adminForm26asView')->with('adminForm26asView', $adminForm26asView);
    }

    public function adminPayrollView(){

        $adminPayrollView = Payroll::all();

        return view('admin.adminPayrollView')->with('adminPayrollView', $adminPayrollView);
    }

    public function adminRocComplianceView(){

        $adminRocComplianceView = RocCompliance::all();

        return view('admin.adminRocComplianceView')->with('adminRocComplianceView', $adminRocComplianceView);
    }

    public function adminRocReturnView(){

        $adminRocReturnView = ROCReturns::all();

        return view('admin.adminRocReturnView')->with('adminRocReturnView', $adminRocReturnView);
    }

    public function adminCustomerAgreementView(){

        $adminCustomerAgreementView = Agreement::all();

        return view('admin.adminCustomerAgreementView')->with('adminCustomerAgreementView', $adminCustomerAgreementView);
    }

    public function adminCustomerInvoiceBillView(){

        $adminCustomerInvoiceBillView = Invoicebill::all();

        return view('admin.adminCustomerInvoiceBillView')->with('adminCustomerInvoiceBillView', $adminCustomerInvoiceBillView);
    }

    public function adminVendorAgreementView(){

        $adminVendorAgreementView = VendorAgreement::all();

        return view('admin.adminVendorAgreementView')->with('adminVendorAgreementView', $adminVendorAgreementView);
    }

    public function adminVendorBillView(){

        $adminVendorBillView = Vendorbill::all();

        return view('admin.adminVendorBillView')->with('adminVendorBillView', $adminVendorBillView);
    }

    public function adminEmployeeAgreementView(){

        $adminEmployeeAgreementView = EmployeeAgreement::all();

        return view('admin.adminEmployeeAgreementView')->with('adminEmployeeAgreementView', $adminEmployeeAgreementView);
    }

    public function adminEmployeePayrollView(){

        $adminEmployeePayrollView = EmployeePayroll::all();

        return view('admin.adminEmployeePayrollView')->with('adminEmployeePayrollView', $adminEmployeePayrollView);
    }

    public function adminEmployeeFFDetailView(){

        $adminEmployeeFFDetailView = EmployeeFFdetail::all();

        return view('admin.adminEmployeeFFDetailView')->with('adminEmployeeFFDetailView', $adminEmployeeFFDetailView);
    }

    public function adminEmployeeHRPolicyView(){

        $adminEmployeeHRPolicyView = EmployeeHRPolicy::all();

        return view('admin.adminEmployeeHRPolicyView')->with('adminEmployeeHRPolicyView', $adminEmployeeHRPolicyView);
    }

    public function adminFixedAssetsView(){

        $adminFixedAssetsView = FixedAsset::all();

        return view('admin.adminFixedAssetsView')->with('adminFixedAssetsView', $adminFixedAssetsView);
    }

    public function adminBankReconcilliationView(){

        $adminBankReconcilliationView = BankReconcilliation::all();

        return view('admin.adminBankReconcilliationView')->with('adminBankReconcilliationView', $adminBankReconcilliationView);
    }
}
