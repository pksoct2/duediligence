<?php

namespace App\Http\Controllers;

use App\Account;
use App\Agreement;
use App\BankReconcilliation;
use App\Branch;
use App\ComplianceTracker;
use App\ComplianceTrackerBranch;
use App\EmployeeAgreement;
use App\EmployeeFFdetail;
use App\EmployeeHRPolicy;
use App\EmployeePayroll;
use App\EsiChallan;
use App\EsiPayment;
use App\EsiReturn;
use App\FixedAsset;
use App\Form26as;
use App\GstChallan;
use App\Gstr1;
use App\Gstr2a;
use App\Gstr3b;
use App\Gstr9;
use App\Gstr9c;
use App\GstRegistration;
use App\Invoicebill;
use App\LutCertificate;
use App\Mastercompanies;
use App\MasterCompanyBranch;
use App\MasterData;
use App\NoticeOrderReceived;
use App\Payroll;
use App\PfChallan;
use App\PfPayment;
use App\PfReturn;
use App\QuickInfo;
use App\RocCompliance;
use App\ROCReturns;
use App\Statutory;
use App\Tds;
use App\TdsReturn;
use App\User;
use App\VendorAgreement;
use App\Vendorbill;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function userLogin(Request $request){

        $password = $request->input('password');
        $username = $request->input('username');

        if (Auth::attempt([ 'username'=> $username, 'password'  => $password ])) {
            return redirect("/clientDashboard");
        }else{
            return redirect("/login");
        }
    }

    public function clientDashboard(Request $request){

        if (Auth::user() != null){

            //Getting User information.
            $quickInfoView = QuickInfo::all()->last();
            $users = User::where('id', Auth::user()->id)->first();

            return view("clientDashboard",compact('users'))->with('quickInfoView', $quickInfoView);
        }else{
            return redirect('/');
        }
    }

    public function userSideView(){
        $id = Auth::id();
        $showUserEntry = User::find($id);
        //ini_set('memory_limit', '-1');
        $masterComapnyEntry = Mastercompanies::where('users_name', $showUserEntry['name'])->first();
        //dd($masterComapnyEntry['users_name']);

        return view('userSideView')->with('showUserEntry', $showUserEntry)->with('masterComapnyEntry', $masterComapnyEntry);
    }
//
//    public function clientComplianceTracker(){
//        $id = Auth::id();
//        $showUserEntry = User::find($id);
//        //dd($showUserEntry['name']);
//        $complianceTracker = ComplianceTracker::where('users_name', $showUserEntry['name'])->first();
//
//
//        return view('clientComplianceTracker')->with('complianceTracker', $complianceTracker);
//    }

//    public function clientBranch(){
//        $id = Auth::id();
//        $showUserEntry = User::find($id);
////        $branchAll = Branch::all();
//        $clientBranch = Branch::where('parent_company_name', $showUserEntry['name'])->get();
//        //dd($clientBranch);
//        //$clientBranch = Branch::find();
//
//        return view('clientBranch')->with('clientBranch', $clientBranch);
//
//    }

//    public function clientBranchMasterCompany($company){
//
//        $id = Auth::id();
//        $showUserEntry = User::find($id);
////        $branchAll = Branch::all();
//        $masterBranch = MasterCompanyBranch::where('parent_company_name', $company)->first();
//        //dd($clientBranch);
//        //$clientBranch = Branch::find();
//
//        return view('clientBranchMasterCompany')->with('masterBranch', $masterBranch);
//
//    }

//    public function clientBranchComplianceTracker(){
//
//        $id = Auth::id();
//        $showUserEntry = User::find($id);
////        $branchAll = Branch::all();
//        $complianceTrackers = ComplianceTrackerBranch::where('parent_company_name', $showUserEntry['name'])->get();
//        //dd($clientBranch);
//        //$clientBranch = Branch::find();
//
//        return view('clientBranchComplianceTracker')->with('complianceTrackers', $complianceTrackers);
//    }

    public function quickInfoView(){

        $quickInfoView = QuickInfo::all()->first();

        return view('footer')->with('quickInfoView', $quickInfoView);
    }

    public function master_DataStore(){

        return view('master_Data');

    }

    public function master_Data(Request $request){

        $this->validate($request, [
            'company_name' => ['unique:master_data'],
            'company_prt__llp_individual' => ['required'],
            'date_incorporation' => ['required'],
            'pan' => ['required'],
            'tan' => ['required'],
            'pf_reg' => ['required'],
            'esi_reg' => ['required'],
            'pt_reg' => ['required'],
            'other_applicable_law_reg' => ['required'],
            'authorized_capital' => ['required'],
            'paid_up_capital' => ['required'],
            'reg_office' => ['required'],
            'objects' => ['required'],
            'email' => ['required'],
            'telephone' => ['required'],
            'director_details' => ['required'],
            'shareholder_details' => ['required'],
            'certificate_incorporation' => 'required | image|mimes:jpeg,png,jpg,pdf',
            'aoa' => 'required | image|mimes:jpeg,png,jpg,pdf',
            'moa' => 'required | image|mimes:jpeg,png,jpg,pdf',
            'cin' => 'required | image|mimes:jpeg,png,jpg,pdf'


        ]);

        $certificate_incorporation_img = $request->certificate_incorporation;

        $certificate_incorporation_img_new_name = time() . $certificate_incorporation_img->getClientOriginalName();

        $certificate_incorporation_img->move(public_path('masterData'),$certificate_incorporation_img_new_name);


        $aoa_img = $request->aoa;

        $aoa_img_new_name = time() . $aoa_img->getClientOriginalName();

        $aoa_img->move(public_path('masterData'),$aoa_img_new_name);


        $moa_img = $request->moa;

        $moa_img_new_name = time() . $moa_img->getClientOriginalName();

        $moa_img->move(public_path('masterData'),$moa_img_new_name);


        $cin_img = $request->cin;

        $cin_img_new_name = time() . $cin_img->getClientOriginalName();

        $cin_img->move(public_path('masterData'),$cin_img_new_name);


        if ($request->hasFile('cin') && $request->hasFile('moa') && $request->hasFile('certificate_incorporation') && $request->hasFile('aoa')){

            $user = Auth::user();
            $masterData = MasterData::create([

                'company_name' => $user['name'],
                'company_prt__llp_individual' => $request['company_prt__llp_individual'],
                'date_incorporation' => $request['date_incorporation'],
                'pan' => $request['pan'],
                'tan' => $request['tan'],
                'pf_reg' => $request['pf_reg'],
                'esi_reg' => $request['esi_reg'],
                'pt_reg' => $request['pt_reg'],
                'other_applicable_law_reg' => $request['other_applicable_law_reg'],
                'authorized_capital' => $request['authorized_capital'],
                'paid_up_capital' => $request['paid_up_capital'],
                'reg_office' => $request['reg_office'],
                'objects' => $request['objects'],
                'email' => $request['email'],
                'telephone' => $request['telephone'],
                'director_details' => $request['director_details'],
                'shareholder_details' => $request['shareholder_details'],
                'cin' => $cin_img_new_name,
                'moa' => $moa_img_new_name,
                'certificate_incorporation' => $certificate_incorporation_img_new_name,
                'aoa' => $aoa_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'Master Data Added successfully.');

        return view('master_Data');


    }

    public function accountFileStore(){

        return view('accountFile');
    }

    public function accountFile(Request $request){

        $this->validate($request, [

            'final_year' => ['required'],
            'upload_field' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $upload_field_img = $request->upload_field;

        $upload_field_img_new_name = time() . $upload_field_img->getClientOriginalName();

        $upload_field_img->move(public_path('accountFileUpload'),$upload_field_img_new_name);


        if ($request->hasFile('upload_field')){
            $user = Auth::user();
            $account = Account::create([
                'company_name' => $user['name'],
                'final_year' => $request['final_year'],
                'upload_field' => $upload_field_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'Accounts File Added successfully.');

        return view('accountFile');

    }

    public function masterDataView(){

        $user = Auth::user();

        $masterData = MasterData::where('company_name',$user['name'])->get();

        return view('masterDataView')->with('masterData', $masterData);
    }

    public function accountView(){

        $user = Auth::user();

        $accountData = Account::where('company_name',$user['name'])->get();

        return view('accountView')->with('accountData', $accountData);

    }

    public function editMasterData($id){

        $editData = MasterData::find($id);

        return view('editMasterData')->with('editData', $editData);

    }

    public function updateMasterData($id, Request $request){

        $this->validate($request, [
            'company_prt__llp_individual' => ['required'],
            'date_incorporation' => ['required'],
            'pan' => ['required'],
            'tan' => ['required'],
            'pf_reg' => ['required'],
            'esi_reg' => ['required'],
            'pt_reg' => ['required'],
            'other_applicable_law_reg' => ['required'],
            'authorized_capital' => ['required'],
            'paid_up_capital' => ['required'],
            'reg_office' => ['required'],
            'objects' => ['required'],
            'email' => ['required'],
            'telephone' => ['required'],
            'director_details' => ['required'],
            'shareholder_details' => ['required'],
            'certificate_incorporation' => 'required | image|mimes:jpeg,png,jpg,pdf',
            'aoa' => 'required | image|mimes:jpeg,png,jpg,pdf',
            'moa' => 'required | image|mimes:jpeg,png,jpg,pdf',
            'cin' => 'required | image|mimes:jpeg,png,jpg,pdf'


        ]);


        $certificate_incorporation_img = $request->certificate_incorporation;

        $certificate_incorporation_img_new_name = time() . $certificate_incorporation_img->getClientOriginalName();

        $certificate_incorporation_img->move(public_path('masterData'),$certificate_incorporation_img_new_name);


        $aoa_img = $request->aoa;

        $aoa_img_new_name = time() . $aoa_img->getClientOriginalName();

        $aoa_img->move(public_path('masterData'),$aoa_img_new_name);


        $moa_img = $request->moa;

        $moa_img_new_name = time() . $moa_img->getClientOriginalName();

        $moa_img->move(public_path('masterData'),$moa_img_new_name);


        $cin_img = $request->cin;

        $cin_img_new_name = time() . $cin_img->getClientOriginalName();

        $cin_img->move(public_path('masterData'),$cin_img_new_name);


        $masterData = MasterData::find($id);
        $masterData->company_prt__llp_individual = $request->company_prt__llp_individual;
        $masterData->date_incorporation = $request->date_incorporation;
        $masterData->pan = $request->pan;
        $masterData->tan = $request->tan;
        $masterData->pf_reg = $request->pf_reg;
        $masterData->esi_reg = $request->esi_reg;
        $masterData->pt_reg = $request->pt_reg;
        $masterData->other_applicable_law_reg = $request->other_applicable_law_reg;
        $masterData->authorized_capital = $request->authorized_capital;
        $masterData->paid_up_capital = $request->paid_up_capital;
        $masterData->reg_office = $request->reg_office;
        $masterData->objects = $request->objects;
        $masterData->email = $request->email;
        $masterData->telephone = $request->telephone;
        $masterData->director_details = $request->director_details;
        $masterData->shareholder_details = $request->shareholder_details;
        $masterData->certificate_incorporation = $certificate_incorporation_img_new_name;
        $masterData->aoa = $aoa_img_new_name;
        $masterData->moa = $moa_img_new_name;
        $masterData->cin = $cin_img_new_name;
        $masterData->save();

        $request->session()->flash('success', 'Master Data Updated successfully.');

        return redirect('masterDataView');

    }

    public function editAccountData($id){

        $editData = Account::find($id);

        return view('editAccountData')->with('editData', $editData);
    }

    public function updateAccountData(Request $request, $id){

        $this->validate($request, [

            'final_year' => ['required'],
            'upload_field' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $upload_field_img = $request->upload_field;

        $upload_field_img_new_name = time() . $upload_field_img->getClientOriginalName();

        $upload_field_img->move(public_path('accountFileUpload'),$upload_field_img_new_name);




           $accountUpdate = Account::find($id);

            $accountUpdate->final_year = $request->final_year;
            $accountUpdate->upload_field = $upload_field_img_new_name;
            $accountUpdate->save();



        $request->session()->flash('success', 'Account Data Updated successfully.');

        return redirect('accountView');
    }

    public function masterDataDelete($id, Request $request){

        MasterData::where('id',$id)->delete();

        $request->session()->flash('success', 'The Master Data Deleted Successfully.');

        return redirect()->back();
    }

    public function accountDataDelete($id, Request $request)
    {

        Account::where('id', $id)->delete();

        $request->session()->flash('success', 'The Account Data Deleted Successfully.');

        return redirect()->back();

    }
    public function statutoryAddStore(){

        return view('statutoryAdd');
    }
    public function statutoryAdd(Request $request){

        $this->validate($request, [

            'yes_no' => ['required'],
            'registration_certificate' => 'image|mimes:jpeg,png,jpg,pdf'

        ]);




        if ($request->hasFile('registration_certificate') && $request->yes_no =="yes"){

            $registration_certificate_img = $request->registration_certificate;

            $registration_certificate_img_new_name = time() . $registration_certificate_img->getClientOriginalName();

            $registration_certificate_img->move(public_path('statutory'),$registration_certificate_img_new_name);

            $user = Auth::user();
            $account = Statutory::create([
                'company_name' => $user['name'],
                'yes_no' => 1,
                'registration_certificate' => $registration_certificate_img_new_name,
            ]);
        }else{
            $user = Auth::user();
//            dd($user['name']);
            $account = Statutory::create([
                'company_name' => $user['name'],
                'yes_no' => 0,
                'registration_certificate' => "no image",
            ]);
        }


        $request->session()->flash('success', 'Statutory File Added successfully.');

        return view('statutoryAdd');

    }

    public function statutoryStore(){

        $user = Auth::user();
        $statutoryData = Statutory::where('company_name',$user['name'])->get();

        return view('statutoryView')->with('statutoryData', $statutoryData);
    }

    public function editStatutory($id){

        $statutory = Statutory::find($id);

        return view('editStatutory')->with('statutory', $statutory);
    }

    public function updateStatutory($id, Request $request){

        $this->validate($request, [

            'yes_no' => ['required'],
            'registration_certificate' => 'image|mimes:jpeg,png,jpg,pdf'

        ]);


        if ($request->hasFile('registration_certificate') && $request->yes_no =="yes"){

            $registration_certificate_img = $request->registration_certificate;

            $registration_certificate_img_new_name = time() . $registration_certificate_img->getClientOriginalName();

            $registration_certificate_img->move(public_path('statutory'),$registration_certificate_img_new_name);


            $statutory = Statutory::find($id);
            $statutory->yes_no = 1;
            $statutory->registration_certificate = $registration_certificate_img_new_name;
            $statutory->save();

        }else{
            $statutory = Statutory::find($id);
            $statutory->yes_no = 1;
            $statutory->registration_certificate = "No Image";
            $statutory->save();
        }


        $request->session()->flash('success', 'Statutory File Updated successfully.');

        return view('editStatutory');

    }
    public function tdsFileStore(){

        return view('tdsFile');
    }
    public function tdsFile(Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'tds_challans' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $tds_challans_img = $request->tds_challans;

        $tds_challans_img_new_name = time() . $tds_challans_img->getClientOriginalName();

        $tds_challans_img->move(public_path('Tds'),$tds_challans_img_new_name);


        if ($request->hasFile('tds_challans')){
            $user = Auth::user();
            $tds = Tds::create([
                'company_name' => $user['name'],
                'year_month' => $request['year_month'],
                'tds_challans' => $tds_challans_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'TDS File Added successfully.');

        return view('tdsFile');

    }
    public function tdsFileView(){
        $user = Auth::user();

        $tdsView = Tds::where('company_name',$user['name'])->get();

        return view('tdsFileView')->with('tdsView',$tdsView);
    }

    public function editTdsData($id, Request $request){

        $updateTds = Tds::find($id);

        return view('editTdsData')->with('updateTds',$updateTds);

    }

    public function updateTdsData(Request $request, $id){

        $this->validate($request, [

            'year_month' => ['required'],
            'tds_challans' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $tds_challans_img = $request->tds_challans;

        $tds_challans_img_new_name = time() . $tds_challans_img->getClientOriginalName();

        $tds_challans_img->move(public_path('Tds'),$tds_challans_img_new_name);




        $tdsUpdate = Tds::find($id);

        $tdsUpdate->year_month = $request->year_month;
        $tdsUpdate->tds_challans = $tds_challans_img_new_name;
        $tdsUpdate->save();



        $request->session()->flash('success', 'TDS Data Updated successfully.');

        return redirect('tdsFileView');
    }

    //functions for tds return

    public function tdsReturnStore(){

        return view('tdsReturn');
    }
    public function tdsReturn(Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'Quarter_wise' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $Quarter_wise_img = $request->Quarter_wise;

        $Quarter_wise_img_new_name = time() . $Quarter_wise_img->getClientOriginalName();

        $Quarter_wise_img->move(public_path('TdsReturnImg'),$Quarter_wise_img_new_name);


        if ($request->hasFile('Quarter_wise')){
            $user = Auth::user();
            $tds = TdsReturn::create([
                'company_name' => $user['name'],
                'year_month' => $request['year_month'],
                'Quarter_wise' => $Quarter_wise_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'TDS Return File Added successfully.');

        return view('tdsReturn');

    }
    public function tdsReturnView(){

        $user = Auth::user();

        $tdsReturnView = TdsReturn::where('company_name',$user['name'])->get();

        return view('tdsReturnView')->with('tdsReturnView',$tdsReturnView);
    }

    public function editTdsReturnData($id, Request $request){

        $updateTdsReturn = TdsReturn::find($id);

        return view('editTdsReturn')->with('updateTdsReturn',$updateTdsReturn);

    }

    public function updateTdsReturnData(Request $request, $id){

        $this->validate($request, [

            'year_month' => ['required'],
            'Quarter_wise' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $Quarter_wise_img = $request->Quarter_wise;

        $Quarter_wise_img_new_name = time() . $Quarter_wise_img->getClientOriginalName();

        $Quarter_wise_img->move(public_path('TdsReturnImg'),$Quarter_wise_img_new_name);




        $tdsUpdate = TdsReturn::find($id);

        $tdsUpdate->year_month = $request->year_month;
        $tdsUpdate->Quarter_wise = $Quarter_wise_img_new_name;
        $tdsUpdate->save();



        $request->session()->flash('success', 'TDS Return Data Updated successfully.');

        return redirect('tdsReturnView');
    }
    public function gstr1Store(){

        return view('gstr1File');
    }

    public function gstr1Submit(Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'gstr1' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $gstr1_img = $request->gstr1;

        $gstr1_img_new_name = time() . $gstr1_img->getClientOriginalName();

        $gstr1_img->move(public_path('GSTR1'),$gstr1_img_new_name);


        if ($request->hasFile('gstr1')){
            $user = Auth::user();
            $tds = Gstr1::create([
                'company_name' => $user['name'],
                'year_month' => $request['year_month'],
                'gstr1' => $gstr1_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'GSTR1 File Added successfully.');

        return view('gstr1File');

    }

    public function gstr2aStore(){

        return view('gstr2aFile');
    }

    public function gstr2aSubmit(Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'gstr2a' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $gstr2a_img = $request->gstr2a;

        $gstr2a_img_new_name = time() . $gstr2a_img->getClientOriginalName();

        $gstr2a_img->move(public_path('GSTR2A'),$gstr2a_img_new_name);


        if ($request->hasFile('gstr2a')){
            $user = Auth::user();
            $tds = Gstr2a::create([
                'company_name' => $user['name'],
                'year_month' => $request['year_month'],
                'gstr2a' => $gstr2a_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'GSTR2A File Added successfully.');

        return view('gstr2aFile');
    }

    public function gstr3bStore(){

        return view('gstr3bFile');
    }

    public function gstr3bSubmit(Request $request){


        $this->validate($request, [

            'year_month' => ['required'],
            'gstr3b' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $gstr3b_img = $request->gstr3b;

        $gstr3b_img_new_name = time() . $gstr3b_img->getClientOriginalName();

        $gstr3b_img->move(public_path('GSTR3B'),$gstr3b_img_new_name);


        if ($request->hasFile('gstr3b')){
            $user = Auth::user();
            $tds = Gstr3b::create([
                'company_name' => $user['name'],
                'year_month' => $request['year_month'],
                'gstr3b' => $gstr3b_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'GSTR3B File Added successfully.');

        return view('gstr3bFile');
    }

    public function gstr9cStore(){

        return view('gstr9cFile');
    }

    public function gstr9cSubmit(Request $request){



        $this->validate($request, [

            'year_month' => ['required'],
            'gstr9c' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $gstr9c_img = $request->gstr9c;

        $gstr9c_img_new_name = time() . $gstr9c_img->getClientOriginalName();

        $gstr9c_img->move(public_path('GSTR9C'),$gstr9c_img_new_name);


        if ($request->hasFile('gstr9c')){
            $user = Auth::user();
            $tds = Gstr9c::create([
                'company_name' => $user['name'],
                'year_month' => $request['year_month'],
                'gstr9c' => $gstr9c_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'GSTR9C File Added successfully.');

        return view('gstr9cFile');
    }

    public function gstr9Store(){

        return view('gstr9File');
    }

    public function gstr9Submit(Request $request){


        $this->validate($request, [

            'year_month' => ['required'],
            'gstr9' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $gstr9_img = $request->gstr9;

        $gstr9_img_new_name = time() . $gstr9_img->getClientOriginalName();

        $gstr9_img->move(public_path('GSTR9'),$gstr9_img_new_name);


        if ($request->hasFile('gstr9')){
            $user = Auth::user();
            $tds = Gstr9::create([
                'company_name' => $user['name'],
                'year_month' => $request['year_month'],
                'gstr9' => $gstr9_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'GSTR9 File Added successfully.');

        return view('gstr9File');
    }

    public function gstrChallanStore(){

        return view('gstrChallan');
    }

    public function gstrChallanSubmit(Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'gst_challan' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $gst_challan_img = $request->gst_challan;

        $gst_challan_img_new_name = time() . $gst_challan_img->getClientOriginalName();

        $gst_challan_img->move(public_path('GSTChallan'),$gst_challan_img_new_name);


        if ($request->hasFile('gst_challan')){
            $user = Auth::user();
            $tds = GstChallan::create([
                'company_name' => $user['name'],
                'year_month' => $request['year_month'],
                'gst_challan' => $gst_challan_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'GST Challan File Added successfully.');

        return view('gstrChallan');
    }

    public function gstRegistration(){

        return view('registrationGst');
    }

    public function gstRegistrationSubmit(Request $request){



        $this->validate($request, [

            'year_month' => ['required'],
            'gst_registration' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $gst_registration_img = $request->gst_registration;

        $gst_registration_img_new_name = time() . $gst_registration_img->getClientOriginalName();

        $gst_registration_img->move(public_path('GSTRegistration'),$gst_registration_img_new_name);


        if ($request->hasFile('gst_registration')){
            $user = Auth::user();
            $tds = GstRegistration::create([
                'company_name' => $user['name'],
                'year_month' => $request['year_month'],
                'gst_registration' => $gst_registration_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'GST Registration File Added successfully.');

        return view('registrationGst');

    }

    public function lutCertificateStore(){

        return view('lut_certificate');
    }

    public function lutCertificateSubmit(Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'lut_certificate' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $lut_certificate_img = $request->lut_certificate;

        $lut_certificate_img_new_name = time() . $lut_certificate_img->getClientOriginalName();

        $lut_certificate_img->move(public_path('LUTCertificate'),$lut_certificate_img_new_name);


        if ($request->hasFile('lut_certificate')){
            $user = Auth::user();
            $lut_certification = LutCertificate::create([
                'company_name' => $user['name'],
                'year_month' => $request['year_month'],
                'lut_certificate' => $lut_certificate_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'LUT Certificate File Added successfully.');

        return view('lut_certificate');

    }

    public function notice_orderStore(){

        return view('notice_order');
    }

    public function notice_orderSubmit(Request $request){


        $this->validate($request, [

            'year_month' => ['required'],
            'notice_order' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $notice_order_img = $request->notice_order;

        $notice_order_img_new_name = time() . $notice_order_img->getClientOriginalName();

        $notice_order_img->move(public_path('NoticeOrderImg'),$notice_order_img_new_name);


        if ($request->hasFile('notice_order')){
            $user = Auth::user();
            $notice = NoticeOrderReceived::create([
                'company_name' => $user['name'],
                'year_month' => $request['year_month'],
                'notice_order' => $notice_order_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'Notices/Orders File Added successfully.');

        return view('notice_order');

    }
    public function gstr1FileView(){

        $user = Auth::user();

        $gstr1Data = Gstr1::where('company_name',$user['name'])->get();

        return view('gstr1FileView')->with('gstr1Data', $gstr1Data);
    }

    public function gstr2aFileView(){

        $user = Auth::user();

        $gstr2aData = Gstr2a::where('company_name',$user['name'])->get();

        return view('gstr2aFileView')->with('gstr2aData', $gstr2aData);
    }

    public function gstr3bFileView(){

        $user = Auth::user();

        $gstr3bData = Gstr3b::where('company_name',$user['name'])->get();

        return view('gstr3bFileView')->with('gstr3bData', $gstr3bData);
    }

    public function gstr9FileView(){

        $user = Auth::user();

        $gstr9Data = Gstr9::where('company_name',$user['name'])->get();

        return view('gstr9FileView')->with('gstr9Data', $gstr9Data);
    }

    public function gstr9cFileView(){

        $user = Auth::user();

        $gstr9cData = Gstr9c::where('company_name',$user['name'])->get();

        return view('gstr9cFileView')->with('gstr9cData', $gstr9cData);
    }

    public function gstChallanView(){

        $user = Auth::user();

        $gstChallanData = GstChallan::where('company_name',$user['name'])->get();

        return view('gstChallanView')->with('gstChallanData', $gstChallanData);
    }

    public function gstRegistrationView(){

        $user = Auth::user();

        $gstRegistrationData = GstRegistration::where('company_name',$user['name'])->get();

        return view('gstRegistrationView')->with('gstRegistrationData', $gstRegistrationData);
    }

    public function lutCertificateView(){

        $user = Auth::user();

        $lutCertificateViewData = LutCertificate::where('company_name',$user['name'])->get();

        return view('lutCertificateView')->with('lutCertificateViewData', $lutCertificateViewData);
    }

    public function noticeOrderView(){

        $user = Auth::user();

        $noticeOrderViewData = NoticeOrderReceived::where('company_name',$user['name'])->get();

        return view('noticeOrderView')->with('noticeOrderViewData', $noticeOrderViewData);
    }

    // edit function gstr

    public function editGstr1File($id){

        $updateGstr1 = Gstr1::find($id);

        return view('editGstr1File')->with('updateGstr1',$updateGstr1);
    }

    public function editGstr2aFile($id){

        $updateGstr2a = Gstr2a::find($id);

        return view('editGstr2aFile')->with('updateGstr2a',$updateGstr2a);
    }

    public function editGstr3bFile($id){

        $updateGstr3b = Gstr3b::find($id);

        return view('editGstr3bFile')->with('updateGstr3b',$updateGstr3b);
    }

    public function editGstr9File($id){

        $updateGstr9 = Gstr9::find($id);

        return view('editGstr9File')->with('updateGstr9',$updateGstr9);
    }

    public function editGstr9cFile($id){

        $updateGstr9c = Gstr9c::find($id);

        return view('editGstr9cFile')->with('updateGstr9c',$updateGstr9c);
    }

    public function editGstChallan($id){

        $updateGstChallan = GstChallan::find($id);

        return view('editGstChallan')->with('updateGstChallan',$updateGstChallan);
    }

    public function editGstRegistration($id){

        $updateGstRegistration = Gstr1::find($id);

        return view('editGstRegistration')->with('updateGstRegistration',$updateGstRegistration);
    }

    public function editLutCertificate($id){

        $updateLutCertification = LutCertificate::find($id);

        return view('editLutCertificate')->with('updateLutCertification',$updateLutCertification);
    }

    public function editNoticeOrder($id){

        $updateNoticeOrder = NoticeOrderReceived::find($id);

        return view('editNoticeOrder')->with('updateNoticeOrder',$updateNoticeOrder);
    }

    // update gst and gstr form

    public function updateGstr1File($id, Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'gstr1' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $gstr1_img = $request->gstr1;

        $gstr1_img_new_name = time() . $gstr1_img->getClientOriginalName();

        $gstr1_img->move(public_path('GSTR1'),$gstr1_img_new_name);



        $gstr1 = Gstr1::find($id);

        $gstr1->year_month = $request->year_month;
        $gstr1->gstr1 = $gstr1_img_new_name;
        $gstr1->save();



        $request->session()->flash('success', 'GSTR1 Data Updated successfully.');


        return view('editGstr1File');
    }

    public function updateGstr2aFile($id, Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'gstr2a' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $gstr2a_img = $request->gstr2a;

        $gstr2a_img_new_name = time() . $gstr2a_img->getClientOriginalName();

        $gstr2a_img->move(public_path('GSTR2A'),$gstr2a_img_new_name);



        $gstr2a = Gstr2a::find($id);

        $gstr2a->year_month = $request->year_month;
        $gstr2a->gstr2a = $gstr2a_img_new_name;
        $gstr2a->save();



        $request->session()->flash('success', 'GSTR2A Data Updated successfully.');

        return view('editGstr2aFile');
    }

    public function updateGstr3bFile($id, Request $request){


        $this->validate($request, [

            'year_month' => ['required'],
            'gstr3b' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $gstr3b_img = $request->gstr3b;

        $gstr3b_img_new_name = time() . $gstr3b_img->getClientOriginalName();

        $gstr3b_img->move(public_path('GSTR3B'),$gstr3b_img_new_name);



        $gstr3b = Gstr3b::find($id);

        $gstr3b->year_month = $request->year_month;
        $gstr3b->gstr3b = $gstr3b_img_new_name;
        $gstr3b->save();



        $request->session()->flash('success', 'GSTR3B Data Updated successfully.');

        return view('editGstr3bFile');
    }

    public function updateGstr9File($id, Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'gstr9' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $gstr9_img = $request->gstr9;

        $gstr9_img_new_name = time() . $gstr9_img->getClientOriginalName();

        $gstr9_img->move(public_path('GSTR9'),$gstr9_img_new_name);



        $gstr9 = Gstr9::find($id);

        $gstr9->year_month = $request->year_month;
        $gstr9->gstr9 = $gstr9_img_new_name;
        $gstr9->save();



        $request->session()->flash('success', 'GSTR9 Data Updated successfully.');

        return view('editGstr9File');
    }

    public function updateGstr9cFile($id, Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'gstr9c' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $gstr9c_img = $request->gstr9c;

        $gstr9c_img_new_name = time() . $gstr9c_img->getClientOriginalName();

        $gstr9c_img->move(public_path('GSTR9'),$gstr9c_img_new_name);



        $gstr9c = Gstr9c::find($id);

        $gstr9c->year_month = $request->year_month;
        $gstr9c->gstr9c = $gstr9c_img_new_name;
        $gstr9c->save();



        $request->session()->flash('success', 'GSTR9C Data Updated successfully.');


        return view('editGstr9cFile');
    }

    public function updateGstChallan($id, Request $request){


        $this->validate($request, [

            'year_month' => ['required'],
            'gst_challan' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $gst_challan_img = $request->gstr9c;

        $gst_challan_img_new_name = time() . $gst_challan_img->getClientOriginalName();

        $gst_challan_img->move(public_path('GSTChallan'),$gst_challan_img_new_name);



        $gst_challan = GstChallan::find($id);

        $gst_challan->year_month = $request->year_month;
        $gst_challan->gst_challan = $gst_challan_img_new_name;
        $gst_challan->save();



        $request->session()->flash('success', 'GST Challan Data Updated successfully.');

        return view('editGstChallan');
    }

    public function updateGstRegistration($id, Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'gst_registration' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $gst_registration_img = $request->gst_registration;

        $gst_registration_img_new_name = time() . $gst_registration_img->getClientOriginalName();

        $gst_registration_img->move(public_path('GSTRegistration'),$gst_registration_img_new_name);



        $gst_registration = GstRegistration::find($id);

        $gst_registration->year_month = $request->year_month;
        $gst_registration->gst_registration = $gst_registration_img_new_name;
        $gst_registration->save();



        $request->session()->flash('success', 'GST Registration Data Updated successfully.');

        return view('editGstRegistration');
    }

    public function updateLutCertificate($id, Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'lut_certificate' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $lut_certificate_img = $request->lut_certificate;

        $lut_certificate_img_new_name = time() . $lut_certificate_img->getClientOriginalName();

        $lut_certificate_img->move(public_path('LUTCertificate'),$lut_certificate_img_new_name);



        $lut_certificate = GstRegistration::find($id);

        $lut_certificate->year_month = $request->year_month;
        $lut_certificate->lut_certificate = $lut_certificate_img_new_name;
        $lut_certificate->save();



        $request->session()->flash('success', 'LUT Certificate Data Updated successfully.');

        return view('editLutCertificate');
    }

    public function updateNoticeOrder($id, Request $request){


        $this->validate($request, [

            'year_month' => ['required'],
            'notice_order' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $notice_order_img = $request->notice_order;

        $notice_order_img_new_name = time() . $notice_order_img->getClientOriginalName();

        $notice_order_img->move(public_path('LUTCertificate'),$notice_order_img_new_name);



        $notice_order = GstRegistration::find($id);

        $notice_order->year_month = $request->year_month;
        $notice_order->notice_order = $notice_order_img_new_name;
        $notice_order->save();



        $request->session()->flash('success', 'Notice Order Data Updated successfully.');

        return view('editNoticeOrder');
    }

    // pf upload data form

    public function pfReturnStore(){

        return view('pfReturnFile');
    }

    public function pfChallanStore(){

        return view('pfChallanFile');
    }

    public function pfPaymentStore(){

        return view('pfPaymentFile');
    }

    public function esiReturnStore(){

        return view('esiReturnFile');
    }

    public function esiChallanStore(){

        return view('esiChallanFile');
    }

    public function esiPaymentStore(){

        return view('esiPaymentFile');
    }
    public function payrollFileStore(){

            return view('payrollFile');
    }
    public function form26asFileStore(){

            return view('form26asFile');
    }

    //For PF data submit

    public function pfReturnSubmit(Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'pf_return' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $pf_return_img = $request->pf_return;

        $pf_return_img_new_name = time() . $pf_return_img->getClientOriginalName();

        $pf_return_img->move(public_path('pfReturn'),$pf_return_img_new_name);


        if ($request->hasFile('pf_return')){
            $user = Auth::user();
            $tds = PfReturn::create([
                'company_name' => $user['name'],
                'year_month' => $request['year_month'],
                'pf_return' => $pf_return_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'PF Return File Added successfully.');


        return view('pfReturnFile');
    }

    public function pfChallanSubmit(Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'pf_challan' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $pf_challan_img = $request->pf_challan;

        $pf_challan_img_new_name = time() . $pf_challan_img->getClientOriginalName();

        $pf_challan_img->move(public_path('pfChallan'),$pf_challan_img_new_name);


        if ($request->hasFile('pf_challan')){
            $user = Auth::user();
            $tds = PfChallan::create([
                'company_name' => $user['name'],
                'year_month' => $request['year_month'],
                'pf_challan' => $pf_challan_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'PF Challan File Added successfully.');

        return view('pfChallanFile');
    }

    public function pfPaymentSubmit(Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'pf_payment' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $pf_payment_img = $request->pf_payment;

        $pf_payment_img_new_name = time() . $pf_payment_img->getClientOriginalName();

        $pf_payment_img->move(public_path('pfPayment'),$pf_payment_img_new_name);


        if ($request->hasFile('pf_payment')){
            $user = Auth::user();
            $tds = PfPayment::create([
                'company_name' => $user['name'],
                'year_month' => $request['year_month'],
                'pf_payment' => $pf_payment_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'PF Payment File Added successfully.');

        return view('pfPaymentFile');
    }

    public function esiReturnSubmit(Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'esi_return' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $esi_return_img = $request->esi_return;

        $esi_return_img_new_name = time() . $esi_return_img->getClientOriginalName();

        $esi_return_img->move(public_path('esiReturn'),$esi_return_img_new_name);


        if ($request->hasFile('esi_return')){
            $user = Auth::user();
            $tds = EsiReturn::create([
                'company_name' => $user['name'],
                'year_month' => $request['year_month'],
                'esi_return' => $esi_return_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'ESI Return File Added successfully.');

        return view('esiReturnFile');
    }

    public function esiChallanSubmit(Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'esi_challan' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $esi_challan_img = $request->esi_challan;

        $esi_challan_img_new_name = time() . $esi_challan_img->getClientOriginalName();

        $esi_challan_img->move(public_path('esiChallan'),$esi_challan_img_new_name);


        if ($request->hasFile('esi_challan')){
            $user = Auth::user();
            $tds = EsiChallan::create([
                'company_name' => $user['name'],
                'year_month' => $request['year_month'],
                'esi_challan' => $esi_challan_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'ESI Challan File Added successfully.');

        return view('esiChallanFile');
    }

    public function esiPaymentSubmit(Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'esi_payment' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $esi_payment_img = $request->esi_payment;

        $esi_payment_img_new_name = time() . $esi_payment_img->getClientOriginalName();

        $esi_payment_img->move(public_path('esiPayment'),$esi_payment_img_new_name);


        if ($request->hasFile('esi_payment')){
            $user = Auth::user();
            $tds = EsiPayment::create([
                'company_name' => $user['name'],
                'year_month' => $request['year_month'],
                'esi_payment' => $esi_payment_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'ESI Payment File Added successfully.');

        return view('esiPaymentFile');
    }
    public function payrollSubmit(Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'payroll' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $payroll_img = $request->payroll;

        $payroll_img_new_name = time() . $payroll_img->getClientOriginalName();

        $payroll_img->move(public_path('payroll'),$payroll_img_new_name);


        if ($request->hasFile('payroll')){
            $user = Auth::user();
            $tds = Payroll::create([
                'company_name' => $user['name'],
                'year_month' => $request['year_month'],
                'payroll' => $payroll_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'Payroll File Added successfully.');

        return view('payrollFile');
    }
    public function form26asSubmit(Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'form26as' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $form26as_img = $request->form26as;

        $form26as_img_new_name = time() . $form26as_img->getClientOriginalName();

        $form26as_img->move(public_path('form26as'),$form26as_img_new_name);


        if ($request->hasFile('form26as')){
            $user = Auth::user();
            $tds = Form26as::create([
                'company_name' => $user['name'],
                'year_month' => $request['year_month'],
                'form26as' => $form26as_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'Form26AS File Added successfully.');

        return view('form26asFile');
    }

    // for view pf and esi data

    public function pfReturnView(){

        $user = Auth::user();

        $pfReturn = PfReturn::where('company_name',$user['name'])->get();

        return view('pfReturnView')->with('pfReturn', $pfReturn);
    }

    public function pfChallanView(){

        $user = Auth::user();

        $pfChallan = PfChallan::where('company_name',$user['name'])->get();

        return view('pfChallanView')->with('pfChallan', $pfChallan);
    }

    public function pfPaymentView(){

        $user = Auth::user();

        $pfPayment =PfPayment::where('company_name',$user['name'])->get();

        return view('pfPaymentView')->with('pfPayment', $pfPayment);
    }

    public function esiReturnView(){

        $user = Auth::user();

        $esiReturn = EsiReturn::where('company_name',$user['name'])->get();


        return view('esiReturnView')->with('esiReturn', $esiReturn);
    }

    public function esiChallanView(){

        $user = Auth::user();

        $esiChallan = EsiChallan::where('company_name',$user['name'])->get();

        return view('esiChallanView')->with('esiChallan', $esiChallan);
    }

    public function esiPaymentView(){

        $user = Auth::user();

        $esiPayment = EsiPayment::where('company_name',$user['name'])->get();

        return view('esiPaymentView')->with('esiPayment', $esiPayment);
    }
    public function payrollView(){

        $user = Auth::user();

        $payroll = Payroll::where('company_name',$user['name'])->get();

        return view('payrollView')->with('payroll',$payroll);
    }
    public function form26asView(){

        $user = Auth::user();

        $form26as = Form26as::where('company_name',$user['name'])->get();

        return view('form26asView')->with('form26as',$form26as);
    }

    // edit section

    public function editPfReturnStore($id){

        $pfReturnUpdate = PfReturn::find($id);

        return view('editPfReturnStore')->with('pfReturnUpdate',$pfReturnUpdate);
    }

    public function editPfChallanStore($id){

        $pfChallanUpdate = PfChallan::find($id);

        return view('editPfChallanStore')->with('pfChallanUpdate',$pfChallanUpdate);
    }

    public function editPfPaymentStore($id){

        $pfPaymentUpdate = PfPayment::find($id);

        return view('editPfPaymentStore')->with('pfPaymentUpdate',$pfPaymentUpdate);
    }

    public function editEsiReturnStore($id){

        $esiReturnUpdate = EsiReturn::find($id);

        return view('editEsiReturnStore')->with('esiReturnUpdate',$esiReturnUpdate);
    }

    public function editEsiChallanStore($id){

        $esiChallanUpdate = EsiChallan::find($id);

        return view('editEsiChallanStore')->with('esiChallanUpdate',$esiChallanUpdate);
    }

    public function editEsiPaymentStore($id){

        $esiPaymentUpdate = EsiPayment::find($id);

        return view('editEsiPaymentStore')->with('esiPaymentUpdate',$esiPaymentUpdate);
    }

    public function editPayrollStore($id){

        $payrollUpdate = Payroll::find($id);

        return view('editPayrollStore')->with('payrollUpdate',$payrollUpdate);
    }

    public function editForm26asStore($id){

        $form26asUpdate = Form26as::find($id);

        return view('editForm26asStore')->with('form26asUpdate',$form26asUpdate);
    }

    //update pf and esi

    public function updatePfReturn($id, Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'pf_return' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $pf_return_img = $request->pf_return;

        $pf_return_img_new_name = time() . $pf_return_img->getClientOriginalName();

        $pf_return_img->move(public_path('pfReturn'),$pf_return_img_new_name);

        $pf_return = PfReturn::find($id);

        $pf_return->year_month = $request->year_month;
        $pf_return->pf_return = $pf_return_img_new_name;
        $pf_return->save();

        $request->session()->flash('success', 'PF Return Data Updated successfully.');


        return view('editPfReturnStore');
    }

    public function updatePfChallan($id, Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'pf_challan' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $pf_challan_img = $request->pf_challan;

        $pf_challan_img_new_name = time() . $pf_challan_img->getClientOriginalName();

        $pf_challan_img->move(public_path('pfChallan'),$pf_challan_img_new_name);

        $pf_challan = PfChallan::find($id);

        $pf_challan->year_month = $request->year_month;
        $pf_challan->pf_challan = $pf_challan_img_new_name;
        $pf_challan->save();

        $request->session()->flash('success', 'PF Challan Data Updated successfully.');


        return view('editPfChallanStore');
    }

    public function updatePfPayment($id, Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'pf_payment' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $pf_payment_img = $request->pf_payment;

        $pf_payment_img_new_name = time() . $pf_payment_img->getClientOriginalName();

        $pf_payment_img->move(public_path('pfPayment'),$pf_payment_img_new_name);

        $pf_payment = PfPayment::find($id);

        $pf_payment->year_month = $request->year_month;
        $pf_payment->pf_payment = $pf_payment_img_new_name;
        $pf_payment->save();

        $request->session()->flash('success', 'PF Payment Data Updated successfully.');


        return view('editPfPaymentStore');
    }

    public function updateEsiReturn($id, Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'esi_return' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $esi_return_img = $request->esi_return;

        $esi_return_img_new_name = time() . $esi_return_img->getClientOriginalName();

        $esi_return_img->move(public_path('esiReturn'),$esi_return_img_new_name);

        $esi_return = EsiReturn::find($id);

        $esi_return->year_month = $request->year_month;
        $esi_return->esi_return = $esi_return_img_new_name;
        $esi_return->save();

        $request->session()->flash('success', 'ESI Return Data Updated successfully.');


        return view('editEsiReturnStore');
    }

    public function updateEsiChallan($id, Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'esi_challan' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $esi_challan_img = $request->esi_challan;

        $esi_challan_img_new_name = time() . $esi_challan_img->getClientOriginalName();

        $esi_challan_img->move(public_path('esiChallan'),$esi_challan_img_new_name);

        $esi_challan = EsiChallan::find($id);

        $esi_challan->year_month = $request->year_month;
        $esi_challan->esi_challan = $esi_challan_img_new_name;
        $esi_challan->save();

        $request->session()->flash('success', 'ESI Challan Data Updated successfully.');


        return view('editEsiChallanStore');
    }

    public function updateEsiPayment($id, Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'esi_payment' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $esi_payment_img = $request->esi_payment;

        $esi_payment_img_new_name = time() . $esi_payment_img->getClientOriginalName();

        $esi_payment_img->move(public_path('esiPayment'),$esi_payment_img_new_name);

        $esi_payment = EsiPayment::find($id);

        $esi_payment->year_month = $request->year_month;
        $esi_payment->esi_payment = $esi_payment_img_new_name;
        $esi_payment->save();

        $request->session()->flash('success', 'ESI Payment Data Updated successfully.');

        return view('editEsiPaymentStore');
    }

    public function updatePayroll($id, Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'payroll' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $payroll_img = $request->payroll;

        $payroll_img_new_name = time() . $payroll_img->getClientOriginalName();

        $payroll_img->move(public_path('payroll'),$payroll_img_new_name);

        $payroll = Payroll::find($id);

        $payroll->year_month = $request->year_month;
        $payroll->payroll = $payroll_img_new_name;
        $payroll->save();

        $request->session()->flash('success', 'Payroll Data Updated successfully.');


        return view('editPayrollStore');
    }

    public function updateForm26as($id, Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'form26as' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $form26as_img = $request->form26as;

        $form26as_img_new_name = time() . $form26as_img->getClientOriginalName();

        $form26as_img->move(public_path('form26as'),$form26as_img_new_name);

        $form26as = Form26as::find($id);

        $form26as->year_month = $request->year_month;
        $form26as->form26as = $form26as_img_new_name;
        $form26as->save();

        $request->session()->flash('success', 'Form26AS Data Updated successfully.');


        return view('editForm26asStore');
    }

    // roc compliance function form data entry

    public function rocCompliance(){

        return view('ROCCompliance');
    }

    public function rocReturn(){

        return view('rocReturn');
    }

    // roc compliance data store

    public function rocComplianceSubmit(Request $request){

        $this->validate($request, [

            'type_meeting' => ['required'],
            'date_meeting' => ['required'],
            'agenda' => ['required'],
            'attendance' => ['required'],
            'remarks' => ['required'],

        ]);


            $user = Auth::user();
            $tds = RocCompliance::create([
                'company_name' => $user['name'],
                'type_meeting' => $request['type_meeting'],
                'date_meeting' => $request['date_meeting'],
                'agenda' => $request['agenda'],
                'attendance' => $request['attendance'],
                'remarks' => $request['remarks'],

            ]);



        $request->session()->flash('success', 'Roc Compliance File Added successfully.');


        return view('ROCCompliance');
    }

    public function rocReturnSubmit(Request $request){

        $this->validate($request, [

            'aoc4' => ['required'],
            'mgt7' => ['required'],
            'srn' => ['required'],
            'date_filing' => ['required']
        ]);


        $user = Auth::user();
        $tds = ROCReturns::create([
            'company_name' => $user['name'],
            'aoc4' => $request['aoc4'],
            'mgt7' => $request['mgt7'],
            'srn' => $request['srn'],
            'date_filing' => $request['date_filing'],
        ]);



        $request->session()->flash('success', 'Roc Return Added successfully.');

        return view('rocReturn');
    }

    public function rocReturnView(){

        $user = Auth::user();

        $rocReturn = ROCReturns::where('company_name',$user['name'])->get();

        return view('rocReturnView')->with('rocReturn',$rocReturn);
    }

    public function ROCComplianceView(){

        $user = Auth::user();

        $rocCompliance = RocCompliance::where('company_name',$user['name'])->get();

        return view('ROCComplianceView')->with('rocCompliance',$rocCompliance);
    }

    //roc compliance edit

    public function editROCCompliance($id){

        $updateRoc = RocCompliance::find($id);

        return view('editROCCompliance')->with('updateRoc',$updateRoc);
    }

    public function editRocReturn($id){

        $updateRoc = ROCReturns::find($id);

        return view('editRocReturn')->with('updateRoc',$updateRoc);
    }

    // update function

    public function updateROCCompliance(Request $request, $id){

        $this->validate($request, [

            'type_meeting' => ['required'],
            'date_meeting' => ['required'],
            'agenda' => ['required'],
            'attendance' => ['required'],
            'remarks' => ['required'],

        ]);

        $rocCompliance = RocCompliance::find($id);

        $rocCompliance->type_meeting = $request['type_meeting'];
        $rocCompliance->date_meeting = $request['date_meeting'];
        $rocCompliance->agenda = $request['agenda'];
        $rocCompliance->attendance = $request['attendance'];
        $rocCompliance->remarks = $request['remarks'];

        $rocCompliance->save();

        $request->session()->flash('success', 'Roc Compliance Updated successfully.');

        return view('editROCCompliance');
    }

    public function updateRocReturn(Request $request, $id){

        $this->validate($request, [

            'aoc4' => ['required'],
            'mgt7' => ['required'],
            'srn' => ['required'],
            'date_filing' => ['required']
        ]);

        $rocReturn = ROCReturns::find($id);

        $rocReturn->aoc4 = $request['aoc4'];
        $rocReturn->mgt7 = $request['mgt7'];
        $rocReturn->srn = $request['srn'];
        $rocReturn->date_filing = $request['date_filing'];
        $rocReturn->save();

        $request->session()->flash('success', 'Roc Return Updated successfully.');

        return view('editRocReturn');

    }

    //customer data

    public function customerAgreement(){

        return view('customerAgreement');
    }

    public function customerInvoiceBill(){

        return view('customerInvoiceBill');
    }

    public function employeeAgreement(){

        return view('employeeAgreement');
    }

    public function employeeFFDetail(){

        return view('employeeFFDetail');
    }

    public function employeeHRPolicy(){

        return view('employeeHRPolicy');
    }

    public function employeePayroll(){

        return view('employeePayroll');
    }

    public function fixedAssetsRegister(){

        return view('fixedAssetsRegister');
    }

    public function bankReconcilliation(){

        return view('bankReconcilliation');
    }

    public function vendorAgreement(){

        return view('vendorAgreement');
    }

    public function vendorBill(){

        return view('vendorBill');
    }

    // customer and employee data store

    public function customerAgreementSubmit(Request $request){

        $this->validate($request, [

            'customer_agreement' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $customer_agreement_img = $request->customer_agreement;

        $customer_agreement_img_new_name = time() . $customer_agreement_img->getClientOriginalName();

        $customer_agreement_img->move(public_path('customerAgreementImg'),$customer_agreement_img_new_name);


        if ($request->hasFile('customer_agreement')){
            $user = Auth::user();
            $tds = Agreement::create([
                'company_name' => $user['name'],
                'customer_agreement' => $customer_agreement_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'Customer Agreement File Added successfully.');

        return view('customerAgreement');
    }

    public function customerInvoiceBillSubmit(Request $request){

        $this->validate($request, [

            'customer_invoice_bill' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $customer_invoice_bill_img = $request->customer_invoice_bill;

        $customer_invoice_bill_img_new_name = time() . $customer_invoice_bill_img->getClientOriginalName();

        $customer_invoice_bill_img->move(public_path('customerInvoiceBillImg'),$customer_invoice_bill_img_new_name);


        if ($request->hasFile('customer_invoice_bill')){
            $user = Auth::user();
            $tds = Invoicebill::create([
                'company_name' => $user['name'],
                'customer_invoice_bill' => $customer_invoice_bill_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'Customer Invoice Bill File Added successfully.');


        return view('customerInvoiceBill');
    }

    public function employeeAgreementSubmit(Request $request){

        $this->validate($request, [

            'employee_agreement' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $employee_agreement_img = $request->employee_agreement;

        $employee_agreement_img_new_name = time() . $employee_agreement_img->getClientOriginalName();

        $employee_agreement_img->move(public_path('employeeAgreementImg'),$employee_agreement_img_new_name);


        if ($request->hasFile('employee_agreement')){
            $user = Auth::user();
            $tds = EmployeeAgreement::create([
                'company_name' => $user['name'],
                'employee_agreement' => $employee_agreement_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'Employee Agreement File Added successfully.');


        return view('employeeAgreement');
    }

    public function employeeFFDetailSubmit(Request $request){

        $this->validate($request, [

            'employee_ffdetails' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $employee_ffdetails_img = $request->employee_ffdetails;

        $employee_ffdetails_img_new_name = time() . $employee_ffdetails_img->getClientOriginalName();

        $employee_ffdetails_img->move(public_path('employeeFFD'),$employee_ffdetails_img_new_name);


        if ($request->hasFile('employee_ffdetails')){
            $user = Auth::user();
            $tds = EmployeeFFdetail::create([
                'company_name' => $user['name'],
                'employee_ffdetails' => $employee_ffdetails_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'Employee F&F details File Added successfully.');


        return view('employeeFFDetail');
    }

    public function employeeHRPolicySubmit(Request $request){

        $this->validate($request, [

            'employee_h_r_policy' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $employee_h_r_policy_img = $request->employee_h_r_policy;

        $employee_h_r_policy_img_new_name = time() . $employee_h_r_policy_img->getClientOriginalName();

        $employee_h_r_policy_img->move(public_path('employeeHRPlocy'),$employee_h_r_policy_img_new_name);


        if ($request->hasFile('employee_h_r_policy')){
            $user = Auth::user();
            $tds = EmployeeHRPolicy::create([
                'company_name' => $user['name'],
                'employee_h_r_policy' => $employee_h_r_policy_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'Employee HR Policy File Added successfully.');


        return view('employeeHRPolicy');
    }

    public function employeePayrollSubmit(Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'employee_payroll' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $employee_payroll_img = $request->employee_payroll;

        $employee_payroll_img_new_name = time() . $employee_payroll_img->getClientOriginalName();

        $employee_payroll_img->move(public_path('employeePayrollImg'),$employee_payroll_img_new_name);


        if ($request->hasFile('employee_payroll')){
            $user = Auth::user();
            $tds = EmployeePayroll::create([
                'company_name' => $user['name'],
                'year_month' => $request['year_month'],
                'employee_payroll' => $employee_payroll_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'Employee Payroll File Added successfully.');


        return view('employeePayroll');
    }

    public function fixedAssetsRegisterSubmit(Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'fixed_asset' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $fixed_asset_img = $request->fixed_asset;

        $fixed_asset_img_new_name = time() . $fixed_asset_img->getClientOriginalName();

        $fixed_asset_img->move(public_path('FixedAssets'),$fixed_asset_img_new_name);


        if ($request->hasFile('fixed_asset')){
            $user = Auth::user();
            $tds = FixedAsset::create([
                'company_name' => $user['name'],
                'year_month' => $request['year_month'],
                'fixed_asset' => $fixed_asset_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'Fixed Assets File Added successfully.');


        return view('fixedAssetsRegister');
    }

    public function bankReconcilliationSubmit(Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'bank_reconcilliation' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $bank_reconcilliation_img = $request->bank_reconcilliation;

        $bank_reconcilliation_img_new_name = time() . $bank_reconcilliation_img->getClientOriginalName();

        $bank_reconcilliation_img->move(public_path('BankReconcilliationImg'),$bank_reconcilliation_img_new_name);


        if ($request->hasFile('bank_reconcilliation')){
            $user = Auth::user();
            $tds = BankReconcilliation::create([
                'company_name' => $user['name'],
                'year_month' => $request['year_month'],
                'bank_reconcilliation' => $bank_reconcilliation_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'Bank Reconcilliation File Added successfully.');


        return view('bankReconcilliation');
    }

    public function vendorAgreementSubmit(Request $request){

        $this->validate($request, [

            'vendor_agreement' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $vendor_agreement_img = $request->vendor_agreement;

        $vendor_agreement_img_new_name = time() . $vendor_agreement_img->getClientOriginalName();

        $vendor_agreement_img->move(public_path('vendorAgreementImg'),$vendor_agreement_img_new_name);


        if ($request->hasFile('vendor_agreement')){
            $user = Auth::user();
            $tds = VendorAgreement::create([
                'company_name' => $user['name'],
                'vendor_agreement' => $vendor_agreement_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'Vendor Agreement File Added successfully.');



        return view('vendorAgreement');
    }

    public function vendorBillSubmit(Request $request){

        $this->validate($request, [

            'vendor_bill' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $vendor_bill_img = $request->vendor_bill;

        $vendor_bill_img_new_name = time() . $vendor_bill_img->getClientOriginalName();

        $vendor_bill_img->move(public_path('vendorBillImg'),$vendor_bill_img_new_name);


        if ($request->hasFile('vendor_bill')){
            $user = Auth::user();
            $tds = Vendorbill::create([
                'company_name' => $user['name'],
                'vendor_bill' => $vendor_bill_img_new_name,
            ]);
        }


        $request->session()->flash('success', 'Vendor Bills  File Added successfully.');


        return view('vendorBill');
    }

    // customer and employee view functions

    public function customerAgreementView(){

        $user = Auth::user();

        $customerAgreement = Agreement::where('company_name',$user['name'])->get();

        return view('customerAgreementView')->with('customerAgreement',$customerAgreement);
    }

    public function customerInvoiceBillView(){

        $user = Auth::user();

        $customerInvoiceBill = Invoicebill::where('company_name',$user['name'])->get();

        return view('customerInvoiceBillView')->with('customerInvoiceBill', $customerInvoiceBill);
    }

    public function employeeAgreementView(){

        $user = Auth::user();

        $employeeAgreement = EmployeeAgreement::where('company_name',$user['name'])->get();

        return view('employeeAgreementView')->with('employeeAgreement', $employeeAgreement);
    }

    public function employeeFFDetailView(){

        $user = Auth::user();

        $employeeFFDetail = EmployeeFFdetail::where('company_name',$user['name'])->get();

        return view('employeeFFDetailView')->with('employeeFFDetail', $employeeFFDetail);
    }

    public function employeeHRPolicyView(){

        $user = Auth::user();

        $employeeHRPolicy = EmployeeHRPolicy::where('company_name',$user['name'])->get();

        return view('employeeHRPolicyView')->with('employeeHRPolicy', $employeeHRPolicy);
    }

    public function employeePayrollView(){

        $user = Auth::user();

        $employeePayroll = EmployeePayroll::where('company_name',$user['name'])->get();

        return view('employeePayrollView')->with('employeePayroll', $employeePayroll);
    }

    public function fixedAssetsRegisterView(){

        $user = Auth::user();

        $fixedAssetsRegister = FixedAsset::where('company_name',$user['name'])->get();

        return view('fixedAssetsRegisterView')->with('fixedAssetsRegister', $fixedAssetsRegister);
    }

    public function bankReconcilliationView(){

        $user = Auth::user();

        $bankReconcilliation = BankReconcilliation::where('company_name',$user['name'])->get();

        return view('bankReconcilliationView')->with('bankReconcilliation', $bankReconcilliation);
    }

    public function vendorAgreementView(){

        $user = Auth::user();

        $vendorAgreement = VendorAgreement::where('company_name',$user['name'])->get();

        return view('vendorAgreementView')->with('vendorAgreement', $vendorAgreement);
    }

    public function vendorBillView(){

        $user = Auth::user();

        $vendorBill = Vendorbill::where('company_name',$user['name'])->get();

        return view('vendorBillView')->with('vendorBill',$vendorBill);
    }

    //edit customer and employee data

    public function customerAgreementEdit($id){

        $customerAgreement = Agreement::find($id);

        return view('customerAgreementEdit')->with('customerAgreement',$customerAgreement);
    }

    public function customerInvoiceBillEdit($id){

        $customerInvoiceBill = Invoicebill::find($id);

        return view('customerInvoiceBillEdit')->with('customerInvoiceBill', $customerInvoiceBill);
    }

    public function employeeAgreementEdit($id){

        $employeeAgreement = EmployeeAgreement::find($id);

        return view('employeeAgreementEdit')->with('employeeAgreement', $employeeAgreement);
    }

    public function employeeFFDetailEdit($id){

        $employeeFFDetail = EmployeeFFdetail::find($id);

        return view('employeeFFDetailEdit')->with('employeeFFDetail', $employeeFFDetail);
    }

    public function employeeHRPolicyEdit($id){

        $employeeHRPolicy = EmployeeHRPolicy::find($id);

        return view('employeeHRPolicyEdit')->with('employeeHRPolicy', $employeeHRPolicy);
    }

    public function employeePayrollEdit($id){

        $employeePayroll = EmployeePayroll::find($id);

        return view('employeePayrollEdit')->with('employeePayroll', $employeePayroll);
    }

    public function fixedAssetsRegisterEdit($id){

        $fixedAssetsRegister = FixedAsset::find($id);

        return view('fixedAssetsRegisterEdit')->with('fixedAssetsRegister', $fixedAssetsRegister);
    }

    public function bankReconcilliationEdit($id){

        $bankReconcilliation = BankReconcilliation::find($id);

        return view('bankReconcilliationEdit')->with('bankReconcilliation', $bankReconcilliation);
    }

    public function vendorAgreementEdit($id){

        $vendorAgreement = VendorAgreement::find($id);

        return view('vendorAgreementEdit')->with('vendorAgreement', $vendorAgreement);
    }

    public function vendorBillEdit($id){

        $vendorBill = Vendorbill::find($id);

        return view('vendorBillEdit')->with('vendorBill',$vendorBill);
    }

    //update customer

    public function customerAgreementUpdate($id,Request $request){

        $this->validate($request, [

            'customer_agreement' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $customer_agreement_img = $request->customer_agreement;

        $customer_agreement_img_new_name = time() . $customer_agreement_img->getClientOriginalName();

        $customer_agreement_img->move(public_path('customerAgreementImg'),$customer_agreement_img_new_name);


        $customerAgreement = Agreement::find($id);
        $customerAgreement->customer_agreement = $customer_agreement_img_new_name;
        $customerAgreement->save();

        $request->session()->flash('success', 'Customer Agreement File Updated successfully.');


        return view('customerAgreementEdit');
    }

    public function customerInvoiceBillUpdate($id ,Request $request){

        $this->validate($request, [

            'customer_invoice_bill' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $customer_invoice_bill_img = $request->customer_invoice_bill;

        $customer_invoice_bill_img_new_name = time() . $customer_invoice_bill_img->getClientOriginalName();

        $customer_invoice_bill_img->move(public_path('customerInvoiceBillImg'),$customer_invoice_bill_img_new_name);


        $customerInvoiceBill = Invoicebill::find($id);
        $customerInvoiceBill->customer_invoice_bill = $customer_invoice_bill_img_new_name;

        $customerInvoiceBill->save();

        $request->session()->flash('success', 'Customer Invoice Bill File Updated successfully.');


        return view('customerInvoiceBillEdit');
    }

    public function employeeAgreementUpdate($id ,Request $request){

        $this->validate($request, [

            'employee_agreement' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $employee_agreement_img = $request->employee_agreement;

        $employee_agreement_img_new_name = time() . $employee_agreement_img->getClientOriginalName();

        $employee_agreement_img->move(public_path('employeeAgreementImg'),$employee_agreement_img_new_name);


        $employeeAgreement = EmployeeAgreement::find($id);
        $employeeAgreement->employee_agreement = $employee_agreement_img_new_name;
        $employeeAgreement->save();


        $request->session()->flash('success', 'Employee Agreement File Added successfully.');

        return view('employeeAgreementEdit');
    }

    public function employeeFFDetailUpdate($id ,Request $request){

        $this->validate($request, [

            'employee_ffdetails' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $employee_ffdetails_img = $request->employee_ffdetails;

        $employee_ffdetails_img_new_name = time() . $employee_ffdetails_img->getClientOriginalName();

        $employee_ffdetails_img->move(public_path('employeeFFD'),$employee_ffdetails_img_new_name);

        $employeeFFDetail = EmployeeFFdetail::find($id);
        $employeeFFDetail->employee_ffdetails = $employee_ffdetails_img_new_name;
        $employeeFFDetail->save();

        $request->session()->flash('success', 'Employee F&F details File Updated successfully.');

        return view('employeeFFDetailEdit');
    }

    public function employeeHRPolicyUpdate($id ,Request $request) {

        $this->validate($request, [

            'employee_h_r_policy' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $employee_h_r_policy_img = $request->employee_h_r_policy;

        $employee_h_r_policy_img_new_name = time() . $employee_h_r_policy_img->getClientOriginalName();

        $employee_h_r_policy_img->move(public_path('employeeHRPlocy'),$employee_h_r_policy_img_new_name);



        $employeeHRPolicy = EmployeeHRPolicy::find($id);
        $employeeHRPolicy->employee_h_r_policy = $employee_h_r_policy_img_new_name;
        $employeeHRPolicy->save();

        $request->session()->flash('success', 'Employee HR Policy File Updated successfully.');

        return view('employeeHRPolicyEdit');
    }

    public function employeePayrollUpdate($id ,Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'employee_payroll' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $employee_payroll_img = $request->employee_payroll;

        $employee_payroll_img_new_name = time() . $employee_payroll_img->getClientOriginalName();

        $employee_payroll_img->move(public_path('employeePayrollImg '),$employee_payroll_img_new_name);


        $employeePayroll = EmployeePayroll::find($id);
        $employeePayroll->year_month = $request->year_month;
        $employeePayroll->employee_payroll = $employee_payroll_img_new_name;
        $employeePayroll->save();

        $request->session()->flash('success', 'Employee Payroll File Updated successfully.');

        return view('employeePayrollEdit');
    }

    public function fixedAssetsRegisterUpdate($id ,Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'fixed_asset' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $fixed_asset_img = $request->fixed_asset;

        $fixed_asset_img_new_name = time() . $fixed_asset_img->getClientOriginalName();

        $fixed_asset_img->move(public_path('FixedAssets'),$fixed_asset_img_new_name);



        $fixedAssetsRegister = FixedAsset::find($id);
        $fixedAssetsRegister->year_month = $request->year_month;
        $fixedAssetsRegister->fixed_asset = $fixed_asset_img_new_name;
        $fixedAssetsRegister->save();

        $request->session()->flash('success', 'Fixed Assets File Added successfully.');


        return view('fixedAssetsRegisterEdit');
    }

    public function bankReconcilliationUpdate($id ,Request $request){

        $this->validate($request, [

            'year_month' => ['required'],
            'bank_reconcilliation' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $bank_reconcilliation_img = $request->bank_reconcilliation;

        $bank_reconcilliation_img_new_name = time() . $bank_reconcilliation_img->getClientOriginalName();

        $bank_reconcilliation_img->move(public_path('BankReconcilliationImg'),$bank_reconcilliation_img_new_name);


        $bankReconcilliation = BankReconcilliation::find($id);
        $bankReconcilliation->year_month = $request->year_month;
        $bankReconcilliation->bank_reconcilliation = $bank_reconcilliation_img_new_name;
        $bankReconcilliation->save();

        $request->session()->flash('success', 'Bank Reconcilliation File Added successfully.');


        return view('bankReconcilliationEdit');
    }

    public function vendorAgreementUpdate($id ,Request $request){

        $this->validate($request, [

            'vendor_agreement' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $vendor_agreement_img = $request->vendor_agreement;

        $vendor_agreement_img_new_name = time() . $vendor_agreement_img->getClientOriginalName();

        $vendor_agreement_img->move(public_path('vendorAgreementImg'),$vendor_agreement_img_new_name);


        $vendorAgreement = VendorAgreement::find($id);
        $vendorAgreement->vendor_agreement = $vendor_agreement_img_new_name;
        $vendorAgreement->save();

        $request->session()->flash('success', 'Vendor Agreement File Updated successfully.');


        return view('vendorAgreementEdit');
    }

    public function vendorBillUpdate($id ,Request $request){

        $this->validate($request, [

            'vendor_bill' => 'required | image|mimes:jpeg,png,jpg,pdf'

        ]);

        $vendor_bill_img = $request->vendor_bill;

        $vendor_bill_img_new_name = time() . $vendor_bill_img->getClientOriginalName();

        $vendor_bill_img->move(public_path('vendorBillImg'),$vendor_bill_img_new_name);


        $vendorBill = Vendorbill::find($id);
        $vendorBill->vendor_bill = $vendor_bill_img_new_name;
        $vendorBill->save();

        $request->session()->flash('success', 'Vendor Bills  File Updated successfully.');

        return view('vendorBillEdit');
    }


}

