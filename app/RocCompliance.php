<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RocCompliance extends Model
{
    //
    protected $fillable = [

        'company_name',
        'type_meeting',
        'date_meeting',
        'agenda',
        'attendance',
        'remarks'
    ];
}
