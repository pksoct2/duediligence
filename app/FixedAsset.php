<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FixedAsset extends Model
{
    //
    protected $fillable = [

        'company_name',
        'year_month',
        'fixed_asset'
    ];
}
