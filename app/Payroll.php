<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payroll extends Model
{
    //
    protected $fillable = [

        'company_name',
        'year_month',
        'payroll'
    ];
}
