<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuickInfo extends Model
{
    protected $fillable = [

        'quick_info' ,
        'tds_rates',
        'gst_rates',
        'quick_glances',
    ];
}
