<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EsiReturn extends Model
{
    //
    protected $fillable = [

        'company_name',
        'year_month',
        'esi_return'
    ];
}
