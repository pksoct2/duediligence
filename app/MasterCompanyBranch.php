<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterCompanyBranch extends Model
{

    protected $fillable = [
        'user_id',
        'parent_company_name',
        'branch_name',
        'pan_income_tax_img',
        'tan_traces_img',
        'certificate_of_incorp_img',
        'memo_of_association_img',
        'articles_of_association_img',
        'gst_tax_cert_img',
        'lut_cert_img',
        'gst_e_com_cert_img',
        'prof_tax_enrollment_cert_img',
        'prof_tax_register_cert_img',
        'pf_establish_cert_img',
        'esic_register_cert_img',
        'shops_establish_cert_trade_licence_img',
        'iec_img',
        'msmed_cert_img',
        'startup_cert_img',
        'lower_tax_deduction_cert_img',
        'tax_exemption_cert_us12a_img',
        'tax_exemption_cert_us80g_img',
        'any_other_details',
        'pan_income_tax',
        'tan_traces',
        'certificate_of_incorp',
        'memo_of_association',
        'articles_of_association',
        'gst_tax_cert',
        'lut_cert',
        'gst_e_com_cert',
        'prof_tax_enrollment_cert',
        'prof_tax_register_cert',
        'pf_establish_cert',
        'esic_register_cert',
        'shops_establish_cert_trade_licence',
        'iec',
        'msmed_cert',
        'startup_cert',
        'lower_tax_deduction_cert',
        'tax_exemption_cert_us12a',
        'tax_exemption_cert_us80g',
        'any_other_details_etc',
    ];
}
