<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterData extends Model
{
    //
    protected $fillable = [

        'company_name',
        'company_prt__llp_individual',
        'date_incorporation',
        'pan',
        'tan',
        'pf_reg',
        'esi_reg',
        'pt_reg',
        'other_applicable_law_reg',
        'certificate_incorporation',
        'aoa',
        'moa',
        'cin',
        'authorized_capital',
        'paid_up_capital',
        'reg_office',
        'objects',
        'email',
        'telephone',
        'director_details',
        'shareholder_details'
    ];

}
