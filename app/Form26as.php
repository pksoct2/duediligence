<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form26as extends Model
{
    //
    protected $fillable = [

        'company_name',
        'year_month',
        'form26as'
    ];
}
