<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EsiPayment extends Model
{
    //
    protected $fillable = [

        'company_name',
        'year_month',
        'esi_payment'
    ];
}
