<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TdsReturn extends Model
{
    //
    protected $fillable = [

        'company_name',
        'year_month',
        'Quarter_wise'
    ];
}
