<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agreement extends Model
{
    //
    protected $fillable = [

        'company_name',
        'customer_agreement',
    ];
}
