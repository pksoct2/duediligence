<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gstr3b extends Model
{
    //
    protected $fillable = [

        'company_name',
        'year_month',
        'gstr3b'
    ];
}
