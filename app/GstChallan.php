<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GstChallan extends Model
{
    //
    protected $fillable = [

        'company_name',
        'year_month',
        'gst_challan'
    ];
}
