<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PfReturn extends Model
{
    //
    protected $fillable = [
        'company_name',
        'year_month',
        'pf_return'
    ];
}
