<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComplianceTracker extends Model
{
    //
    protected $fillable = [
        'users_name',
        'select_month_year',
        'due_date',
        'category',
        'details',
        'compliance_date',
        'documents',
    ];
}
