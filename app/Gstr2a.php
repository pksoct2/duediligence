<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gstr2a extends Model
{
    //
    protected $fillable = [

        'company_name',
        'year_month',
        'gstr2a'
    ];
}
