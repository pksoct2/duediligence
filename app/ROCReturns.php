<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ROCReturns extends Model
{
    //
    protected $fillable = [

        'company_name',
        'aoc4',
        'mgt7',
        'srn',
        'date_filing',

    ];
}
