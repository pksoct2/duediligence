<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    //
    protected $fillable = [

        'company_name',
        'final_year',
        'upload_field'
    ];
}
