<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeFFdetail extends Model
{
    //
    protected $fillable = [

        'company_name',
        'employee_ffdetails',
    ];
}
