<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tds extends Model
{
    //
    protected $fillable = [

        'company_name',
        'year_month',
        'tds_challans'
    ];
}
