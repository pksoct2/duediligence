<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gstr1 extends Model
{
    //
    protected $fillable = [

        'company_name',
        'year_month',
        'gstr1'
    ];
}
