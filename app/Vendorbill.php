<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendorbill extends Model
{
    //
    protected $fillable = [

        'company_name',
        'vendor_bill',
    ];
}
