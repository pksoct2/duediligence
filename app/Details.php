<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Details extends Model
{
    //
    protected $fillable = [
        'user_id',
        'user_name',
        'pan_income_tax_img',
        'tan_traces_img',
        'certificate_of_incorp_img',
        'memo_of_association_img',
        'articles_of_association_img',
        'gst_tax_cert_img',
        'lut_cert_img',
        'gst_e_com_cert_img',
        'prof_tax_enrollment_cert_img',
        'prof_tax_register_cert_img',
        'pf_establish_cert_img',
        'esic_register_cert_img',
        'shops_establish_cert_trade_licence_img',
        'iec_img',
        'msmed_cert_img',
        'startup_cert_img',
        'lower_tax_deduction_cert_img',
        'tax_exemption_cert_us12a_img',
        'tax_exemption_cert_us80g_img',
        'any_other_details',
    ];
}
