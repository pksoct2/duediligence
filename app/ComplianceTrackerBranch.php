<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComplianceTrackerBranch extends Model
{
    //
    protected $fillable = [
        'parent_company_name',
        'branch_name',
        'select_month_year',
        'due_date',
        'category',
        'details',
        'compliance_date',
        'documents',
    ];
}
