<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorAgreement extends Model
{
    //
    protected $fillable = [

        'company_name',
        'vendor_agreement',
    ];
}
