<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoicebill extends Model
{
    //
    protected $fillable = [

        'company_name',
        'customer_invoice_bill',
    ];
}
