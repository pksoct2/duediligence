<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statutory extends Model
{
    //
    protected $fillable = [
        'company_name',
        'registration_certificate',
        'yes_no'
    ];
}
