<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeePayroll extends Model
{
    //
    protected $fillable = [

        'company_name',
        'year_month',
        'employee_payroll'
    ];
}
