<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PfChallan extends Model
{
    //
    protected $fillable = [

        'company_name',
        'year_month',
        'pf_challan'
    ];
}
