<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LutCertificate extends Model
{
    //
    protected $fillable = [

        'company_name',
        'year_month',
        'lut_certificate'
    ];
}
