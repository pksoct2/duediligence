<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoticeOrderReceived extends Model
{
    //
    protected $fillable = [

        'company_name',
        'year_month',
        'notice_order'
    ];
}
