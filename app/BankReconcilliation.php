<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankReconcilliation extends Model
{
    //
    protected $fillable = [

        'company_name',
        'year_month',
        'bank_reconcilliation'
    ];
}
