<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeHRPolicy extends Model
{
    //
    protected $fillable = [

        'company_name',
        'employee_h_r_policy',
    ];
}
