<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    //
    protected $fillable = [
        'parent_company_name', 'branch_name', 'email', 'mobile', 'bank_name','bank_account', 'date_of_incorp', 'org_address', 'org_website', 'spoc_dev'
    ];
}
