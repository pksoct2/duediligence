@extends('layouts.user')
@section('content')
    <?php $gstr9cFile = 'gstr9cFile';?>
    <div class="content">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-sm-9 col-md-9">

                <div class="panel panel-default">
                    @if (session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="panel-heading h3 p-2 bg-light font-weight-bold border breadcrumb">
                        Client GSTR9C File
                    </div>
                    <div class="panel-body p-2">

                        <form action="{{ route("gstr9cSubmit") }}" method="POST" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group {{ $errors->has('year_month') ? 'has-error' : '' }}">
                                <label for="year_month">Month/Year*</label>
                                <input type="month" id="year_month" name="year_month" class="form-control" value="{{ old('year_month', isset($user) ? $user->year_month : '') }}" required>
                                @if($errors->has('year_month'))
                                    <p class="help-block">
                                        {{ $errors->first('year_month') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>

                            <div class="form-group {{ $errors->has('gstr9c') ? 'has-error' : '' }}">

                                <label for="gstr9c">GSTR9C*</label>
                                <input id="gstr9c" type="file" value="" class="form-control @error('gstr9c') is-invalid @enderror" name="gstr9c" >
                                @if($errors->has('gstr9c'))
                                    <p class="help-block">
                                        {{ $errors->first('gstr9c') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.email_helper') }}
                                </p>
                                <p class="text-danger">Rename File as GST9C_F.Y._Dateofupload*</p>

                            </div>

                            {{---------------}}

                            <div>
                                <button class="btn btn-success" type="submit" value="{{ trans('global.save') }}">{{ trans('global.save') }}</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
