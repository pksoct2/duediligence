@extends('layouts.user')
@section('content')
    <div class="content">

        <div class="row justify-content-center">
            <div class="col-lg-8 col-sm-9 col-md-9">

                <div class="panel panel-default">
                    @if (session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="panel-heading h3 p-2 bg-light font-weight-bold border breadcrumb">
                        Client Statutory File
                    </div>
                    <div class="panel-body p-2">

                        <form action="{{ route("updateStatutory", $statutory->id ?? '') }}" method="POST" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group {{ $errors->has('registration_certificate') ? 'has-error' : '' }}">
                                <label for="cin">Statutory Applicability Law*</label><br>
                                Yes <input type="radio" name="yes_no" value="yes"/><br><br>
                                <input style="display:none;" id="registration_certificate" type="file" value="" class="form-control @error('registration_certificate') is-invalid @enderror" name="registration_certificate" ><br>
                                No <input type="radio" name="yes_no" checked="checked" value="no"/> <br>

                                @if($errors->has('registration_certificate'))
                                    <p class="help-block">
                                        {{ $errors->first('registration_certificate') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>

                            {{---------------}}

                            <div>
                                <button class="btn btn-success btn-lg" type="submit" value="{{ trans('global.save') }}">{{ trans('global.save') }}</button>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $("input[type='radio']").change(function(){
                if($(this).val()== "yes")
                {
                    $("#registration_certificate").show();
                }
                else
                {
                    $("#registration_certificate").hide();
                }
            });
        });
    </script>
@endsection
