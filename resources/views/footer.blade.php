@section('content')
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="d-none d-sm-block pb-2">
                    <b>Quick Info</b>
                    <ul class="">
                        <li>{{ $quickInfoView['quick_info'] ?? '' }}</li>
                    </ul>
                    <b>TDS Rates</b>
                    <ul>
                        <li>{{ $quickInfoView['tds_rates'] ?? '' }}</li>
                    </ul>
                    <b>GST Rates</b>
                    <ul>
                        <li>{{ $quickInfoView['gst_rates'] ?? '' }}</li>
                    </ul>
                    <b>Quick Glances</b>
                    <ul>
                        <li>{{ $quickInfoView['quick_glances'] ?? '' }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent

@endsection
