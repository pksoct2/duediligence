<aside class="main-sidebar sidebar-dark-primary elevation-4 position-fixed" style="min-height: 1100px;background-color: #1c5c92;">
    <!-- Brand Logo -->
    <a href="#" class="brand-link pl-4">
        <span class="brand-text font-weight-light"><img class="img-fluid" src="../assets/logo.png" style="height: 30px;" alt=""></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar position-fixed">
        <!-- Sidebar user (optional) -->

        <!-- Sidebar Menu -->
        <nav class="mt-2 position-relative">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                <li class="nav-item">
                    <a href="{{ route("home") }}" class="nav-link <?php if($home ?? ''=='home'){echo 'active';}?>">
                        <i class="fas fa-fw fa-tachometer-alt">

                        </i>
                        <p>
                            <span>{{ trans('global.dashboard') }}</span>
                        </p>
                    </a>
                </li>

                <li class="nav-item has-treeview menu-open">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="fa-fw fas fa-users"></i>
                        <p>
                            <span>{{ trans('cruds.userManagement.title') }}</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="{{route("storeUser")}}" class="nav-link <?php if($storeUser ?? ''=='storeUser'){echo 'active';}?>">
                                <i class="fa-fw fas fa-user-plus">

                                </i>
                                <p>
                                    <span> Add Clients</span>
                                </p>
                            </a>
                        </li>

                            <li class="nav-item">
                                <a href="{{route("showUser")}}" class="nav-link <?php if($showUser ?? ''=='showUser'){echo 'active';}?>">
                                    <i class="fa-fw fas fa-user-alt">

                                    </i>
                                    <p>
                                        <span> Clients</span>
                                    </p>
                                </a>
                            </li>


                        <li class="nav-item">
                            <a href="{{route('quickInfo')}}" class="nav-link <?php if($quickInfo ?? ''=='quickInfo'){echo 'active';}?>">
                                <i class="fa-fw fas fa-info-circle">

                                </i>
                                <p>
                                    <span>Quick Info Updates</span>
                                </p>
                            </a>
                        </li>

                    </ul>
                </li>

                <li class="nav-item  has-treeview menu-open">

                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-server"></i>
                        <p>
                            <span>Master Data</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="{{route('adminMasterView')}}" class="nav-link <?php if($adminMaster ?? ''=='adminMasterView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View Master Data </span>
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="{{route('accountFile')}}" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>Accounts</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="{{route('adminAccountView')}}" class="nav-link <?php if($adminAccount ?? ''=='adminAccountView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View Account</span>

                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-file-import"></i>
                        <p>
                            <span>Statutory</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('adminStatutoryView')}}" class="nav-link <?php if($adminStatutory ?? ''=='adminStatutoryView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View Statutory</span>
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-file-import"></i>
                        <p>
                            <span>TDS</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="{{route('adminTdsView')}}" class="nav-link <?php if($adminTds ?? ''=='adminTdsView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View TDS</span>
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-file-import"></i>
                        <p>
                            <span>TDS Return</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('adminTdsReturnView')}}" class="nav-link <?php if($adminTdsReturn ?? ''=='adminTdsReturnView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View TDS Return</span>
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-file-upload">
                        </i>
                        <p>
                            <span>GSTR1</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="{{route('adminGstr1View')}}" class="nav-link <?php if($adminGstr1 ?? ''=='adminGstr1View'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View GSTR1</span>
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-file-upload">

                        </i>
                        <p>
                            <span>GSTR2A</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="{{route('adminGstr2aView')}}" class="nav-link <?php if($adminGstr2a ?? ''=='adminGstr2aView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View GSTR2A</span>
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-file-upload">
                        </i>
                        <p>
                            <span>GSTR3B</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="{{route('adminGstr3bView')}}" class="nav-link <?php if($adminGstr3b ?? ''=='adminGstr3bView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View GSTR3B</span>
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-file-upload">

                        </i>
                        <p>
                            <span>GSTChallans</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('adminGstChallanView')}}" class="nav-link <?php if($adminGstChallan ?? ''=='adminGstChallanView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View GSTChallans</span>
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="{{route('gstr9File')}}" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-file-upload">
                        </i>
                        <p>
                            <span>GSTR9</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('adminGstr9View')}}" class="nav-link <?php if($adminGstr9 ?? ''=='adminGstr9View'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View GSTR9</span>
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="{{route('gstr9cFile')}}" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-file-upload">

                        </i>
                        <p>
                            <span>GSTR9C</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="{{route('adminGstr9cView')}}" class="nav-link <?php if($adminGstr9c ?? ''=='adminGstr9cView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View GSTR9C</span>
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-file-upload">
                        </i>
                        <p>
                            <span>LUT Certificate</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('adminLutCertificateView')}}" class="nav-link <?php if($adminLutCertificate ?? ''=='adminLutCertificateView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View LUT Cert</span>
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="{{route('gstRegistration')}}" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-file-upload">

                        </i>
                        <p>
                            <span>List GST Reg</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="{{route('adminGstRegistrationView')}}" class="nav-link <?php if($adminGstRegistration ?? ''=='adminGstRegistrationView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View GST Reg</span>
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="{{route('notice_order')}}" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-file-upload">
                        </i>
                        <p>
                            <span>Notices/Orders Receive</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="{{route('adminNoticeOrderView')}}" class="nav-link <?php if($adminNoticeOrder ?? ''=='adminNoticeOrderView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View Notices/Orders</span>
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>PF Return</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="{{route('adminPfReturnView')}}" class="nav-link <?php if($adminPfReturn ?? ''=='adminPfReturnView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View PF Return</span>

                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>PF Payment Conf</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('adminPfPaymentView')}}" class="nav-link <?php if($adminPfPayment ?? ''=='adminPfPaymentView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View PF Payment</span>

                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>PF challan</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('adminPfChallanView')}}" class="nav-link <?php if($adminPfChallan ?? ''=='adminPfChallanView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View PF challan</span>

                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>ESI Return</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="{{route('adminEsiReturnView')}}" class="nav-link <?php if($adminEsiReturn ?? ''=='adminEsiReturnView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View ESI Return</span>
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>ESI Payment Conf</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="{{route('adminEsiPaymentView')}}" class="nav-link <?php if($adminEsiPayment ?? ''=='adminEsiPaymentView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View ESI Payment Conf</span>

                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>ESI challan</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="{{route('adminEsiChallanView')}}" class="nav-link <?php if($adminEsiChallan ?? ''=='adminEsiChallanView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye"></i>
                                <p>
                                    <span>View ESI challan</span>
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>Form 26AS</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="{{route('adminForm26asView')}}" class="nav-link <?php if($adminForm26as ?? ''=='adminForm26asView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View Form 26AS</span>
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>Payroll</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="{{route('adminPayrollView')}}" class="nav-link <?php if($adminPayroll ?? ''=='adminPayrollView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View Payroll</span>

                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>ROC Compliance</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="{{route('adminRocComplianceView')}}" class="nav-link <?php if($adminRocCompliance ?? ''=='adminRocComplianceView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View ROC Compliance</span>

                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>ROC Returns</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="{{route('adminRocReturnView')}}" class="nav-link <?php if($adminRocReturn ?? ''=='adminRocReturnView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View ROC Returns</span>

                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="{{route('accountFile')}}" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>Customers Data</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">


                        <li class="nav-item">
                            <a href="{{route('adminCustomerAgreementView')}}" class="nav-link <?php if($adminCustomerAgreement ?? ''=='adminCustomerAgreementView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View Agreements Name</span>

                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('adminCustomerInvoiceBillView')}}" class="nav-link <?php if($adminCustomerInvoiceBill ?? ''=='adminCustomerInvoiceBillView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View Invoices or bill</span>

                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>Vendor Data</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">


                        <li class="nav-item">
                            <a href="{{route('adminVendorAgreementView')}}" class="nav-link <?php if($adminVendorAgreement ?? ''=='adminVendorAgreementView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View Agreements</span>

                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('adminVendorBillView')}}" class="nav-link <?php if($adminVendorBill ?? ''=='adminVendorBillView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View Bills</span>

                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>Fixed Assets Register</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="{{route('adminFixedAssetsView')}}" class="nav-link <?php if($adminFixedAssets ?? ''=='adminFixedAssetsView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View Fixed Assets Reg.</span>

                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>Employee</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="{{route('adminEmployeeAgreementView')}}" class="nav-link <?php if($adminEmployeeAgreement ?? ''=='adminEmployeeAgreementView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View Employee Agr.</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('adminEmployeePayrollView')}}" class="nav-link <?php if($adminEmployeePayroll ?? ''=='adminEmployeePayrollView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View Payroll</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('adminEmployeeFFDetailView')}}" class="nav-link <?php if($adminEmployeeFFDetail ?? ''=='adminEmployeeFFDetailView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View F&F details</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('adminEmployeeHRPolicyView')}}" class="nav-link <?php if($adminEmployeeHRPolicy ?? ''=='adminEmployeeHRPolicyView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View HR Policy</span>
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>Bank Reconcilliation St.</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('adminBankReconcilliationView')}}" class="nav-link <?php if($adminBankReconcilliation ?? ''=='adminBankReconcilliationView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View Bank Reconcilliation</span>
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="{{route('logout')}}" class="nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">

                            <i class="fas fa-fw fa-sign-out-alt">

                            </i>
                        <p>
                            <span>{{ trans('global.logout') }}</span>
                        </p>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
