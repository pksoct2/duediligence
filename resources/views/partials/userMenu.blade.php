<aside class="main-sidebar sidebar-dark-primary elevation-4 position-fixed" style="min-height: auto;background-color: #1c5c92">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <span class="brand-text font-weight-light font-weight-bolder pl-4">{{ trans('panel.site_title') }}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar position-fixed" style="min-height: auto">
        <!-- Sidebar user (optional) -->

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar nav-treeview flex-column" data-widget="treeview" role="menu" data-accordion="false">

                <li class="nav-item">
                    <a href="{{ route("clientDashboard") }}" class="nav-link <?php if($clientDashboard ?? ''=='clientDashboard'){echo 'active';}?>">
                        <i class="fas fa-fw fa-tachometer-alt">

                        </i>
                        <p>
                            <span>{{ trans('global.dashboard') }}</span>
                        </p>
                    </a>
                </li>

                <li class="nav-item has-treeview menu-open">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="fa-fw fas fa-users"></i>
                        <p>
                            <span>Company Profile</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">


                            <li class="nav-item">
                                <a href="{{route('userSideView')}}" class="nav-link <?php if($userSide ?? ''=='userSideView'){echo 'active';}?>">
                                    <i class="fa-fw fas fa-eye">

                                    </i>
                                    <p>
                                        <span> Profile View</span>
                                    </p>
                                </a>
                            </li>
                    </ul>
                </li>

                <li class="nav-item has-treeview menu-open">

                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-server"></i>
                        <p>
                            <span>Master Data</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('master_Data')}}" class="nav-link <?php if($master_Data ?? ''=='master_Data'){echo 'active';}?>">
                                <i class="fa-fw fas fa-user-plus">

                                </i>
                                <p>
                                    <span>Add Master Data</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('masterDataView')}}" class="nav-link <?php if($masterDataView ?? ''=='masterDataView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View Master Data </span>
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="{{route('accountFile')}}" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>Accounts</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('accountFile')}}" class="nav-link <?php if($accountFile ?? ''=='accountFile'){echo 'active';}?>">
                                <i class="fa-fw fas fa-user-plus">

                                </i>
                                <p>
                                    <span>Add Account</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('accountView')}}" class="nav-link <?php if($accountView ?? ''=='accountView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View Account</span>

                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-file-import"></i>
                        <p>
                            <span>Statutory</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('statutoryAdd')}}" class="nav-link <?php if($statutoryAdd ?? ''=='statutoryAdd'){echo 'active';}?>">
                                <i class="fa-fw fas fa-user-plus">

                                </i>
                                <p>
                                    <span>Add Statutory</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('statutoryView')}}" class="nav-link <?php if($statutoryView ?? ''=='statutoryView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View Statutory</span>
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-file-import"></i>
                        <p>
                            <span>TDS</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('tdsFile')}}" class="nav-link <?php if($tdsFile1 ?? ''=='tdsFile'){echo 'active';}?>">
                                <i class="fa-fw fas fa-user-plus">

                                </i>
                                <p>
                                    <span>Add TDS</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('tdsFileView')}}" class="nav-link <?php if($tdsFile ?? ''=='tdsFileView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View TDS</span>
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-file-import"></i>
                        <p>
                            <span>TDS Return</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('tdsReturn')}}" class="nav-link <?php if($tdsReturn ?? ''=='tdsReturn'){echo 'active';}?>">
                                <i class="fa-fw fas fa-user-plus">
                                </i>
                                <p>
                                    <span>Add TDS Return</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('tdsReturnView')}}" class="nav-link <?php if($tdsReturn1 ?? ''=='tdsReturnView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View TDS Return</span>
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item has-treeview menu-open">
                            <a href="" class="nav-link nav-dropdown-toggle">
                                <i class="fa-fw fas fa-file-upload">
                                </i>
                                <p>
                                    <span>GSTR1</span>
                                    <i class="right fa fa-fw fa-angle-left"></i>
                                </p>
                            </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('gstr1File')}}" class="nav-link <?php if($gstr1File ?? ''=='gstr1File'){echo 'active';}?>">
                                <i class="fa-fw fas fa-user-plus">
                                </i>
                                <p>
                                    <span>Add GSTR1</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('gstr1FileView')}}" class="nav-link <?php if($gstr1File1 ?? ''=='gstr1FileView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View GSTR1</span>
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                        <li class="nav-item has-treeview menu-open">
                            <a href="" class="nav-link nav-dropdown-toggle">
                                <i class="fa-fw fas fa-file-upload">

                                </i>
                                <p>
                                    <span>GSTR2A</span>
                                    <i class="right fa fa-fw fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('gstr2aFile')}}" class="nav-link <?php if($gstr2aFile ?? ''=='gstr2aFile'){echo 'active';}?>">
                                        <i class="fa-fw fas fa-user-plus">
                                        </i>
                                        <p>
                                            <span>Add GSTR2A</span>
                                        </p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('gstr2aFileView')}}" class="nav-link <?php if($gstr2aFile1 ?? ''=='gstr2aFileView'){echo 'active';}?>">
                                        <i class="fa-fw fas fa-eye">

                                        </i>
                                        <p>
                                            <span>View GSTR2A</span>
                                        </p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview menu-open">
                            <a href="" class="nav-link nav-dropdown-toggle">
                                <i class="fa-fw fas fa-file-upload">
                                </i>
                                <p>
                                    <span>GSTR3B</span>
                                    <i class="right fa fa-fw fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('gstr3bFile')}}" class="nav-link <?php if($gstr3bFile ?? ''=='gstr3bFile'){echo 'active';}?>">
                                        <i class="fa-fw fas fa-user-plus">
                                        </i>
                                        <p>
                                            <span>Add GSTR3B</span>
                                        </p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('gstr3bFileView')}}" class="nav-link <?php if($gstr3bFile1 ?? ''=='gstr3bFileView'){echo 'active';}?>">
                                        <i class="fa-fw fas fa-eye">

                                        </i>
                                        <p>
                                            <span>View GSTR3B</span>
                                        </p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview menu-open">
                            <a href="{{route('gstrChallan')}}" class="nav-link nav-dropdown-toggle">
                                <i class="fa-fw fas fa-file-upload">

                                </i>
                                <p>
                                    <span>GSTChallans</span>
                                    <i class="right fa fa-fw fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('gstrChallan')}}" class="nav-link <?php if($gstrChallan ?? ''=='gstrChallan'){echo 'active';}?>">
                                        <i class="fa-fw fas fa-user-plus">
                                        </i>
                                        <p>
                                            <span>Add GSTChallans</span>
                                        </p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('gstChallanView')}}" class="nav-link <?php if($gstrChallan1 ?? ''=='gstChallanView'){echo 'active';}?>">
                                        <i class="fa-fw fas fa-eye">

                                        </i>
                                        <p>
                                            <span>View GSTChallans</span>
                                        </p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview menu-open">
                            <a href="{{route('gstr9File')}}" class="nav-link nav-dropdown-toggle">
                                <i class="fa-fw fas fa-file-upload">
                                </i>
                                <p>
                                    <span>GSTR9</span>
                                    <i class="right fa fa-fw fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('gstr9File')}}" class="nav-link <?php if($gstr9File ?? ''=='gstr9File'){echo 'active';}?>">
                                        <i class="fa-fw fas fa-user-plus">
                                        </i>
                                        <p>
                                            <span>Add GSTR9</span>
                                        </p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('gstr9FileView')}}" class="nav-link <?php if($gstr9File1 ?? ''=='gstr9FileView'){echo 'active';}?>">
                                        <i class="fa-fw fas fa-eye">

                                        </i>
                                        <p>
                                            <span>View GSTR9</span>
                                        </p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview menu-open">
                            <a href="{{route('gstr9cFile')}}" class="nav-link nav-dropdown-toggle">
                                <i class="fa-fw fas fa-file-upload">

                                </i>
                                <p>
                                    <span>GSTR9C</span>
                                    <i class="right fa fa-fw fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('gstr9cFile')}}" class="nav-link <?php if($gstr9cFile ?? ''=='gstr9cFile'){echo 'active';}?>">
                                        <i class="fa-fw fas fa-user-plus">
                                        </i>
                                        <p>
                                            <span>Add GSTR9C</span>
                                        </p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('gstr9cFileView')}}" class="nav-link <?php if($gstr9cFile1 ?? ''=='gstr9cFileView'){echo 'active';}?>">
                                        <i class="fa-fw fas fa-eye">

                                        </i>
                                        <p>
                                            <span>View GSTR9C</span>
                                        </p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview menu-open">
                            <a href="{{route('lut_certificate')}}" class="nav-link nav-dropdown-toggle">
                                <i class="fa-fw fas fa-file-upload">
                                </i>
                                <p>
                                    <span>LUT Certificate</span>
                                    <i class="right fa fa-fw fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('lut_certificate')}}" class="nav-link <?php if($lutCertificateView ?? ''=='lut_certificate'){echo 'active';}?>">
                                        <i class="fa-fw fas fa-user-plus">
                                        </i>
                                        <p>
                                            <span>Add LUT Cert</span>
                                        </p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('lutCertificateView')}}" class="nav-link <?php if($lut_certificate11 ?? ''=='lutCertificateView'){echo 'active';}?>">
                                        <i class="fa-fw fas fa-eye">

                                        </i>
                                        <p>
                                            <span>View LUT Cert</span>
                                        </p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview menu-open">
                            <a href="#" class="nav-link nav-dropdown-toggle">
                                <i class="fa-fw fas fa-file-upload">

                                </i>
                                <p>
                                    <span>List GST Reg</span>
                                    <i class="right fa fa-fw fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('registrationGst')}}" class="nav-link <?php if($gstRegistration ?? '' == 'gstRegistrations'){echo 'active';}?>">
                                        <i class="fa-fw fas fa-user-plus">
                                        </i>
                                        <p>
                                            <span>Add GST Reg</span>
                                        </p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('gstRegistrationView')}}" class="nav-link <?php if($gstRegistration1 ?? ''=='gstRegistrationView'){echo 'active';}?>">
                                        <i class="fa-fw fas fa-eye">

                                        </i>
                                        <p>
                                            <span>View GST Reg</span>
                                        </p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview menu-open">
                            <a href="{{route('notice_order')}}" class="nav-link nav-dropdown-toggle">
                                <i class="fa-fw fas fa-file-upload">
                                </i>
                                <p>
                                    <span>Notices/Orders Receive</span>
                                    <i class="right fa fa-fw fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('notice_order')}}" class="nav-link <?php if($notice_order ?? ''=='notice_order'){echo 'active';}?>">
                                        <i class="fa-fw fas fa-user-plus">
                                        </i>
                                        <p>
                                            <span>Add Notices/Orders</span>
                                        </p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('noticeOrderView')}}" class="nav-link <?php if($notice_order1 ?? ''=='notice_orderView'){echo 'active';}?>">
                                        <i class="fa-fw fas fa-eye">

                                        </i>
                                        <p>
                                            <span>View Notices/Orders</span>
                                        </p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>PF Return</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('pfReturnFile')}}" class="nav-link <?php if($pfReturnFile ?? ''=='pfReturnFile'){echo 'active';}?>">
                                <i class="fa-fw fas fa-user-plus">

                                </i>
                                <p>
                                    <span>Add PF Return</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('pfReturnView')}}" class="nav-link <?php if($pfReturnFile1 ?? ''=='pfReturnView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View PF Return</span>

                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>PF Payment Conf</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('pfPaymentFile')}}" class="nav-link <?php if($pfPaymentFile ?? ''=='pfPaymentFile'){echo 'active';}?>">
                                <i class="fa-fw fas fa-user-plus">

                                </i>
                                <p>
                                    <span>Add PF Payment</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('pfPaymentView')}}" class="nav-link <?php if($pfPaymentFile1 ?? ''=='pfPaymentView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View PF Payment</span>

                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>PF challan</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('pfChallanFile')}}" class="nav-link <?php if($pfChallanFile ?? ''=='pfChallanFile'){echo 'active';}?>">
                                <i class="fa-fw fas fa-user-plus">

                                </i>
                                <p>
                                    <span>Add PF challan</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('pfChallanView')}}" class="nav-link <?php if($pfChallanFile1 ?? ''=='pfChallanView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View PF challan</span>

                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>ESI Return</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('esiReturnFile')}}" class="nav-link <?php if($esiReturnFile ?? ''=='esiReturnFile'){echo 'active';}?>">
                                <i class="fa-fw fas fa-user-plus">

                                </i>
                                <p>
                                    <span>Add ESI Return</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('pfReturnView')}}" class="nav-link <?php if($esiReturnFile1 ?? ''=='esiReturnView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View ESI Return</span>
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>ESI Payment Conf</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('esiPaymentFile')}}" class="nav-link <?php if($esiPaymentFile ?? ''=='esiPaymentFile'){echo 'active';}?>">
                                <i class="fa-fw fas fa-user-plus">

                                </i>
                                <p>
                                    <span>Add ESI Payment Conf</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('esiPaymentView')}}" class="nav-link <?php if($esiPaymentFile1 ?? ''=='esiPaymentView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View ESI Payment Conf</span>

                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>ESI challan</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('esiChallanFile')}}" class="nav-link <?php if($esiChallanFile ?? ''=='esiChallanFile'){echo 'active';}?>">
                                <i class="fa-fw fas fa-user-plus">

                                </i>
                                <p>
                                    <span>Add ESI challan</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('esiChallanView')}}" class="nav-link <?php if($esiChallanFile1 ?? ''=='esiChallanView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View ESI challan</span>

                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>Form 26AS</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('form26asFile')}}" class="nav-link <?php if($form26asFile ?? ''=='form26asFile'){echo 'active';}?>">
                                <i class="fa-fw fas fa-user-plus">

                                </i>
                                <p>
                                    <span>Add Form 26AS</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('form26asView')}}" class="nav-link <?php if($form26asFile1 ?? ''=='form26asView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View Form 26AS</span>

                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>Payroll</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('payrollFile')}}" class="nav-link <?php if($payrollFile ?? ''=='payrollFile'){echo 'active';}?>">
                                <i class="fa-fw fas fa-user-plus">

                                </i>
                                <p>
                                    <span>Add Payroll</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('payrollView')}}" class="nav-link <?php if($payrollFile1 ?? ''=='payrollView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View Payroll</span>

                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>ROC Compliance</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('ROCCompliance')}}" class="nav-link <?php if($ROCCompliance ?? ''=='ROCCompliance'){echo 'active';}?>">
                                <i class="fa-fw fas fa-user-plus">

                                </i>
                                <p>
                                    <span>Add ROC Compliance</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('ROCComplianceView')}}" class="nav-link <?php if($ROCCompliance1 ?? ''=='ROCComplianceView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View ROC Compliance</span>

                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>ROC Returns</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('rocReturn')}}" class="nav-link <?php if($rocReturn1 ?? ''=='rocReturn'){echo 'active';}?>">
                                <i class="fa-fw fas fa-user-plus">

                                </i>
                                <p>
                                    <span>Add ROC Returns</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('rocReturnView')}}" class="nav-link <?php if($rocReturnView ?? ''=='rocReturnView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View ROC Returns</span>

                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="{{route('accountFile')}}" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>Customers Data</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('customerAgreement')}}" class="nav-link <?php if($customer ?? ''=='customerAgreement'){echo 'active';}?>">
                                <i class="fa-fw fas fa-user-plus">

                                </i>
                                <p>
                                    <span>Add Agreements Name</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('customerInvoiceBill')}}" class="nav-link <?php if($customerAgreement11 ?? ''=='customerInvoiceBill'){echo 'active';}?>">
                                <i class="fa-fw fas fa-user-plus">

                                </i>
                                <p>
                                    <span>Add Invoices or bill</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('customerAgreementView')}}" class="nav-link <?php if($customerAgreement111 ?? ''=='customerAgreementView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View Agreements Name</span>

                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('customerInvoiceBillView')}}" class="nav-link <?php if($customerBill ?? ''=='customerInvoiceBillView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View Invoices or bill</span>

                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>Vendor Data</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('vendorAgreement')}}" class="nav-link <?php if($vendor ?? ''=='vendorAgreement'){echo 'active';}?>">
                                <i class="fa-fw fas fa-user-plus">

                                </i>
                                <p>
                                    <span>Add Agreements</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('vendorBill')}}" class="nav-link <?php if($vendorBillwe ?? ''=='vendorBill'){echo 'active';}?>">
                                <i class="fa-fw fas fa-user-plus">

                                </i>
                                <p>
                                    <span>Add Bills</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('vendorAgreementView')}}" class="nav-link <?php if($vendorAgreementView ?? ''=='vendorAgreementView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View Agreements</span>

                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('vendorBillView')}}" class="nav-link <?php if($vendorBillView ?? ''=='vendorBillView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View Bills</span>

                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>Fixed Assets Register</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('fixedAssetsRegister')}}" class="nav-link <?php if($fixedAssetsRegister1 ?? ''=='fixedAssetsRegister'){echo 'active';}?>">
                                <i class="fa-fw fas fa-user-plus">

                                </i>
                                <p>
                                    <span>Add Fixed Assets Reg.</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('fixedAssetsRegisterView')}}" class="nav-link <?php if( $fixedAssetsRegisterView ?? ''=='fixedAssetsRegisterView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View Fixed Assets Reg.</span>

                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="{{route('accountFile')}}" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>Employee</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('employeeAgreement')}}" class="nav-link <?php if($employeeAgr ?? ''=='employeeAgreement'){echo 'active';}?>">
                                <i class="fa-fw fas fa-user-plus">

                                </i>
                                <p>
                                    <span>Add Employee Agr.</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('employeePayroll')}}" class="nav-link <?php if($employeePay ?? ''=='employeePayroll'){echo 'active';}?>">
                                <i class="fa-fw fas fa-user-plus">

                                </i>
                                <p>
                                    <span>Add Payroll</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('employeeFFDetail')}}" class="nav-link <?php if($employeeFF ?? ''=='employeeFFDetail'){echo 'active';}?>">
                                <i class="fa-fw fas fa-user-plus">

                                </i>
                                <p>
                                    <span>Add F&F details</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('employeeHRPolicy')}}" class="nav-link <?php if($employeeHR ?? ''=='employeeHRPolicy'){echo 'active';}?>">
                                <i class="fa-fw fas fa-user-plus">

                                </i>
                                <p>
                                    <span>Add HR Policy</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('employeeAgreementView')}}" class="nav-link <?php if($employeeAgreementView ?? ''=='employeeAgreementView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View Employee Agr.</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('employeePayrollView')}}" class="nav-link <?php if($employeePayrollView ?? ''=='employeePayrollView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View Payroll</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('employeeFFDetailView')}}" class="nav-link <?php if($employeeFFDetailView ?? ''=='employeeFFDetailView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View F&F details</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('employeeHRPolicyView')}}" class="nav-link <?php if($employeeHRPolicyView ?? ''=='employeeHRPolicyView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View HR Policy</span>
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="" class="nav-link nav-dropdown-toggle">
                        <i class="fa-fw fas fa-user-circle"></i>
                        <p>
                            <span>Bank Reconcilliation St.</span>
                            <i class="right fa fa-fw fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('bankReconcilliation')}}" class="nav-link <?php if($bankRecon ?? ''=='bankReconcilliation'){echo 'active';}?>">
                                <i class="fa-fw fas fa-user-plus">

                                </i>
                                <p>
                                    <span>Add Bank Reconcilliation</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('bankReconcilliationView')}}" class="nav-link <?php if($bankReconcilliationView ?? ''=='bankReconcilliationView'){echo 'active';}?>">
                                <i class="fa-fw fas fa-eye">

                                </i>
                                <p>
                                    <span>View Bank Reconcilliation</span>

                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="{{route('logout')}}" class="nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                        <i class="fas fa-fw fa-sign-out-alt"></i>
                        <p><span>{{ trans('global.logout') }}</span></p>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
