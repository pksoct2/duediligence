@extends('layouts.admin')
@section('content')
    <div class="content">

        <div class="row justify-content-center">
            <div class="col-lg-8 col-sm-9 col-md-9">

                <div class="panel panel-default">
                    @if (session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="panel-heading h3 p-2 bg-light font-weight-bold border breadcrumb">
                        {{ trans('global.create') }} {{ trans('cruds.user.title_singular') }}
                    </div>
                    <div class="panel-body p-2">

                        <form action="{{ route("branchesCompany") }}" method="POST" enctype="multipart/form-data">
                            @csrf

                            <div class="row">
                                <div class="col-sm-6">
                                    <label for="branch_name">Select {{ trans('cruds.user.fields.name') }}*</label>

                                    <select name="parent_company_name" id="parent_company_name" class="form-control select2" required>
                                        @php use App\User;
                                        $showAllUser = User::all();
                                        print_r($showAllUser);
                                        @endphp

                                        @foreach( $showAllUser as $user)

                                            <option value="{{$user->name}}">{{$user->name}}</option>

                                        @endforeach
                                    </select>
                                    @if($errors->has('name'))
                                        <p class="help-block">
                                            {{ $errors->first('name') }}
                                        </p>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.user.fields.roles_helper') }}
                                    </p>
                                </div>

                            </div>

                            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                <label for="name">{{ trans('cruds.user.fields.name') }}*</label>
                                <input type="text" id="branch_name" name="branch_name" class="form-control" value="" placeholder="Enter Branch Organization Name" required>
                                @if($errors->has('name'))
                                    <p class="help-block">
                                        {{ $errors->first('name') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>

                            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                <label for="email">{{ trans('cruds.user.fields.email') }}*</label>
                                <input type="email" id="email" name="email" class="form-control" value="" placeholder="Enter Email*" required>
                                @if($errors->has('email'))
                                    <p class="help-block">
                                        {{ $errors->first('email') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.email_helper') }}
                                </p>
                            </div>


                            <div class="form-group {{ $errors->has('mobile') ? 'has-error' : '' }}">
                                <label for="name">{{ trans('cruds.user.fields.mobile') }}*</label>
                                <input type="text" id="mobile" name="mobile" class="form-control" value="" placeholder="Enter Mobile Number*" required>
                                @if($errors->has('mobile'))
                                    <p class="help-block">
                                        {{ $errors->first('mobile') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            <div class="form-group {{ $errors->has('bank_name') ? 'has-error' : '' }}">
                                <label for="bank_name">{{ trans('cruds.user.fields.bank_name') }}*</label>
                                <input type="text" id="bank_name" name="bank_name" class="form-control" value="" placeholder="Enter Bank Name" required>
                                @if($errors->has('bank_name'))
                                    <p class="help-block">
                                        {{ $errors->first('bank_name') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            <div class="form-group {{ $errors->has('bank_account') ? 'has-error' : '' }}">
                                <label for="bank_account">{{ trans('cruds.user.fields.bank_account') }}*</label>
                                <input type="text" id="bank_account" name="bank_account" class="form-control" value="" placeholder="Enter Bank Account*" required>
                                @if($errors->has('bank_account'))
                                    <p class="help-block">
                                        {{ $errors->first('bank_account') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            <div class="form-group {{ $errors->has('date_of_incorp') ? 'has-error' : '' }}">
                                <label for="date_of_incorp">{{ trans('cruds.user.fields.date_of_incorp') }}*</label>
                                <input type="date" id="date_of_incorp" name="date_of_incorp" class="form-control" value="" required>
                                @if($errors->has('date_of_incorp'))
                                    <p class="help-block">
                                        {{ $errors->first('date_of_incorp') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            <div class="form-group {{ $errors->has('org_address') ? 'has-error' : '' }}">
                                <label for="org_address">{{ trans('cruds.user.fields.org_address') }}*</label>
                                <input type="text" id="org_address" name="org_address" class="form-control" value="" placeholder="Enter Organization Address*" required>
                                @if($errors->has('org_address'))
                                    <p class="help-block">
                                        {{ $errors->first('org_address') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            <div class="form-group {{ $errors->has('org_website') ? 'has-error' : '' }}">
                                <label for="org_website">{{ trans('cruds.user.fields.org_website') }}*</label>
                                <input type="text" id="org_website" name="org_website" class="form-control" value="" placeholder="Enter Organization Website*" required>
                                @if($errors->has('org_website'))
                                    <p class="help-block">
                                        {{ $errors->first('org_website') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            <div class="form-group {{ $errors->has('spoc_dev') ? 'has-error' : '' }}">
                                <label for="spoc_dev">{{ trans('cruds.user.fields.spoc_dev') }}*</label>
                                <input type="text" id="spoc_dev" name="spoc_dev" class="form-control " value="" placeholder="Enter  SPOC Dev Mantra*" required>
                                @if($errors->has('spoc_dev'))
                                    <p class="help-block">
                                        {{ $errors->first('spoc_dev') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            {{---------------}}

                            <div>
                                <button class="btn btn-success" type="submit" value="{{ trans('global.save') }}">{{ trans('global.save') }}</button>
                            </div>
                        </form>


                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
