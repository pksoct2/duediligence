@extends('layouts.admin')
@section('content')
    <div class="content">

        <div class="row justify-content-center">
            <div class="col-lg-8 col-sm-9 col-md-9">

                <div class="panel panel-default">
                    @if (session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="panel-heading h3 p-2 bg-light font-weight-bold border">
                        <span>Master Company Uploads All Documents</span>
                    </div>
                    <div class="panel-body p-2">

                        <form action="{{ route("masterCompanyUpload") }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="users_name">Select {{ trans('cruds.user.fields.name') }}*</label>

                                        <select name="users_name" id="users_name" class="form-control select2" required>
                                            @foreach($showAllUser as $user)

                                                <option value="{{$user->name}}">{{$user->name}}</option>

                                            @endforeach
                                        </select>
                                        @if($errors->has('name'))
                                            <p class="help-block">
                                                {{ $errors->first('name') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.roles_helper') }}
                                        </p>
                                    </div>

                                </div>

                            </div>
                            <div class="form-group {{ $errors->has('pan_income_tax_img') ? 'has-error' : '' }}">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="email">{{ trans('cruds.user.fields.pan_income_tax_img') }}*</label>
                                        <input type="text" id="pan_income_tax" name="pan_income_tax" class="form-control" value="{{ old('pan_income_tax', isset($user) ? $user->pan_income_tax : '') }}" placeholder="Enter Details*" required>
                                        @if($errors->has('email'))
                                            <p class="help-block">
                                                {{ $errors->first('email') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="pan_income_tax_img">{{ trans('cruds.user.fields.pan_income_tax_img') }}*</label>
                                        <input id="pan_income_tax_img" type="file" value="" class="form-control @error('pan_income_tax_img') is-invalid @enderror" name="pan_income_tax_img" >
                                        @if($errors->has('pan_income_tax_img'))
                                            <p class="help-block">
                                                {{ $errors->first('pan_income_tax_img') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>

                                </div>

                            </div>
                            <div class="form-group {{ $errors->has('tan_traces_img') ? 'has-error' : '' }}">
                                <div class="row">
                                    <div class="col-sm-6">
                                            <label for="email">{{ trans('cruds.user.fields.tan_traces_img') }}*</label>
                                            <input type="text" id="tan_traces" name="tan_traces" class="form-control" value="{{ old('tan_traces', isset($user) ? $user->tan_traces : '') }}" placeholder="Enter Details*" required>
                                            @if($errors->has('tan_traces'))
                                                <p class="help-block">
                                                    {{ $errors->first('tan_traces') }}
                                                </p>
                                            @endif
                                            <p class="helper-block">
                                                {{ trans('cruds.user.fields.email_helper') }}
                                            </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="tan_traces_img">{{ trans('cruds.user.fields.tan_traces_img') }}*</label>

                                        <input id="tan_traces_img" type="file" value="" class="form-control @error('tan_traces_img') is-invalid @enderror" name="tan_traces_img" >
                                        @if($errors->has('tan_traces_img'))
                                            <p class="help-block">
                                                {{ $errors->first('tan_traces_img') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>

                            </div>
                            <div class="form-group {{ $errors->has('certificate_of_incorp_img') ? 'has-error' : '' }}">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="email">{{ trans('cruds.user.fields.certificate_of_incorp_img') }}*</label>
                                        <input type="text" id="certificate_of_incorp" name="certificate_of_incorp" class="form-control" value="{{ old('certificate_of_incorp', isset($user) ? $user->certificate_of_incorp : '') }}" placeholder="Enter Details*" required>
                                        @if($errors->has('certificate_of_incorp'))
                                            <p class="help-block">
                                                {{ $errors->first('certificate_of_incorp') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="certificate_of_incorp_img">{{ trans('cruds.user.fields.certificate_of_incorp_img') }}*</label>

                                        <input id="certificate_of_incorp_img" type="file" value="" class="form-control @error('certificate_of_incorp_img') is-invalid @enderror" name="certificate_of_incorp_img" >
                                        @if($errors->has('certificate_of_incorp_img'))
                                            <p class="help-block">
                                                {{ $errors->first('certificate_of_incorp_img') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                            </div>
                            <div class="form-group {{ $errors->has('memo_of_association_img') ? 'has-error' : '' }}">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="email">{{ trans('cruds.user.fields.memo_of_association_img') }}*</label>
                                        <input type="text" id="memo_of_association" name="memo_of_association" class="form-control" value="{{ old('memo_of_association', isset($user) ? $user->memo_of_association : '') }}" placeholder="Enter Details*" required>
                                        @if($errors->has('memo_of_association'))
                                            <p class="help-block">
                                                {{ $errors->first('memo_of_association') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="memo_of_association_img">{{ trans('cruds.user.fields.memo_of_association_img') }}*</label>

                                        <input id="memo_of_association_img" type="file" value="" class="form-control @error('memo_of_association_img') is-invalid @enderror" name="memo_of_association_img" >
                                        @if($errors->has('memo_of_association_img'))
                                            <p class="help-block">
                                                {{ $errors->first('memo_of_association_img') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                            </div>
                            <div class="form-group {{ $errors->has('articles_of_association_img') ? 'has-error' : '' }}">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="email">{{ trans('cruds.user.fields.articles_of_association_img') }}*</label>
                                        <input type="text" id="articles_of_association" name="articles_of_association" class="form-control" value="{{ old('articles_of_association', isset($user) ? $user->articles_of_association : '') }}" placeholder="Enter Details*" required>
                                        @if($errors->has('articles_of_association'))
                                            <p class="help-block">
                                                {{ $errors->first('articles_of_association') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="articles_of_association_img">{{ trans('cruds.user.fields.articles_of_association_img') }}*</label>

                                        <input id="articles_of_association_img" type="file" value="" class="form-control @error('articles_of_association_img') is-invalid @enderror" name="articles_of_association_img" >
                                        @if($errors->has('articles_of_association_img'))
                                            <p class="help-block">
                                                {{ $errors->first('articles_of_association_img') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                            </div>
                            <div class="form-group {{ $errors->has('gst_tax_cert_img') ? 'has-error' : '' }}">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="email">{{ trans('cruds.user.fields.gst_tax_cert_img') }}*</label>
                                        <input type="text" id="gst_tax_cert" name="gst_tax_cert" class="form-control" value="{{ old('gst_tax_cert', isset($user) ? $user->gst_tax_cert : '') }}" placeholder="Enter Details*" required>
                                        @if($errors->has('gst_tax_cert'))
                                            <p class="help-block">
                                                {{ $errors->first('gst_tax_cert') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="gst_tax_cert_img">{{ trans('cruds.user.fields.gst_tax_cert_img') }}*</label>

                                        <input id="gst_tax_cert_img" type="file" value="" class="form-control @error('gst_tax_cert_img') is-invalid @enderror" name="gst_tax_cert_img" >
                                        @if($errors->has('gst_tax_cert_img'))
                                            <p class="help-block">
                                                {{ $errors->first('gst_tax_cert_img') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                            </div>
                            <div class="form-group {{ $errors->has('lut_cert_img') ? 'has-error' : '' }}">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="email">{{ trans('cruds.user.fields.lut_cert_img') }}*</label>
                                        <input type="text" id="lut_cert" name="lut_cert" class="form-control" value="{{ old('lut_cert', isset($user) ? $user->lut_cert : '') }}" placeholder="Enter Details*" required>
                                        @if($errors->has('lut_cert'))
                                            <p class="help-block">
                                                {{ $errors->first('lut_cert') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="lut_cert_img">{{ trans('cruds.user.fields.lut_cert_img') }}*</label>

                                        <input id="lut_cert_img" type="file" value="" class="form-control @error('lut_cert_img') is-invalid @enderror" name="lut_cert_img" >
                                        @if($errors->has('lut_cert_img'))
                                            <p class="help-block">
                                                {{ $errors->first('lut_cert_img') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                            </div>
                            <div class="form-group {{ $errors->has('gst_e_com_cert_img') ? 'has-error' : '' }}">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="email">{{ trans('cruds.user.fields.gst_e_com_cert_img') }}*</label>
                                        <input type="text" id="gst_e_com_cert" name="gst_e_com_cert" class="form-control" value="{{ old('gst_e_com_cert', isset($user) ? $user->gst_e_com_cert : '') }}" placeholder="Enter Details*" required>
                                        @if($errors->has('gst_e_com_cert'))
                                            <p class="help-block">
                                                {{ $errors->first('gst_e_com_cert') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="gst_e-com_cert_img">{{ trans('cruds.user.fields.gst_e_com_cert_img') }}*</label>

                                        <input id="gst_e_com_cert_img" type="file" value="" class="form-control @error('gst_e_com_cert_img') is-invalid @enderror" name="gst_e_com_cert_img" >
                                        @if($errors->has('gst_e_com_cert_img'))
                                            <p class="help-block">
                                                {{ $errors->first('gst_e_com_cert_img') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                            </div>
                            <div class="form-group {{ $errors->has('prof_tax_enrollment_cert_img') ? 'has-error' : '' }}">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="email">{{ trans('cruds.user.fields.prof_tax_enrollment_cert_img') }}*</label>
                                        <input type="text" id="prof_tax_enrollment_cert" name="prof_tax_enrollment_cert" class="form-control" value="{{ old('prof_tax_enrollment_cert', isset($user) ? $user->prof_tax_enrollment_cert : '') }}" placeholder="Enter Details*" required>
                                        @if($errors->has('prof_tax_enrollment_cert'))
                                            <p class="help-block">
                                                {{ $errors->first('prof_tax_enrollment_cert') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="prof_tax_enrollment_cert_img">{{ trans('cruds.user.fields.prof_tax_enrollment_cert_img') }}*</label>

                                        <input id="prof_tax_enrollment_cert_img" type="file" value="" class="form-control @error('prof_tax_enrollment_cert_img') is-invalid @enderror" name="prof_tax_enrollment_cert_img" >
                                        @if($errors->has('prof_tax_enrollment_cert_img'))
                                            <p class="help-block">
                                                {{ $errors->first('prof_tax_enrollment_cert_img') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                            </div>
                            <div class="form-group {{ $errors->has('prof_tax_register_cert_img') ? 'has-error' : '' }}">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="email">{{ trans('cruds.user.fields.prof_tax_register_cert_img') }}*</label>
                                        <input type="text" id="prof_tax_register_cert" name="prof_tax_register_cert" class="form-control" value="{{ old('prof_tax_register_cert', isset($user) ? $user->prof_tax_register_cert : '') }}" placeholder="Enter Details*" required>
                                        @if($errors->has('prof_tax_register_cert'))
                                            <p class="help-block">
                                                {{ $errors->first('prof_tax_register_cert') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="prof_tax_register_cert_img">{{ trans('cruds.user.fields.prof_tax_register_cert_img') }}*</label>

                                        <input id="prof_tax_register_cert_img" type="file" value="" class="form-control @error('prof_tax_register_cert_img') is-invalid @enderror" name="prof_tax_register_cert_img" >
                                        @if($errors->has('prof_tax_register_cert_img'))
                                            <p class="help-block">
                                                {{ $errors->first('prof_tax_register_cert_img') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                            </div>
                            <div class="form-group {{ $errors->has('pf_establish_cert_img') ? 'has-error' : '' }}">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="email">{{ trans('cruds.user.fields.pf_establish_cert_img') }}*</label>
                                        <input type="text" id="pf_establish_cert" name="pf_establish_cert" class="form-control" value="{{ old('pf_establish_cert', isset($user) ? $user->pf_establish_cert : '') }}" placeholder="Enter Details*" required>
                                        @if($errors->has('pf_establish_cert'))
                                            <p class="help-block">
                                                {{ $errors->first('pf_establish_cert') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="pf_establish_cert_img">{{ trans('cruds.user.fields.pf_establish_cert_img') }}*</label>

                                        <input id="pf_establish_cert_img" type="file" value="" class="form-control @error('pf_establish_cert_img') is-invalid @enderror" name="pf_establish_cert_img" >
                                        @if($errors->has('pf_establish_cert_img'))
                                            <p class="help-block">
                                                {{ $errors->first('pf_establish_cert_img') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                            </div>
                            <div class="form-group {{ $errors->has('esic_register_cert_img') ? 'has-error' : '' }}">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="email">{{ trans('cruds.user.fields.esic_register_cert_img') }}*</label>
                                        <input type="text" id="esic_register_cert" name="esic_register_cert" class="form-control" value="{{ old('esic_register_cert', isset($user) ? $user->esic_register_cert : '') }}" placeholder="Enter Details*" required>
                                        @if($errors->has('esic_register_cert'))
                                            <p class="help-block">
                                                {{ $errors->first('esic_register_cert') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="esic_register_cert_img">{{ trans('cruds.user.fields.esic_register_cert_img') }}*</label>

                                        <input id="esic_register_cert_img" type="file" value="" class="form-control @error('esic_register_cert_img') is-invalid @enderror" name="esic_register_cert_img" >
                                        @if($errors->has('esic_register_cert_img'))
                                            <p class="help-block">
                                                {{ $errors->first('esic_register_cert_img') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                            </div>
                            <div class="form-group {{ $errors->has('shops_establish_cert_trade_licence_img') ? 'has-error' : '' }}">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="email">{{ trans('cruds.user.fields.shops_establish_cert_trade_licence_img') }}*</label>
                                        <input type="text" id="shops_establish_cert_trade_licence" name="shops_establish_cert_trade_licence" class="form-control" value="{{ old('shops_establish_cert_trade_licence', isset($user) ? $user->shops_establish_cert_trade_licence : '') }}" placeholder="Enter Details*" required>
                                        @if($errors->has('shops_establish_cert_trade_licence'))
                                            <p class="help-block">
                                                {{ $errors->first('shops_establish_cert_trade_licence') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="shops_establish_cert_trade_licence_img">{{ trans('cruds.user.fields.shops_establish_cert_trade_licence_img') }}*</label>

                                        <input id="shops_establish_cert_trade_licence_img" type="file" value="" class="form-control @error('shops_establish_cert_trade_licence_img') is-invalid @enderror" name="shops_establish_cert_trade_licence_img" >
                                        @if($errors->has('shops_establish_cert_trade_licence_img'))
                                            <p class="help-block">
                                                {{ $errors->first('shops_establish_cert_trade_licence_img') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                            </div>
                            <div class="form-group {{ $errors->has('iec_img') ? 'has-error' : '' }}">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="email">{{ trans('cruds.user.fields.iec_img') }}*</label>
                                        <input type="text" id="iec" name="iec" class="form-control" value="{{ old('iec', isset($user) ? $user->iec : '') }}" placeholder="Enter Details*" required>
                                        @if($errors->has('iec'))
                                            <p class="help-block">
                                                {{ $errors->first('iec') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="iec_img">{{ trans('cruds.user.fields.iec_img') }}*</label>

                                        <input id="iec_img" type="file" value="" class="form-control @error('iec_img') is-invalid @enderror" name="iec_img" >
                                        @if($errors->has('iec_img'))
                                            <p class="help-block">
                                                {{ $errors->first('iec_img') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                            </div>
                            <div class="form-group {{ $errors->has('msmed_cert_img') ? 'has-error' : '' }}">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="email">{{ trans('cruds.user.fields.msmed_cert_img') }}*</label>
                                        <input type="text" id="msmed_cert" name="msmed_cert" class="form-control" value="{{ old('msmed_cert', isset($user) ? $user->msmed_cert : '') }}" placeholder="Enter Details*" required>
                                        @if($errors->has('msmed_cert'))
                                            <p class="help-block">
                                                {{ $errors->first('msmed_cert') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="msmed_cert_img">{{ trans('cruds.user.fields.msmed_cert_img') }}*</label>

                                        <input id="msmed_cert_img" type="file" value="" class="form-control @error('msmed_cert_img') is-invalid @enderror" name="msmed_cert_img" >
                                        @if($errors->has('msmed_cert_img'))
                                            <p class="help-block">
                                                {{ $errors->first('msmed_cert_img') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                            </div>
                            <div class="form-group {{ $errors->has('startup_cert_img') ? 'has-error' : '' }}">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="email">{{ trans('cruds.user.fields.startup_cert_img') }}*</label>
                                        <input type="text" id="startup_cert" name="startup_cert" class="form-control" value="{{ old('startup_cert', isset($user) ? $user->startup_cert : '') }}" placeholder="Enter Details*" required>
                                        @if($errors->has('startup_cert'))
                                            <p class="help-block">
                                                {{ $errors->first('startup_cert') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="startup_cert_img">{{ trans('cruds.user.fields.startup_cert_img') }}*</label>

                                        <input id="startup_cert_img" type="file" value="" class="form-control @error('startup_cert_img') is-invalid @enderror" name="startup_cert_img" >
                                        @if($errors->has('startup_cert_img'))
                                            <p class="help-block">
                                                {{ $errors->first('startup_cert_img') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                            </div>
                            <div class="form-group {{ $errors->has('lower_tax_deduction_cert_img') ? 'has-error' : '' }}">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="email">{{ trans('cruds.user.fields.lower_tax_deduction_cert_img') }}*</label>
                                        <input type="text" id="lower_tax_deduction_cert" name="lower_tax_deduction_cert" class="form-control" value="{{ old('lower_tax_deduction_cert', isset($user) ? $user->lower_tax_deduction_cert : '') }}" placeholder="Enter Details*" required>
                                        @if($errors->has('lower_tax_deduction_cert'))
                                            <p class="help-block">
                                                {{ $errors->first('lower_tax_deduction_cert') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="lower_tax_deduction_cert_img">{{ trans('cruds.user.fields.lower_tax_deduction_cert_img') }}*</label>

                                        <input id="lower_tax_deduction_cert_img" type="file" value="" class="form-control @error('lower_tax_deduction_cert_img') is-invalid @enderror" name="lower_tax_deduction_cert_img" >
                                        @if($errors->has('lower_tax_deduction_cert_img'))
                                            <p class="help-block">
                                                {{ $errors->first('lower_tax_deduction_cert_img') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                            </div>
                            <div class="form-group {{ $errors->has('tax_exemption_cert_us12a_img') ? 'has-error' : '' }}">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="email">{{ trans('cruds.user.fields.tax_exemption_cert_us12a_img') }}*</label>
                                        <input type="text" id="tax_exemption_cert_us12a" name="tax_exemption_cert_us12a" class="form-control" value="{{ old('tax_exemption_cert_us12a', isset($user) ? $user->tax_exemption_cert_us12a : '') }}" placeholder="Enter Details*" required>
                                        @if($errors->has('tax_exemption_cert_us12a'))
                                            <p class="help-block">
                                                {{ $errors->first('tax_exemption_cert_us12a') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="tax_exemption_cert_us12a_img">{{ trans('cruds.user.fields.tax_exemption_cert_us12a_img') }}*</label>

                                        <input id="tax_exemption_cert_us12a_img" type="file" value="" class="form-control @error('tax_exemption_cert_us12a_img') is-invalid @enderror" name="tax_exemption_cert_us12a_img" >
                                        @if($errors->has('tax_exemption_cert_us12a_img'))
                                            <p class="help-block">
                                                {{ $errors->first('tax_exemption_cert_us12a_img') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                            </div>
                            <div class="form-group {{ $errors->has('tax_exemption_cert_us80g_img') ? 'has-error' : '' }}">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="email">{{ trans('cruds.user.fields.tax_exemption_cert_us80g_img') }}*</label>
                                        <input type="text" id="tax_exemption_cert_us80g" name="tax_exemption_cert_us80g" class="form-control" value="{{ old('tax_exemption_cert_us80g', isset($user) ? $user->tax_exemption_cert_us80g : '') }}" placeholder="Enter Details*" required>
                                        @if($errors->has('tax_exemption_cert_us80g'))
                                            <p class="help-block">
                                                {{ $errors->first('tax_exemption_cert_us80g') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="tax_exemption_cert_us80g_img">{{ trans('cruds.user.fields.tax_exemption_cert_us80g_img') }}*</label>

                                        <input id="tax_exemption_cert_us80g_img" type="file" value="" class="form-control @error('tax_exemption_cert_us80g_img') is-invalid @enderror" name="tax_exemption_cert_us80g_img" >

                                        @if($errors->has('tax_exemption_cert_us80g_img'))
                                            <p class="help-block">
                                                {{ $errors->first('tax_exemption_cert_us80g_img') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                            </div>

                            <div class="form-group {{ $errors->has('any_other_details') ? 'has-error' : '' }}">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="email">{{ trans('cruds.user.fields.any_other_details') }}*</label>
                                        <input type="text" id="any_other_details_etc" name="any_other_details_etc" class="form-control" value="{{ old('any_other_details_etc', isset($user) ? $user->any_other_details_etc : '') }}" placeholder="Enter Details*" required>
                                        @if($errors->has('any_other_details_etc'))
                                            <p class="help-block">
                                                {{ $errors->first('any_other_details_etc') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.email_helper') }}
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="any_other_details">{{ trans('cruds.user.fields.any_other_details') }}*</label>
                                        <input id="any_other_details" type="file" value="" class="form-control @error('any_other_details') is-invalid @enderror" name="any_other_details" >

                                    @if($errors->has('any_other_details'))
                                            <p class="help-block">
                                                {{ $errors->first('any_other_details') }}
                                            </p>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.user.fields.name_helper') }}
                                        </p>
                                    </div>
                            </div>
                            {{---------------}}

                            <div>
                                <button class="btn btn-success" type="submit" value="{{ trans('global.save') }}">{{ trans('global.save') }}</button>
                            </div>
                        </form>


                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
