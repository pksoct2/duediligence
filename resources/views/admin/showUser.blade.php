<style>
    table.dataTable tbody th, table.dataTable tbody td {
        padding: 0px 10px;
    }
</style>
@extends('layouts.admin')
@section('content')
    <?php $showUser = 'showUser';?>
    <div class="content">

            <div style="margin-bottom: 10px;" class="row">
                <div class="col-lg-12">
                    <a class="btn btn-success" href="{{route("storeUser")}}">
                        {{ trans('global.add') }} {{ trans('cruds.user.title_singular') }}
                    </a>
                </div>
            </div>

        <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-default">
                    <div class="panel-heading breadcrumb p-2 font-weight-bold">
                        {{ trans('cruds.user.title_singular') }} {{ trans('global.list') }}
                    </div>
                    <div class="panel-body">

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover datatable datatable-User">
                                <thead>
                                <tr>
                                    <th width="8">

                                    </th>
                                    <th>
                                        {{ trans('cruds.user.fields.id') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.user.fields.name') }}
                                    </th>
                                    <th>
                                        Username
                                    </th>

                                    <th>
                                        References
                                    </th>

                                    <th>
                                       Edit, Update, View
                                    </th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($userShow as $key => $user)
                                    <tr data-entry-id="{{ $user->id }}">
                                        <td class="">

                                        </td>
                                        <td>
                                            {{ $user->id ?? '' }}
                                        </td>
                                        <td>
                                            {{ $user->name ?? '' }}
                                        </td>
                                        <td>
                                            {{ $user->username ?? '' }}
                                        </td>

                                        <td>
                                            {{ $user->spoc_dev ?? '' }}
                                        </td>

                                        <td class="d-flex flex-row bd-highlight mb-3">
{{--                                            <a class="btn btn-primary btn-sm m-1" href="{{ route('masterCompanyUpload') }}">--}}
{{--                                                <i class="fa-fw fas fa-upload">--}}

{{--                                                </i>--}}

{{--                                            </a>--}}
{{--                                                <a class="btn btn-primary btn-sm m-1" href="{{ route('showUserData', $user->id) }}">--}}
{{--                                                    <i class="fa-fw fas fa-eye">--}}

{{--                                                    </i>--}}

{{--                                                </a>--}}

                                                <a class="btn btn-sm btn-info m-1" href="{{ route('editUser', $user->id) }}">
                                                    <i class="fa-fw fas fa-edit">

                                                    </i>
                                                </a>

                                                    <a class="btn btn-sm btn-danger m-1" href="{{ route('delete', $user->id) }}">
                                                        <i class="fa-fw fas fa-trash-alt">
                                                        </i>
                                                    </a>

{{--                                                <form action="{{ route('destroyUser', ['id'=>$user->id]) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">--}}
{{--                                                    <input type="hidden" name="_method" value="DELETE">--}}
{{--                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
{{--                                                    <input type="submit" class="btn btn-danger btn-sm" value="{{ trans('global.delete') }}">--}}
{{--                                                </form>--}}


                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script>
        $(function () {
            let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
                @can('user_delete')
            let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
            let deleteButton = {
                text: deleteButtonTrans,
                url: "{{ route('destroyUser') }}",
                className: 'btn-danger',
                action: function (e, dt, node, config) {
                    var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
                        return $(entry).data('entry-id')
                    });

                    if (ids.length === 0) {
                        alert('{{ trans('global.datatables.zero_selected') }}')

                        return
                    }

                    if (confirm('{{ trans('global.areYouSure') }}')) {
                        $.ajax({
                            headers: {'x-csrf-token': _token},
                            method: 'POST',
                            url: config.url,
                            data: { ids: ids, _method: 'DELETE' }})
                            .done(function () { location.reload() })
                    }
                }
            }
            dtButtons.push(deleteButton)
            @endcan

            $.extend(true, $.fn.dataTable.defaults, {
                order: [[ 1, 'desc' ]],
                pageLength: 100,
            });
            $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
                $($.fn.dataTable.tables(true)).DataTable()
                    .columns.adjust();
            });
        })

    </script>
@endsection
