@extends('layouts.admin')
@section('content')
    <?php $quickInfo = 'quickInfo';?>
    <div class="content">

        <div class="row justify-content-center">
            <div class="col-lg-8 col-sm-9 col-md-9">

                <div class="panel panel-default">
                    @if (session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="panel-heading h3 p-2 bg-light font-weight-bold border breadcrumb">
                        Quick Info Update
                    </div>
                    <div class="panel-body p-2">

                        <form action="{{ route("quickInfo") }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @php// dd($info ?? ''); @endphp
                            <div class="form-group {{ $errors->has('quick_info') ? 'has-error' : '' }}">
                                <label for="quick_info">{{ trans('cruds.user.fields.quick_info') }}*</label>
                                <input type="text" id="quick_info" name="quick_info" class="form-control" value="{{$info['quick_info'] ?? ''}}" placeholder="Enter Quick Info*" required>
                                @if($errors->has('quick_info'))
                                    <p class="help-block">
                                        {{ $errors->first('quick_info') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            <div class="form-group {{ $errors->has('tds_rates') ? 'has-error' : '' }}">
                                <label for="tds_rates">{{ trans('cruds.user.fields.tds_rates') }}*</label>
                                <input type="text" id="tds_rates" name="tds_rates" class="form-control" value="{{$info['tds_rates'] ?? ''}}" placeholder="Enter TDS Rates*" required>
                                @if($errors->has('tds_rates'))
                                    <p class="help-block">
                                        {{ $errors->first('tds_rates') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            <div class="form-group {{ $errors->has('gst_rates') ? 'has-error' : '' }}">
                                <label for="gst_rates">{{ trans('cruds.user.fields.gst_rates') }}*</label>
                                <input type="text" id="category" name="gst_rates" class="form-control" value="{{$info['tds_rates'] ?? ''}}" placeholder="Enter GST Rates*" required>
                                @if($errors->has('gst_rates'))
                                    <p class="help-block">
                                        {{ $errors->first('gst_rates') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>

                            <div class="form-group {{ $errors->has('quick_glances') ? 'has-error' : '' }}">
                                <label for="quick_glances">{{ trans('cruds.user.fields.quick_glances') }}*</label>
                                <input type="text" id="quick_glances" name="quick_glances" class="form-control" value="{{ $info['quick_glances'] ?? '' }}" placeholder="Enter A Quick Glance - E Way Bill*" required>
                                @if($errors->has('quick_glances'))
                                    <p class="help-block">
                                        {{ $errors->first('quick_glances') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>



                            {{---------------}}

                            <div>
                                <button class="btn btn-success" type="submit" value="{{ trans('global.save') }}">{{ trans('global.save') }}</button>
                            </div>
                        </form>


                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
