@extends('layouts.admin')
@section('content')
    <?php $storeUser = 'storeUser';?>
<div class="content">

    <div class="row justify-content-center">
        <div class="col-lg-8 col-sm-9 col-md-9">

            <div class="panel panel-default">
                @if (session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('success') }}
                    </div>
                @endif
                <div class="panel-heading h3 p-2 bg-light font-weight-bold border breadcrumb">
                    {{ trans('global.create') }} {{ trans('cruds.user.title_singular') }}
                </div>
                <div class="panel-body p-2">

                    <form action="{{ route("storeUser") }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label for="name">{{ trans('cruds.user.fields.name') }}*</label>
                            <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($user) ? $user->name : '') }}" placeholder="Enter Organization Name" required>
                            @if($errors->has('name'))
                                <p class="help-block">
                                    {{ $errors->first('name') }}
                                </p>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                        </div>

                        <div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
                            <label for="username">User Name*</label>
                            <input type="text" id="username" name="username" class="form-control" value="{{ old('username', isset($user) ? $user->username : '') }}" placeholder="Enter Username*" required>
                            @if($errors->has('username'))
                                <p class="help-block">
                                    {{ $errors->first('username') }}
                                </p>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.email_helper') }}
                            </p>
                        </div>

                        <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                            <label for="password">{{ trans('cruds.user.fields.password') }}</label>
                            <input type="password" id="password" name="password" class="form-control" placeholder="Enter Password*" required>
                            @if($errors->has('password'))
                                <p class="help-block">
                                    {{ $errors->first('password') }}
                                </p>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.password_helper') }}
                            </p>
                        </div>

                        <div class="form-group {{ $errors->has('spoc_dev') ? 'has-error' : '' }}">
                            <label for="spoc_dev">References*</label>
                            <input type="text" id="spoc_dev" name="spoc_dev" class="form-control " value="{{ old('spoc_dev', isset($user) ? $user->spoc_dev : '') }}" placeholder="Enter  SPOC Dev Mantra*" required>
                            @if($errors->has('spoc_dev'))
                                <p class="help-block">
                                    {{ $errors->first('spoc_dev') }}
                                </p>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                        </div>
                        {{---------------}}

                        <div>
                            <button class="btn btn-success" type="submit" value="{{ trans('global.save') }}">{{ trans('global.save') }}</button>
                        </div>
                    </form>


                </div>
            </div>

        </div>
    </div>
</div>
@endsection
