@extends('layouts.admin')
@section('content')
    <div class="content">

        <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-default">
                    <div class="panel-heading p-2 font-weight-bold breadcrumb h3">
                        {{ trans('global.show') }} {{ trans('cruds.user.title') }}
                    </div>
                    <div class="panel-body">

                        <div class="form-group">
                            <table class="table table-bordered table-striped">
                                <tbody>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.id') }}
                                    </th>
                                    <td>
                                        {{ $showUserEntry->id }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.name') }}
                                    </th>
                                    <td>
                                        {{ $showUserEntry->name }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.email') }}
                                    </th>
                                    <td>
                                        {{ $showUserEntry->email }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.mobile') }}
                                    </th>
                                    <td>
                                        {{ $showUserEntry->mobile }}
                                    </td>
                                </tr>

                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.bank_name') }}
                                    </th>
                                    <td>
                                        {{ $showUserEntry->mobile }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.bank_account') }}
                                    </th>
                                    <td>
                                        {{ $showUserEntry->bank_account }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.date_of_incorp') }}
                                    </th>
                                    <td>
                                        {{ $showUserEntry->date_of_incorp }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.org_address') }}
                                    </th>
                                    <td>
                                        {{ $showUserEntry->org_address }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.org_website') }}
                                    </th>
                                    <td>
                                        {{ $showUserEntry->org_website }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.spoc_dev') }}
                                    </th>
                                    <td>
                                        {{ $showUserEntry->spoc_dev }}
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                                <h3 class="breadcrumb font-weight-bold p-2 ">Master Company Documents</h3>
                                <table class="table table-bordered table-striped">
                                    <tbody>
                                    <tr>
                                        <th>
                                            {{ trans('cruds.user.fields.id') }}
                                        </th>
                                        <td>
                                            {{ $masterComapnyEntry->user_id }}

                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            {{ trans('cruds.user.fields.name') }}
                                        </th>
                                        <td>
                                            {{ $masterComapnyEntry->users_name }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            {{ trans('cruds.user.fields.pan_income_tax_img') }}
                                        </th>
                                        <td>
                                            <button type="button" data-src="/userdoc/{{ $masterComapnyEntry->pan_income_tax_img }}" class="btn btn-outline-secondary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                            <a class="btn btn-outline-secondary" href="/userdoc/{{ $masterComapnyEntry->pan_income_tax_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            {{ trans('cruds.user.fields.tan_traces_img') }}
                                        </th>
                                        <td>
                                            <button type="button" data-src="/userdoc/{{ $masterComapnyEntry->tan_traces_img}}" class="btn btn-outline-secondary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                            <a class="btn btn-outline-secondary" href="/userdoc/{{ $masterComapnyEntry->tan_traces_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>
                                            {{ trans('cruds.user.fields.certificate_of_incorp_img') }}
                                        </th>
                                        <td>
                                            <button type="button"  data-src="/userdoc/{{ $masterComapnyEntry->certificate_of_incorp_img}}" class="btn btn-outline-secondary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                            <a class="btn btn-outline-secondary" href="/userdoc/{{ $masterComapnyEntry->certificate_of_incorp_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            {{ trans('cruds.user.fields.memo_of_association_img') }}
                                        </th>
                                        <td>
                                            <button type="button" data-src="/userdoc/{{ $masterComapnyEntry->memo_of_association_img}}" class="btn btn-outline-secondary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                            <a class="btn btn-outline-secondary" href="/userdoc/{{ $masterComapnyEntry->memo_of_association_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            {{ trans('cruds.user.fields.articles_of_association_img') }}
                                        </th>
                                        <td>
                                            <button type="button" data-src="/userdoc/{{ $masterComapnyEntry->articles_of_association_img}}" class="btn btn-outline-secondary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                            <a class="btn btn-outline-secondary" href="/userdoc/{{ $masterComapnyEntry->articles_of_association_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            {{ trans('cruds.user.fields.gst_tax_cert_img') }}
                                        </th>
                                        <td>
                                            <button type="button" data-src="/userdoc/{{ $masterComapnyEntry->gst_tax_cert_img}}" class="btn btn-outline-secondary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                            <a class="btn btn-outline-secondary" href="/userdoc/{{ $masterComapnyEntry->gst_tax_cert_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            {{ trans('cruds.user.fields.lut_cert_img') }}
                                        </th>
                                        <td>
                                            <button type="button" data-src="/userdoc/{{ $masterComapnyEntry->lut_cert_img}}" class="btn btn-outline-secondary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                            <a class="btn btn-outline-secondary" href="/userdoc/{{ $masterComapnyEntry->lut_cert_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            {{ trans('cruds.user.fields.gst_e_com_cert_img') }}
                                        </th>
                                        <td>
                                            <button type="button" data-src="/userdoc/{{ $masterComapnyEntry->gst_e_com_cert_img}}" class="btn btn-outline-secondary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                            <a class="btn btn-outline-secondary" href="/userdoc/{{ $masterComapnyEntry->gst_e_com_cert_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            {{ trans('cruds.user.fields.prof_tax_enrollment_cert_img') }}
                                        </th>
                                        <td>
                                            <button type="button" data-src="/userdoc/{{ $masterComapnyEntry->prof_tax_enrollment_cert_img}}" class="btn btn-outline-secondary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                            <a class="btn btn-outline-secondary" href="/userdoc/{{ $masterComapnyEntry->prof_tax_enrollment_cert_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            {{ trans('cruds.user.fields.prof_tax_register_cert_img') }}
                                        </th>
                                        <td>
                                            <button type="button" data-src="/userdoc/{{ $masterComapnyEntry->prof_tax_register_cert_img}}" class="btn btn-outline-secondary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                            <a class="btn btn-outline-secondary" href="/userdoc/{{ $masterComapnyEntry->prof_tax_register_cert_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>
                                            {{ trans('cruds.user.fields.pf_establish_cert_img') }}
                                        </th>
                                        <td>
                                            <button type="button" data-src="/userdoc/{{ $masterComapnyEntry->pf_establish_cert_img}}" class="btn btn-outline-secondary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                            <a class="btn btn-outline-secondary" href="/userdoc/{{ $masterComapnyEntry->pf_establish_cert_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            {{ trans('cruds.user.fields.esic_register_cert_img') }}
                                        </th>
                                        <td>
                                            <button type="button" data-src="/userdoc/{{ $masterComapnyEntry->esic_register_cert_img}}" class="btn btn-outline-secondary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                            <a class="btn btn-outline-secondary" href="/userdoc/{{ $masterComapnyEntry->esic_register_cert_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            {{ trans('cruds.user.fields.shops_establish_cert_trade_licence_img') }}
                                        </th>
                                        <td>
                                            <button type="button" data-src="/userdoc/{{ $masterComapnyEntry->shops_establish_cert_trade_licence_img}}" class="btn btn-outline-secondary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                            <a class="btn btn-outline-secondary" href="/userdoc/{{ $masterComapnyEntry->shops_establish_cert_trade_licence_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            {{ trans('cruds.user.fields.iec_img') }}
                                        </th>
                                        <td>
                                            <button type="button" data-src="/userdoc/{{ $masterComapnyEntry->iec_img}}" class="btn btn-outline-secondary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                            <a class="btn btn-outline-secondary" href="/userdoc/{{ $masterComapnyEntry->iec_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            {{ trans('cruds.user.fields.msmed_cert_img') }}
                                        </th>
                                        <td>
                                            <button type="button" data-src="/userdoc/{{ $masterComapnyEntry->msmed_cert_img}}" class="btn btn-outline-secondary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                            <a class="btn btn-outline-secondary" href="/userdoc/{{ $masterComapnyEntry->msmed_cert_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            {{ trans('cruds.user.fields.startup_cert_img') }}
                                        </th>
                                        <td>
                                            <button type="button" data-src="/userdoc/{{ $masterComapnyEntry->startup_cert_img}}" class="btn btn-outline-secondary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                            <a class="btn btn-outline-secondary" href="/userdoc/{{ $masterComapnyEntry->startup_cert_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            {{ trans('cruds.user.fields.lower_tax_deduction_cert_img') }}
                                        </th>
                                        <td>
                                            <button type="button" data-src="/userdoc/{{ $masterComapnyEntry->lower_tax_deduction_cert_img}}" class="btn btn-outline-secondary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                            <a class="btn btn-outline-secondary" href="/userdoc/{{ $masterComapnyEntry->lower_tax_deduction_cert_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            {{ trans('cruds.user.fields.tax_exemption_cert_us12a_img') }}
                                        </th>
                                        <td>
                                            <button type="button" data-src="/userdoc/{{ $masterComapnyEntry->tax_exemption_cert_us12a_img}}" class="btn btn-outline-secondary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                            <a class="btn btn-outline-secondary" href="/userdoc/{{ $masterComapnyEntry->tax_exemption_cert_us12a_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            {{ trans('cruds.user.fields.tax_exemption_cert_us80g_img') }}
                                        </th>
                                        <td>
                                            <button type="button" data-src="/userdoc/{{ $masterComapnyEntry->tax_exemption_cert_us80g_img}}" class="btn btn-outline-secondary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                            <a class="btn btn-outline-secondary" href="/userdoc/{{ $masterComapnyEntry->tax_exemption_cert_us80g_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            {{ trans('cruds.user.fields.any_other_details') }}
                                        </th>
                                        <td>
                                            <button type="button" data-src="/userdoc/{{ $masterComapnyEntry->any_other_details}}" class="btn btn-outline-secondary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                            <a class="btn btn-outline-secondary" href="/userdoc/{{ $masterComapnyEntry->any_other_details}}" download><i class="fa-fw fas fa-download"></i></a>
                                        </td>
                                    </tr>

                                    </tbody>
                                </table>
                                <a style="margin-top:20px;" class="btn btn-default" href="{{ url()->previous() }}">
                                    {{ trans('global.back_to_list') }}
                                </a>
{{--                                <a style="margin-top:20px;" class="btn btn-default" href="{{ url()->previous() }}">--}}
{{--                                    {{ trans('global.back_to_list') }}--}}
{{--                                </a>--}}
                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="modal fade bd-example1-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold text-capitalize text-center" id="exampleModalLabel">User Recent Image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <a id="myAnchor" href="" onclick="openwindow()" download>
                        <img src="" class="newmodalImageAppend img-fluid" height="550px" width="550px">
                    </a>
                </div>
            </div>
        </div>
    </div>

    <script !src="">
        $(".bd-example-modal-lg").on('click', function(e) {
            var self = $(this);
            var imagePath = self.attr("data-src");
            var modal = $(".bd-example1-modal-lg");
            var imageElm = $(".newmodalImageAppend");
            var imgdowld = $(".newmodalImageAppend");

            imageElm.attr('src', imagePath);
            modal.modal('show');

        });

    </script>
    <script>
        function openwindow() {
            var imagePath = self.attr("data-src");
            document.getElementById("myAnchor").href = self.attr("data-src");
            document.getElementById("demo").innerHTML = "The link above now goes to www.cnn.com.";
        }
    </script>
@endsection
