<style>
    table.dataTable tbody th, table.dataTable tbody td {
        padding: 0px 10px;
    }
</style>
@extends('layouts.admin')
@section('content')
    <?php $adminRocCompliance = 'adminRocComplianceView';?>
    <div class="content">

        <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-default">
                    <div class="panel-heading breadcrumb p-2 font-weight-bold h2 mb-5">
                        ROC Compliance Data List
                    </div>
                    <div class="panel-body">

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover datatable datatable-User">
                                <thead>
                                <tr>
                                    <th width="8">

                                    </th>

                                    <th>
                                        Company Name
                                    </th>
                                    <th>
                                        Types of Meeting
                                    </th>
                                    <th>
                                        Date of Meeting
                                    </th>
                                    <th>
                                        Agenda
                                    </th>
                                    <th>
                                        Attendance
                                    </th>
                                    <th>
                                        Remarks
                                    </th>


                                </tr>
                                </thead>
                                <tbody>
                                @php //print_r($clientBranch->branch_id); @endphp
                                @foreach($adminRocComplianceView as $key => $userData)
                                    <tr data-entry-id="">
                                        <td class="">

                                        </td>

                                        <td>
                                            {{ $userData->company_name ?? '' }}
                                        </td>
                                        <td>
                                            {{ $userData->type_meeting ?? '' }}
                                        </td>
                                        <td>
                                            {{ $userData->date_meeting ?? '' }}
                                        </td>
                                        <td>
                                            {{ $userData->agenda ?? '' }}
                                        </td>
                                        <td>
                                            {{ $userData->attendance ?? '' }}
                                        </td>
                                        <td>
                                            {{ $userData->remarks ?? '' }}
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent

    <script>
        $(function () {
            let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
                @can('user_delete')
            let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
            let deleteButton = {
                text: deleteButtonTrans,
                url: "{{ route('destroyUser') }}",
                className: 'btn-danger',
                action: function (e, dt, node, config) {
                    var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
                        return $(entry).data('entry-id')
                    });

                    if (ids.length === 0) {
                        alert('{{ trans('global.datatables.zero_selected') }}')

                        return
                    }

                    if (confirm('{{ trans('global.areYouSure') }}')) {
                        $.ajax({
                            headers: {'x-csrf-token': _token},
                            method: 'POST',
                            url: config.url,
                            data: { ids: ids, _method: 'DELETE' }})
                            .done(function () { location.reload() })
                    }
                }
            }
            dtButtons.push(deleteButton)
            @endcan

            $.extend(true, $.fn.dataTable.defaults, {
                order: [[ 1, 'desc' ]],
                pageLength: 100,
            });
            $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
                $($.fn.dataTable.tables(true)).DataTable()
                    .columns.adjust();
            });
        })

    </script>
@endsection
