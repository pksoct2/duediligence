@extends('layouts.admin')
@section('content')
    <div class="content">

        <div class="row justify-content-center">
            <div class="col-lg-8 col-sm-9 col-md-9">

                <div class="panel panel-default">
                    @if (session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="panel-heading h3 p-2 bg-light font-weight-bold border breadcrumb">
                        Compliance Trackers
                    </div>
                    <div class="panel-body p-2">

                        <form action="{{ route("branchComplianceTracker") }}" method="POST" enctype="multipart/form-data">
                            @csrf


                            <div class="row">
                                <div class="col-sm-6">
                                    <label for="users_name">Select {{ trans('cruds.user.fields.name') }}*</label>

                                    <select name="parent_company_name" id="parent_company_name" class="form-control select2" required>
                                        @php use App\Mastercompanies;
                                        $showAllUser = Mastercompanies::all();@endphp
                                        @foreach( $showAllUser as $user)

                                            <option value="{{$user->users_name}}">{{$user->users_name}}</option>

                                        @endforeach
                                    </select>
                                    @if($errors->has('name'))
                                        <p class="help-block">
                                            {{ $errors->first('name') }}
                                        </p>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.user.fields.roles_helper') }}
                                    </p>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label for="branch_name">Select {{ trans('cruds.user.fields.name') }}*</label>

                                    <select name="branch_name" id="branch_name" class="form-control select2" required>
                                        @php use App\MasterCompanyBranch;
                                        $showAllUser = MasterCompanyBranch::all();@endphp

                                        @foreach( $showAllUser as $user)

                                            <option value="{{$user->branch_name}}">{{$user->branch_name}}</option>

                                        @endforeach
                                    </select>
                                    @if($errors->has('name'))
                                        <p class="help-block">
                                            {{ $errors->first('name') }}
                                        </p>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.user.fields.roles_helper') }}
                                    </p>
                                </div>

                            </div>
                            <div class="form-group {{ $errors->has('select_month_year') ? 'has-error' : '' }}">
                                <label for="select_month_year">{{ trans('cruds.user.fields.select_month_year') }}*</label>
                                <input type="month" id="select_month_year" name="select_month_year" class="form-control" value="{{ old('select_month_year', isset($user) ? $user->select_month_year : '') }}" required>
                                @if($errors->has('select_month_year'))
                                    <p class="help-block">
                                        {{ $errors->first('select_month_year') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            <div class="form-group {{ $errors->has('due_date') ? 'has-error' : '' }}">
                                <label for="due_date">{{ trans('cruds.user.fields.due_date') }}*</label>
                                <input type="date" id="due_date" name="due_date" class="form-control" value="{{ old('due_date', isset($user) ? $user->due_date : '') }}" required>
                                @if($errors->has('due_date'))
                                    <p class="help-block">
                                        {{ $errors->first('due_date') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            <div class="form-group {{ $errors->has('category') ? 'has-error' : '' }}">
                                <label for="category">{{ trans('cruds.user.fields.category') }}*</label>
                                <input type="text" id="category" name="category" class="form-control" value="{{ old('category', isset($user) ? $user->category : '') }}" placeholder="Enter Category" required>
                                @if($errors->has('category'))
                                    <p class="help-block">
                                        {{ $errors->first('category') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>

                            <div class="form-group {{ $errors->has('details') ? 'has-error' : '' }}">
                                <label for="details">{{ trans('cruds.user.fields.details') }}*</label>
                                <input type="text" id="details" name="details" class="form-control" value="{{ old('details', isset($user) ? $user->details : '') }}" placeholder="Enter Details*" required>
                                @if($errors->has('details'))
                                    <p class="help-block">
                                        {{ $errors->first('details') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            <div class="form-group {{ $errors->has('compliance_date') ? 'has-error' : '' }}">
                                <label for="compliance_date">{{ trans('cruds.user.fields.compliance_date') }}*</label>
                                <input type="date" id="compliance_date" name="compliance_date" class="form-control" value="{{ old('compliance_date', isset($user) ? $user->compliance_date : '') }}" required>
                                @if($errors->has('compliance_date'))
                                    <p class="help-block">
                                        {{ $errors->first('compliance_date') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            <div class="form-group {{ $errors->has('documents') ? 'has-error' : '' }}">

                                <label for="documents">{{ trans('cruds.user.fields.documents') }}*</label>
                                <input id="documents" type="file" value="" class="form-control @error('documents') is-invalid @enderror" name="documents" >
                                @if($errors->has('documents'))
                                    <p class="help-block">
                                        {{ $errors->first('documents') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.email_helper') }}
                                </p>

                            </div>

                            {{---------------}}

                            <div>
                                <button class="btn btn-success" type="submit" value="{{ trans('global.save') }}">{{ trans('global.save') }}</button>
                            </div>
                        </form>


                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
