@extends('layouts.user')
@section('content')
    <?php $customer = 'customerAgreement';?>
    <div class="content">

        <div class="row justify-content-center">
            <div class="col-lg-8 col-sm-9 col-md-9">

                <div class="panel panel-default">
                    @if (session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="panel-heading h3 p-2 bg-light font-weight-bold border breadcrumb">
                        Customers Agreements Data File
                    </div>
                    <div class="panel-body p-2">

                        <form action="{{ route("customerAgreementSubmit") }}" method="POST" enctype="multipart/form-data">
                            @csrf


                            <div class="form-group {{ $errors->has('customer_agreement') ? 'has-error' : '' }}">

                                <label for="customer_agreement">Customers Agreements*</label>
                                <input id="customer_agreement" type="file" value="" class="form-control @error('customer_agreement') is-invalid @enderror" name="customer_agreement" >
                                @if($errors->has('customer_agreement'))
                                    <p class="help-block">
                                        {{ $errors->first('customer_agreement') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.email_helper') }}
                                </p>
                                <p class="text-danger">Rename_Name_Dateofupload*</p>

                            </div>

                            {{---------------}}

                            <div>
                                <button class="btn btn-success" type="submit" value="{{ trans('global.save') }}">{{ trans('global.save') }}</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
