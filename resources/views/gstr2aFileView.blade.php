<style>
    table.dataTable tbody th, table.dataTable tbody td {
        padding: 0px 10px;
    }
</style>
@extends('layouts.user')
@section('content')
    <?php $gstr2aFile1 = 'gstr2aFileView';?>
    <div class="content">

        <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-default">
                    <div class="panel-heading breadcrumb p-2 font-weight-bold h2 mb-5">
                        GSTR2A Data List
                    </div>
                    <div class="panel-body">

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover datatable datatable-User">
                                <thead>
                                <tr>
                                    <th width="8">

                                    </th>

                                    <th>
                                        Company Name
                                    </th>
                                    <th>
                                    Month/Year
                                    </th>
                                    <th>
                                        GSTR2A
                                    </th>
                                    <th>
                                        Edit
                                    </th>


                                </tr>
                                </thead>
                                <tbody>
                                @php //print_r($clientBranch->branch_id); @endphp
                                @foreach($gstr2aData as $key => $userData)
                                    <tr data-entry-id="">
                                        <td class="">

                                        </td>

                                        <td>
                                            {{ $userData->company_name ?? '' }}
                                        </td>
                                        <td>
                                            {{ $userData->year_month ?? '' }}
                                        </td>

                                        <td>
                                            <button type="button" data-src="/GSTR2A/{{ $userData->gstr2a}}" class="btn btn-outline-secondary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                        </td>

                                        <td class="d-flex flex-row bd-highlight">
                                            <a class="btn btn-outline-primary" href="{{route('editGstr2aFile', $userData->id)}}">
                                                <i class="fa-fw fas fa-edit"></i>
                                            </a>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="modal fade bd-example1-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold text-capitalize text-center" id="exampleModalLabel">User Recent Image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <a id="myAnchor" href="" onclick="openwindow()" download>
                        <img src="" class="newmodalImageAppend img-fluid" height="550px" width="550px">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <script !src="">
        $(".bd-example-modal-lg").on('click', function(e) {
            var self = $(this);
            var imagePath = self.attr("data-src");
            var modal = $(".bd-example1-modal-lg");
            var imageElm = $(".newmodalImageAppend");

            imageElm.attr('src', imagePath);
            modal.modal('show');

        });

    </script>
@endsection
@section('scripts')
    @parent

    <script>
        $(function () {
            let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
                @can('user_delete')
            let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
            let deleteButton = {
                text: deleteButtonTrans,
                url: "{{ route('destroyUser') }}",
                className: 'btn-danger',
                action: function (e, dt, node, config) {
                    var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
                        return $(entry).data('entry-id')
                    });

                    if (ids.length === 0) {
                        alert('{{ trans('global.datatables.zero_selected') }}')

                        return
                    }

                    if (confirm('{{ trans('global.areYouSure') }}')) {
                        $.ajax({
                            headers: {'x-csrf-token': _token},
                            method: 'POST',
                            url: config.url,
                            data: { ids: ids, _method: 'DELETE' }})
                            .done(function () { location.reload() })
                    }
                }
            }
            dtButtons.push(deleteButton)
            @endcan

            $.extend(true, $.fn.dataTable.defaults, {
                order: [[ 1, 'desc' ]],
                pageLength: 100,
            });
            $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
                $($.fn.dataTable.tables(true)).DataTable()
                    .columns.adjust();
            });
        })

    </script>
@endsection
