@extends('layouts.user')
@section('content')
    <?php $employeePay = 'employeePayroll';?>
    <div class="content">

        <div class="row justify-content-center">
            <div class="col-lg-8 col-sm-9 col-md-9">

                <div class="panel panel-default">
                    @if (session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="panel-heading h3 p-2 bg-light font-weight-bold border breadcrumb">
                        Client Employee Payroll File
                    </div>
                    <div class="panel-body p-2">

                        <form action="{{ route("employeePayrollSubmit") }}" method="POST" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group {{ $errors->has('year_month') ? 'has-error' : '' }}">
                                <label for="year_month">Month/Year*</label>
                                <input type="month" id="year_month" name="year_month" class="form-control" value="{{ old('year_month', isset($user) ? $user->year_month : '') }}" required>
                                @if($errors->has('year_month'))
                                    <p class="help-block">
                                        {{ $errors->first('year_month') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>

                            <div class="form-group {{ $errors->has('employee_payroll') ? 'has-error' : '' }}">

                                <label for="employee_payroll">Employee Payroll*</label>
                                <input id="employee_payroll" type="file" value="" class="form-control @error('employee_payroll') is-invalid @enderror" name="employee_payroll" >
                                @if($errors->has('employee_payroll'))
                                    <p class="help-block">
                                        {{ $errors->first('employee_payroll') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.email_helper') }}
                                </p>
                                <p class="text-danger">RenameMonth_dateofupload*</p>

                            </div>

                            {{---------------}}

                            <div>
                                <button class="btn btn-success" type="submit" value="{{ trans('global.save') }}">{{ trans('global.save') }}</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
