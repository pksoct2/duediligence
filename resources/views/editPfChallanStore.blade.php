@extends('layouts.user')
@section('content')
    <div class="content">

        <div class="row justify-content-center">
            <div class="col-lg-8 col-sm-9 col-md-9">

                <div class="panel panel-default">
                    @if (session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="panel-heading h3 p-2 bg-light font-weight-bold border breadcrumb">
                        Client PF Challan File
                    </div>
                    <div class="panel-body p-2">

                        <form action="{{ route("updatePfChallan", $pfChallanUpdate->id ?? '') }}" method="POST" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group {{ $errors->has('year_month') ? 'has-error' : '' }}">
                                <label for="year_month">Year/Month*</label>
                                <input type="month" id="year_month" name="year_month" class="form-control" value="{{ old('year_month', isset($pfChallanUpdate) ? $pfChallanUpdate->year_month ?? '' : '') }}" required>
                                @if($errors->has('year_month'))
                                    <p class="help-block">
                                        {{ $errors->first('year_month') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>

                            <div class="form-group {{ $errors->has('pf_challan') ? 'has-error' : '' }}">

                                <label for="pf_challan">PF Challan*</label>
                                <input id="pf_challan" type="file" name="pf_challan" value="" class="form-control @error('pf_challan') is-invalid @enderror"  >
                                @if($errors->has('pf_challan'))
                                    <p class="help-block">
                                        {{ $errors->first('pf_challan') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.email_helper') }}
                                </p>
                                <p class="text-danger">Rename File as month_Dateofupload*</p>

                            </div>

                            {{---------------}}

                            <div>
                                <button class="btn btn-success" type="submit" value="{{ trans('global.save') }}">{{ trans('global.save') }}</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
