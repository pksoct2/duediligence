@extends('layouts.user')
@section('content')
    <?php $gstr2aFile = 'gstr2aFile';?>
    <div class="content">

        <div class="row justify-content-center">
            <div class="col-lg-8 col-sm-9 col-md-9">

                <div class="panel panel-default">
                    @if (session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="panel-heading h3 p-2 bg-light font-weight-bold border breadcrumb">
                        Client GSTR2A File
                    </div>
                    <div class="panel-body p-2">

                        <form action="{{ route("gstr2aSubmit") }}" method="POST" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group {{ $errors->has('year_month') ? 'has-error' : '' }}">
                                <label for="year_month">Final Year*</label>
                                <input type="month" id="year_month" name="year_month" class="form-control" value="{{ old('year_month', isset($user) ? $user->year_month : '') }}" required>
                                @if($errors->has('year_month'))
                                    <p class="help-block">
                                        {{ $errors->first('year_month') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>

                            <div class="form-group {{ $errors->has('gstr2a') ? 'has-error' : '' }}">

                                <label for="gstr2a">GSTR2A*</label>
                                <input id="gstr2a" type="file" value="" class="form-control @error('gstr2a') is-invalid @enderror" name="gstr2a" >
                                @if($errors->has('gstr2a'))
                                    <p class="help-block">
                                        {{ $errors->first('gstr2a') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.email_helper') }}
                                </p>
                                <p class="text-danger">Rename File as GSTR2A_Month_Dateofupload*</p>

                            </div>

                            {{---------------}}

                            <div>
                                <button class="btn btn-success" type="submit" value="{{ trans('global.save') }}">{{ trans('global.save') }}</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
