@extends('layouts.user')
@section('content')
    <?php $master_Data = 'master_Data';?>
    <div class="content">

        <div class="row justify-content-center">
            <div class="col-lg-8 col-sm-9 col-md-9">

                <div class="panel panel-default">
                    @if (session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="panel-heading h3 p-2 bg-light font-weight-bold border breadcrumb">
                        Client Master Data
                    </div>
                    <div class="panel-body p-2">

                        <form action="{{ route("master_Data") }}" method="POST" enctype="multipart/form-data">
                            @csrf



                            <div class="form-group {{ $errors->has('company_prt__llp_individual') ? 'has-error' : '' }}">
                                <label for="company_prt__llp_individual">Company/Partnership/LLP/Individual*</label>
                                <input type="text" id="company_prt__llp_individual" name="company_prt__llp_individual" class="form-control" value="{{ old('company_prt__llp_individual', isset($user) ? $user->select_month_year : '') }}" placeholder="Enter Company/Partnership/LLP/Individual*" required>
                                @if($errors->has('company_prt__llp_individual'))
                                    <p class="help-block">
                                        {{ $errors->first('company_prt__llp_individual') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            <div class="form-group {{ $errors->has('date_incorporation') ? 'has-error' : '' }}">
                                <label for="date_incorporation">Date of Incorporation*</label>
                                <input type="date" id="date_incorporation" name="date_incorporation" class="form-control" value="{{ old('date_incorporation', isset($user) ? $user->due_date : '') }}" required>
                                @if($errors->has('date_incorporation'))
                                    <p class="help-block">
                                        {{ $errors->first('date_incorporation') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            <div class="form-group {{ $errors->has('pan') ? 'has-error' : '' }}">
                                <label for="pan">PAN*</label>
                                <input type="text" id="pan" name="pan" class="form-control" value="{{ old('pan', isset($user) ? $user->category : '') }}" placeholder="Enter PAN*" required>
                                @if($errors->has('pan'))
                                    <p class="help-block">
                                        {{ $errors->first('pan') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>

                            <div class="form-group {{ $errors->has('tan') ? 'has-error' : '' }}">
                                <label for="tan">TAN*</label>
                                <input type="text" id="tan" name="tan" class="form-control" value="{{ old('tan', isset($user) ? $user->details : '') }}" placeholder="Enter TAN*" required>
                                @if($errors->has('tan'))
                                    <p class="help-block">
                                        {{ $errors->first('tan') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            <div class="form-group {{ $errors->has('pf_reg') ? 'has-error' : '' }}">
                                <label for="pf_reg">PF Reg*</label>
                                <input type="text" id="pf_reg" name="pf_reg" class="form-control" value="{{ old('pf_reg', isset($user) ? $user->select_month_year : '') }}" placeholder="Enter PF Reg*" required>
                                @if($errors->has('pf_reg'))
                                    <p class="help-block">
                                        {{ $errors->first('pf_reg') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            <div class="form-group {{ $errors->has('esi_reg') ? 'has-error' : '' }}">
                                <label for="esi_reg">ESI Reg*</label>
                                <input type="text" id="esi_reg" name="esi_reg" class="form-control" value="{{ old('esi_reg', isset($user) ? $user->due_date : '') }}" placeholder="Enter ESI Reg*" required>
                                @if($errors->has('esi_reg'))
                                    <p class="help-block">
                                        {{ $errors->first('esi_reg') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            <div class="form-group {{ $errors->has('pt_reg') ? 'has-error' : '' }}">
                                <label for="pt_reg">PT Reg*</label>
                                <input type="text" id="pt_reg" name="pt_reg" class="form-control" value="{{ old('pt_reg', isset($user) ? $user->category : '') }}" placeholder="Enter PT Reg*" required>
                                @if($errors->has('pt_reg'))
                                    <p class="help-block">
                                        {{ $errors->first('pt_reg') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>

                            <div class="form-group {{ $errors->has('other_applicable_law_reg') ? 'has-error' : '' }}">
                                <label for="other_applicable_law_reg">Other Applicable Laws Reg*</label>
                                <input type="text" id="other_applicable_law_reg" name="other_applicable_law_reg" class="form-control" value="{{ old('other_applicable_law_reg', isset($user) ? $user->details : '') }}" placeholder="Enter Other Applicable Laws Reg*" required>
                                @if($errors->has('other_applicable_law_reg'))
                                    <p class="help-block">
                                        {{ $errors->first('other_applicable_law_reg') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            <div class="form-group {{ $errors->has('authorized_capital') ? 'has-error' : '' }}">
                                <label for="authorized_capital">Authorized Capital*</label>
                                <input type="text" id="authorized_capital" name="authorized_capital" class="form-control" value="{{ old('authorized_capital', isset($user) ? $user->select_month_year : '') }}" placeholder="Enter Authorized Capital*" required>
                                @if($errors->has('authorized_capital'))
                                    <p class="help-block">
                                        {{ $errors->first('authorized_capital') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            <div class="form-group {{ $errors->has('paid_up_capital') ? 'has-error' : '' }}">
                                <label for="paid_up_capital">Paid up Capital*</label>
                                <input type="text" id="paid_up_capital" name="paid_up_capital" class="form-control" value="{{ old('paid_up_capital', isset($user) ? $user->due_date : '') }}" placeholder="Enter Paid up Capital*" required>
                                @if($errors->has('paid_up_capital'))
                                    <p class="help-block">
                                        {{ $errors->first('paid_up_capital') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            <div class="form-group {{ $errors->has('reg_office') ? 'has-error' : '' }}">
                                <label for="reg_office">Registered Office*</label>
                                <input type="text" id="reg_office" name="reg_office" class="form-control" value="{{ old('reg_office', isset($user) ? $user->reg_office : '') }}" placeholder="Enter Registered Office*" required>
                                @if($errors->has('reg_office'))
                                    <p class="help-block">
                                        {{ $errors->first('reg_office') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            <div class="form-group {{ $errors->has('objects') ? 'has-error' : '' }}">
                                <label for="objects">Objects*</label>
                                <input type="text" id="objects" name="objects" class="form-control" value="{{ old('objects', isset($user) ? $user->objects : '') }}" placeholder="Enter Objects*" required>
                                @if($errors->has('objects'))
                                    <p class="help-block">
                                        {{ $errors->first('objects') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                <label for="email">Email*</label>
                                <input type="text" id="email" name="email" class="form-control" value="{{ old('email', isset($user) ? $user->email : '') }}" placeholder="Enter Email*" required>
                                @if($errors->has('email'))
                                    <p class="help-block">
                                        {{ $errors->first('email') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            <div class="form-group {{ $errors->has('telephone') ? 'has-error' : '' }}">
                                <label for="telephone">Telephone*</label>
                                <input type="text" id="telephone" name="telephone" class="form-control" value="{{ old('telephone', isset($user) ? $user->telephone : '') }}" placeholder="Enter Telephone*" required>
                                @if($errors->has('telephone'))
                                    <p class="help-block">
                                        {{ $errors->first('telephone') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>

                            <div class="form-group {{ $errors->has('director_details') ? 'has-error' : '' }}">
                                <label for="director_details">Director Details*</label>
                                <input type="text" id="director_details" name="director_details" class="form-control" value="{{ old('director_details', isset($user) ? $user->director_details : '') }}" placeholder="Enter Director Details*" required>
                                @if($errors->has('director_details'))
                                    <p class="help-block">
                                        {{ $errors->first('director_details') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            <div class="form-group {{ $errors->has('shareholder_details') ? 'has-error' : '' }}">
                                <label for="shareholder_details">Shareholder Details*</label>
                                <input type="text" id="shareholder_details" name="shareholder_details" class="form-control" value="{{ old('shareholder_details', isset($user) ? $user->shareholder_details : '') }}" placeholder="Enter Shareholder Details*" required>
                                @if($errors->has('shareholder_details'))
                                    <p class="help-block">
                                        {{ $errors->first('shareholder_details') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            <div class="form-group {{ $errors->has('certificate_incorporation') ? 'has-error' : '' }}">

                                <label for="certificate_incorporation">Certificate of Incorporation/Partnership date*</label>
                                <input id="certificate_incorporation" type="file" value="" class="form-control @error('certificate_incorporation') is-invalid @enderror" name="certificate_incorporation" >
                                @if($errors->has('certificate_incorporation'))
                                    <p class="help-block">
                                        {{ $errors->first('certificate_incorporation') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.email_helper') }}
                                </p>

                            </div>
                            <div class="form-group {{ $errors->has('aoa') ? 'has-error' : '' }}">

                                <label for="aoa">AOA*</label>
                                <input id="aoa" type="file" value="" class="form-control @error('aoa') is-invalid @enderror" name="aoa" >
                                @if($errors->has('aoa'))
                                    <p class="help-block">
                                        {{ $errors->first('aoa') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.email_helper') }}
                                </p>

                            </div>
                            <div class="form-group {{ $errors->has('moa') ? 'has-error' : '' }}">

                                <label for="moa">MOA*</label>
                                <input id="moa" type="file" value="" class="form-control @error('moa') is-invalid @enderror" name="moa" >
                                @if($errors->has('moa'))
                                    <p class="help-block">
                                        {{ $errors->first('moa') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.email_helper') }}
                                </p>

                            </div>
                            <div class="form-group {{ $errors->has('cin') ? 'has-error' : '' }}">

                                <label for="cin">CIN*</label>
                                <input id="cin" type="file" value="" class="form-control @error('cin') is-invalid @enderror" name="cin" >
                                @if($errors->has('cin'))
                                    <p class="help-block">
                                        {{ $errors->first('cin') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.email_helper') }}
                                </p>

                            </div>
                            {{---------------}}

                            <div>
                                <button class="btn btn-success" type="submit" value="{{ trans('global.save') }}">{{ trans('global.save') }}</button>
                            </div>
                        </form>


                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
