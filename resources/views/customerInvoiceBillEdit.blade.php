@extends('layouts.user')
@section('content')
    <div class="content">

        <div class="row justify-content-center">
            <div class="col-lg-8 col-sm-9 col-md-9">

                <div class="panel panel-default">
                    @if (session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="panel-heading h3 p-2 bg-light font-weight-bold border breadcrumb">
                        Customers Invoices or bill of supply Data File
                    </div>
                    <div class="panel-body p-2">

                        <form action="{{ route("customerInvoiceBillUpdate", $customerInvoiceBill->id ?? '') }}" method="POST" enctype="multipart/form-data">
                            @csrf


                            <div class="form-group {{ $errors->has('customer_invoice_bill') ? 'has-error' : '' }}">

                                <label for="customer_invoice_bill">Customer Invoice Bill*</label>
                                <input id="customer_invoice_bill" type="file" value="" class="form-control @error('customer_invoice_bill') is-invalid @enderror" name="customer_invoice_bill" >
                                @if($errors->has('customer_invoice_bill'))
                                    <p class="help-block">
                                        {{ $errors->first('customer_invoice_bill') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.email_helper') }}
                                </p>
                                <p class="text-danger">Rename_Name_Dateofupload*</p>

                            </div>

                            {{---------------}}

                            <div>
                                <button class="btn btn-success" type="submit" value="{{ trans('global.save') }}">{{ trans('global.save') }}</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
