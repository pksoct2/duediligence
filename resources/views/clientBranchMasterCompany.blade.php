@extends('layouts.user')
@section('content')
    <div class="content">

        <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-default">
                    <div class="panel-heading p-2 font-weight-bold breadcrumb h3">
                        <h3 class="font-weight-bold text-danger">Client Branch Master Company Documents</h3>
                    </div>
                    <div class="panel-body">

                        <div class="form-group">

                            <table class="table table-bordered table-striped">
                                <tbody>

                                <tr>
                                    <th>
                                        Parent Company Name
                                    </th>
                                    <td>
                                        {{ $masterBranch->parent_company_name }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.name') }}
                                    </th>
                                    <td>
                                        {{ $masterBranch->branch_name }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.pan_income_tax_img') }}
                                    </th>
                                    <td>
                                        {{ $masterBranch->pan_income_tax }}
                                    </td>
                                    <td>
                                        <button type="button" data-src="/branchesDocs/{{ $masterBranch->pan_income_tax_img }}" class="btn btn-outline-primary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                        <a class="btn btn-outline-success" href="/branchesDocs/{{ $masterBranch->pan_income_tax_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.tan_traces_img') }}
                                    </th>
                                    <td>
                                        {{ $masterBranch->tan_traces }}
                                    </td>
                                    <td>
                                        <button type="button" data-src="/branchesDocs/{{ $masterBranch->tan_traces_img}}" class="btn btn-outline-primary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                        <a class="btn btn-outline-success" href="/branchesDocs/{{ $masterBranch->tan_traces_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                    </td>
                                </tr>

                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.certificate_of_incorp_img') }}
                                    </th>
                                    <td>
                                        {{ $masterBranch->certificate_of_incorp }}
                                    </td>
                                    <td>
                                        <button type="button"  data-src="/branchesDocs/{{ $masterBranch->certificate_of_incorp_img}}" class="btn btn-outline-primary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                        <a class="btn btn-outline-success" href="/branchesDocs/{{ $masterBranch->certificate_of_incorp_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.memo_of_association_img') }}
                                    </th>
                                    <td>
                                        {{ $masterBranch->memo_of_association }}
                                    </td>
                                    <td>
                                        <button type="button" data-src="/branchesDocs/{{ $masterBranch->memo_of_association_img}}" class="btn btn-outline-primary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                        <a class="btn btn-outline-success" href="/branchesDocs/{{ $masterBranch->memo_of_association_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.articles_of_association_img') }}
                                    </th>
                                    <td>
                                        {{ $masterBranch->articles_of_association }}
                                    </td>
                                    <td>
                                        <button type="button" data-src="/branchesDocs/{{ $masterBranch->articles_of_association_img}}" class="btn btn-outline-primary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                        <a class="btn btn-outline-success" href="/branchesDocs/{{ $masterBranch->articles_of_association_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.gst_tax_cert_img') }}
                                    </th>
                                    <td>
                                        {{ $masterBranch->gst_tax_cert }}
                                    </td>
                                    <td>
                                        <button type="button" data-src="/branchesDocs/{{ $masterBranch->gst_tax_cert_img}}" class="btn btn-outline-primary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                        <a class="btn btn-outline-success" href="/branchesDocs/{{ $masterBranch->gst_tax_cert_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.lut_cert_img') }}
                                    </th>
                                    <td>
                                        {{ $masterBranch->lut_cert }}
                                    </td>
                                    <td>
                                        <button type="button" data-src="/branchesDocs/{{ $masterBranch->lut_cert_img}}" class="btn btn-outline-primary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                        <a class="btn btn-outline-success" href="/branchesDocs/{{ $masterBranch->lut_cert_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.gst_e_com_cert_img') }}
                                    </th>
                                    <td>
                                        {{ $masterBranch->gst_e_com_cert }}
                                    </td>
                                    <td>
                                        <button type="button" data-src="/branchesDocs/{{ $masterBranch->gst_e_com_cert_img}}" class="btn btn-outline-primary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                        <a class="btn btn-outline-success" href="/branchesDocs/{{ $masterBranch->gst_e_com_cert_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.prof_tax_enrollment_cert_img') }}
                                    </th>
                                    <td>
                                        {{ $masterBranch->prof_tax_enrollment_cert }}
                                    </td>
                                    <td>
                                        <button type="button" data-src="/branchesDocs/{{ $masterBranch->prof_tax_enrollment_cert_img}}" class="btn btn-outline-primary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                        <a class="btn btn-outline-success" href="/branchesDocs/{{ $masterBranch->prof_tax_enrollment_cert_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.prof_tax_register_cert_img') }}
                                    </th>
                                    <td>
                                        {{ $masterBranch->prof_tax_register_cert }}
                                    </td>
                                    <td>
                                        <button type="button" data-src="/branchesDocs/{{ $masterBranch->prof_tax_register_cert_img}}" class="btn btn-outline-primary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                        <a class="btn btn-outline-success" href="/branchesDocs/{{ $masterBranch->prof_tax_register_cert_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                    </td>
                                </tr>

                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.pf_establish_cert_img') }}
                                    </th>
                                    <td>
                                        {{ $masterBranch->pf_establish_cert }}
                                    </td>
                                    <td>
                                        <button type="button" data-src="/branchesDocs/{{ $masterBranch->pf_establish_cert_img}}" class="btn btn-outline-primary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                        <a class="btn btn-outline-success" href="/branchesDocs/{{ $masterBranch->pf_establish_cert_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.esic_register_cert_img') }}
                                    </th>
                                    <td>
                                        {{ $masterBranch->esic_register_cert }}
                                    </td>
                                    <td>
                                        <button type="button" data-src="/branchesDocs/{{ $masterBranch->esic_register_cert_img}}" class="btn btn-outline-primary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                        <a class="btn btn-outline-success" href="/branchesDocs/{{ $masterBranch->esic_register_cert_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.shops_establish_cert_trade_licence_img') }}
                                    </th>
                                    <td>
                                        {{ $masterBranch->shops_establish_cert_trade_licence }}
                                    </td>
                                    <td>
                                        <button type="button" data-src="/branchesDocs/{{ $masterBranch->shops_establish_cert_trade_licence_img}}" class="btn btn-outline-primary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                        <a class="btn btn-outline-success" href="/branchesDocs/{{ $masterBranch->shops_establish_cert_trade_licence_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.iec_img') }}
                                    </th>
                                    <td>
                                        {{ $masterBranch->iec }}
                                    </td>
                                    <td>
                                        <button type="button" data-src="/branchesDocs/{{ $masterBranch->iec_img}}" class="btn btn-outline-primary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                        <a class="btn btn-outline-success" href="/branchesDocs/{{ $masterBranch->iec_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.msmed_cert_img') }}
                                    </th>
                                    <td>
                                        {{ $masterBranch->msmed_cert }}
                                    </td>
                                    <td>
                                        <button type="button" data-src="/branchesDocs/{{ $masterBranch->msmed_cert_img}}" class="btn btn-outline-primary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                        <a class="btn btn-outline-success" href="/branchesDocs/{{ $masterBranch->msmed_cert_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.startup_cert_img') }}
                                    </th>
                                    <td>
                                        {{ $masterBranch->startup_cert }}
                                    </td>
                                    <td>
                                        <button type="button" data-src="/branchesDocs/{{ $masterBranch->startup_cert_img}}" class="btn btn-outline-primary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                        <a class="btn btn-outline-success" href="/branchesDocs/{{ $masterBranch->startup_cert_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.lower_tax_deduction_cert_img') }}
                                    </th>
                                    <td>
                                        {{ $masterBranch->lower_tax_deduction_cert }}
                                    </td>
                                    <td>
                                        <button type="button" data-src="/branchesDocs/{{ $masterBranch->lower_tax_deduction_cert_img}}" class="btn btn-outline-primary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                        <a class="btn btn-outline-success" href="/branchesDocs/{{ $masterBranch->lower_tax_deduction_cert_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.tax_exemption_cert_us12a_img') }}
                                    </th>
                                    <td>
                                        {{ $masterBranch->tax_exemption_cert_us12a }}
                                    </td>
                                    <td>
                                        <button type="button" data-src="/branchesDocs/{{ $masterBranch->tax_exemption_cert_us12a_img}}" class="btn btn-outline-primary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                        <a class="btn btn-outline-success" href="/branchesDocs/{{ $masterBranch->tax_exemption_cert_us12a_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.tax_exemption_cert_us80g_img') }}
                                    </th>
                                    <td>
                                        {{ $masterBranch->tax_exemption_cert_us80g }}
                                    </td>
                                    <td>
                                        <button type="button" data-src="/branchesDocs/{{ $masterBranch->tax_exemption_cert_us80g_img}}" class="btn btn-outline-primary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                        <a class="btn btn-outline-success" href="/branchesDocs/{{ $masterBranch->tax_exemption_cert_us80g_img}}" download><i class="fa-fw fas fa-download"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.any_other_details') }}
                                    </th>
                                    <td>
                                        {{ $masterBranch->any_other_details_etc }}
                                    </td>
                                    <td>
                                        <button type="button" data-src="/branchesDocs/{{ $masterBranch->any_other_details}}" class="btn btn-outline-primary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                        <a class="btn btn-outline-success" href="/branchesDocs/{{ $masterBranch->any_other_details}}" download><i class="fa-fw fas fa-download"></i></a>
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                            <a style="margin-top:20px;" class="btn btn-outline-info" href="{{ url()->previous() }}">
                                {{ trans('global.back_to_list') }}
                            </a>
                            {{--                                <a style="margin-top:20px;" class="btn btn-default" href="{{ url()->previous() }}">--}}
                            {{--                                    {{ trans('global.back_to_list') }}--}}
                            {{--                                </a>--}}
                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="modal fade bd-example1-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold text-capitalize text-center" id="exampleModalLabel">User Recent Image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <a id="myAnchor" href="" onclick="openwindow()" download>
                        <img src="" class="newmodalImageAppend img-fluid" height="550px" width="550px">
                    </a>
                </div>
            </div>
        </div>
    </div>

    <script !src="">
        $(".bd-example-modal-lg").on('click', function(e) {
            var self = $(this);
            var imagePath = self.attr("data-src");
            var modal = $(".bd-example1-modal-lg");
            var imageElm = $(".newmodalImageAppend");
            var imgdowld = $(".newmodalImageAppend");

            imageElm.attr('src', imagePath);
            modal.modal('show');

        });

    </script>
    <script>
        function openwindow() {
            var imagePath = self.attr("data-src");
            document.getElementById("myAnchor").href = self.attr("data-src");
            document.getElementById("demo").innerHTML = "The link above now goes to www.cnn.com.";
        }
    </script>
@endsection
