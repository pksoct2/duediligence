<style>
    table.dataTable tbody th, table.dataTable tbody td {
        padding: 0px 10px;
    }
</style>
@extends('layouts.user')
@section('content')
    <?php $rocReturnView = 'rocReturnView';?>
    <div class="content">

        <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-default">
                    <div class="panel-heading breadcrumb p-2 font-weight-bold h2 mb-5">
                        ROC Return Data List
                    </div>
                    <div class="panel-body">

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover datatable datatable-User">
                                <thead>
                                <tr>
                                    <th width="8">

                                    </th>

                                    <th>
                                        Company Name
                                    </th>
                                    <th>
                                        AOC4
                                    </th>
                                    <th>
                                        MGT7
                                    </th>
                                    <th>
                                        SRN
                                    </th>
                                    <th>
                                        Date of Filing
                                    </th>

                                    <th>
                                        Edit
                                    </th>


                                </tr>
                                </thead>
                                <tbody>
                                @php //print_r($clientBranch->branch_id); @endphp
                                @foreach($rocReturn as $key => $userData)
                                    <tr data-entry-id="">
                                        <td class="">

                                        </td>

                                        <td>
                                            {{ $userData->company_name ?? '' }}
                                        </td>
                                        <td>
                                            {{ $userData->aoc4 ?? '' }}
                                        </td>
                                        <td>
                                            {{ $userData->mgt7 ?? '' }}
                                        </td>
                                        <td>
                                            {{ $userData->srn ?? '' }}
                                        </td>
                                        <td>
                                            {{ $userData->date_filing ?? '' }}
                                        </td>

                                        <td class="d-flex flex-row bd-highlight">
                                            <a class="btn btn-outline-primary" href="{{route('editRocReturn', $userData->id)}}">
                                                <i class="fa-fw fas fa-edit"></i>
                                            </a>
                                        </td>
                                        {{--                                        <td>--}}
                                        {{--                                            <a class="btn btn-outline-danger" href="{{route('accountDataDelete', $userData->id)}}">--}}
                                        {{--                                                <i class="fa-fw fas fa-trash-alt"></i>--}}
                                        {{--                                            </a>--}}
                                        {{--                                        </td>--}}

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent

    <script>
        $(function () {
            let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
                @can('user_delete')
            let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
            let deleteButton = {
                text: deleteButtonTrans,
                url: "{{ route('destroyUser') }}",
                className: 'btn-danger',
                action: function (e, dt, node, config) {
                    var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
                        return $(entry).data('entry-id')
                    });

                    if (ids.length === 0) {
                        alert('{{ trans('global.datatables.zero_selected') }}')

                        return
                    }

                    if (confirm('{{ trans('global.areYouSure') }}')) {
                        $.ajax({
                            headers: {'x-csrf-token': _token},
                            method: 'POST',
                            url: config.url,
                            data: { ids: ids, _method: 'DELETE' }})
                            .done(function () { location.reload() })
                    }
                }
            }
            dtButtons.push(deleteButton)
            @endcan

            $.extend(true, $.fn.dataTable.defaults, {
                order: [[ 1, 'desc' ]],
                pageLength: 100,
            });
            $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
                $($.fn.dataTable.tables(true)).DataTable()
                    .columns.adjust();
            });
        })

    </script>
@endsection
