@extends('layouts.user')
@section('content')
    <?php $ROCCompliance = 'ROCCompliance';?>
    <div class="content">

        <div class="row justify-content-center">
            <div class="col-lg-8 col-sm-9 col-md-9">

                <div class="panel panel-default">
                    @if (session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="panel-heading h3 p-2 bg-light font-weight-bold border breadcrumb">
                        ROC Compliances
                    </div>
                    <div class="panel-body p-2">

                        <form action="{{ route("rocComplianceSubmit") }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group {{ $errors->has('type_meeting') ? 'has-error' : '' }}">
                                <label for="type_meeting">Types of Meeting*</label>
                                <input type="text" id="type_meeting" name="type_meeting" class="form-control" value="{{ old('type_meeting', isset($user) ? $user->type_meeting : '') }}" placeholder="Enter Types of Meeting*" required>
                                @if($errors->has('type_meeting'))
                                    <p class="help-block">
                                        {{ $errors->first('type_meeting') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>

                            <div class="form-group {{ $errors->has('date_meeting') ? 'has-error' : '' }}">
                                <label for="date_meeting">Date of Meeting*</label>
                                <input type="date" id="date_meeting" name="date_meeting" class="form-control" value="{{ old('date_meeting', isset($user) ? $user->date_meeting : '') }}" placeholder="Enter Date of Meeting*" required>
                                @if($errors->has('date_meeting'))
                                    <p class="help-block">
                                        {{ $errors->first('date_meeting') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.email_helper') }}
                                </p>
                            </div>

                            <div class="form-group {{ $errors->has('agenda') ? 'has-error' : '' }}">
                                <label for="agenda">Agenda*</label>
                                <input type="text" id="agenda" name="agenda" class="form-control" value="{{ old('agenda', isset($user) ? $user->agenda : '') }}" placeholder="Enter Agenda*" required>
                                @if($errors->has('agenda'))
                                    <p class="help-block">
                                        {{ $errors->first('agenda') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            <div class="form-group {{ $errors->has('attendance') ? 'has-error' : '' }}">
                                <label for="attendance">Attendance*</label>
                                <input type="text" id="attendance" name="attendance" class="form-control" value="{{ old('attendance', isset($user) ? $user->attendance : '') }}" placeholder="Enter Attendance" required>
                                @if($errors->has('attendance'))
                                    <p class="help-block">
                                        {{ $errors->first('attendance') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            <div class="form-group {{ $errors->has('remarks') ? 'has-error' : '' }}">
                                <label for="remarks">Remarks*</label>
                                <input type="text" id="remarks" name="remarks" class="form-control" value="{{ old('remarks', isset($user) ? $user->remarks : '') }}" placeholder="Enter Remarks*" required>
                                @if($errors->has('remarks'))
                                    <p class="help-block">
                                        {{ $errors->first('remarks') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            <div>
                                <button class="btn btn-success" type="submit" value="{{ trans('global.save') }}">{{ trans('global.save') }}</button>
                            </div>
                        </form>


                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
