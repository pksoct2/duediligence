@extends('layouts.user')
@section('content')
    <?php $gstRegistration = 'gstRegistrations';?>
    <div class="content">

        <div class="row justify-content-center">
            <div class="col-lg-8 col-sm-9 col-md-9">

                <div class="panel panel-default">
                    @if (session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="panel-heading h3 p-2 bg-light font-weight-bold border breadcrumb">
                        Client List of GST Registrations File
                    </div>
                    <div class="panel-body p-2">

                        <form action="{{ route("gstRegistrationSubmit") }}" method="POST" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group {{ $errors->has('year_month') ? 'has-error' : '' }}">
                                <label for="year_month">Month/Year*</label>
                                <input type="month" id="year_month" name="year_month" class="form-control" value="{{ old('year_month', isset($user) ? $user->year_month : '') }}" required>
                                @if($errors->has('year_month'))
                                    <p class="help-block">
                                        {{ $errors->first('year_month') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>

                            <div class="form-group {{ $errors->has('gst_registration') ? 'has-error' : '' }}">

                                <label for="gst_registration">List of GST Registrations*</label>
                                <input id="gst_registration" type="file" value="" class="form-control @error('gst_registration') is-invalid @enderror" name="gst_registration" >
                                @if($errors->has('gst_registration'))
                                    <p class="help-block">
                                        {{ $errors->first('gst_registration') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.email_helper') }}
                                </p>
                                <p class="text-danger">Rename File as LUT_Dateofupload*</p>

                            </div>

                            {{---------------}}

                            <div>
                                <button class="btn btn-success" type="submit" value="{{ trans('global.save') }}">{{ trans('global.save') }}</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
