@extends('layouts.user')
@section('content')
    <div class="content">

        <div class="row justify-content-center">
            <div class="col-lg-8 col-sm-9 col-md-9">

                <div class="panel panel-default">
                    @if (session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="panel-heading h3 p-2 bg-light font-weight-bold border breadcrumb">
                        Client Update GSTR1 Data
                    </div>
                    <div class="panel-body p-2">

                        <form action="{{ route('updateGstr1File',$updateGstr1->id ?? '') }}" method="POST" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group {{ $errors->has('year_month') ? 'has-error' : '' }}">
                                <label for="year_month">Month/Year*</label>
                                <input type="month" id="year_month" name="year_month" class="form-control" value="{{ old('year_month', isset($updateGstr1) ? $updateGstr1->year_month : '') }}" required>
                                @if($errors->has('year_month'))
                                    <p class="help-block">
                                        {{ $errors->first('year_month') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            <div class="form-group {{ $errors->has('gstr1') ? 'has-error' : '' }}">

                                <label for="gstr1">GSTR!*</label>
                                <input id="gstr1" type="file" name="gstr1"  value="{{ old('gstr1', isset($updateGstr1) ? $updateGstr1->gstr1 : '') }}" class="form-control @error('gstr1') is-invalid @enderror" >
                                @if($errors->has('gstr1'))
                                    <p class="help-block">
                                        {{ $errors->first('gstr1') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.email_helper') }}
                                </p>

                            </div>

                            {{---------------}}

                            <div>
                                <button class="btn btn-success" type="submit" value="{{ trans('global.save') }}">{{ trans('global.save') }}</button>
                            </div>
                        </form>


                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
