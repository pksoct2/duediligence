<style>
    table.dataTable tbody th, table.dataTable tbody td {
        padding: 0px 10px;
    }
</style>
@extends('layouts.user')
@section('content')
    <div class="content">

        <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-default">
                    <div class="panel-heading breadcrumb p-2 font-weight-bold h2 mb-5">
                       Client Branch List
                    </div>
                    <div class="panel-body">

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover datatable datatable-User">
                                <thead>
                                <tr>
                                    <th width="8">

                                    </th>
                                    <th>
                                        {{ trans('cruds.user.fields.id') }}
                                    </th>
                                    <th>
                                        Parents Company
                                    </th>
                                    <th>
                                        Branch Name
                                    </th>
                                    <th>
                                        {{ trans('cruds.user.fields.email') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.user.fields.mobile') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.user.fields.bank_name') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.user.fields.bank_account') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.user.fields.date_of_incorp') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.user.fields.org_address') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.user.fields.org_website') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.user.fields.spoc_dev') }}
                                    </th>

                                    <th>
                                        Branch Master Company
                                    </th>

                                </tr>
                                </thead>
                                <tbody>
                                @php //print_r($clientBranch->branch_id); @endphp
                                @foreach($clientBranch as $key => $user)
                                    <tr data-entry-id="">
                                        <td class="">

                                        </td>
                                        <td>
                                            {{ $user->branch_id ?? '' }}
                                        </td>
                                        <td>
                                            {{ $user->parent_company_name ?? '' }}
                                        </td>
                                        <td>
                                            {{ $user->branch_name ?? '' }}
                                        </td>
                                        <td>
                                            {{ $user->email ?? '' }}
                                        </td>
                                        <td>
                                            {{ $user->mobile ?? '' }}
                                        </td>
                                        <td>
                                            {{ $user->bank_name ?? '' }}
                                        </td>
                                        <td>
                                            {{ $user->bank_account ?? '' }}
                                        </td>
                                        <td>
                                            {{ $user->date_of_incorp ?? '' }}
                                        </td>
                                        <td>
                                            {{ $user->org_address ?? '' }}
                                        </td>
                                        <td>
                                            {{ $user->org_website ?? '' }}
                                        </td>
                                        <td>
                                            {{ $user->spoc_dev ?? '' }}
                                        </td>

                                        <td class="d-flex flex-row bd-highlight mb-3">
{{--                                            <a class="btn btn-primary btn-sm m-1" href="{{ route('masterCompanyUpload') }}">--}}
{{--                                                <i class="fa-fw fas fa-upload"></i>--}}
{{--                                            </a>--}}
                                            <a class="btn btn-outline-primary" href="{{route('clientBranchMasterCompany', $user->parent_company_name )}}">
                                                <i class="fa-fw fas fa-eye"></i>
                                            </a>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
@section('scripts')
    @parent
    <script>
        $(function () {
            let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
                @can('user_delete')
            let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
            let deleteButton = {
                text: deleteButtonTrans,
                url: "{{ route('destroyUser') }}",
                className: 'btn-danger',
                action: function (e, dt, node, config) {
                    var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
                        return $(entry).data('entry-id')
                    });

                    if (ids.length === 0) {
                        alert('{{ trans('global.datatables.zero_selected') }}')

                        return
                    }

                    if (confirm('{{ trans('global.areYouSure') }}')) {
                        $.ajax({
                            headers: {'x-csrf-token': _token},
                            method: 'POST',
                            url: config.url,
                            data: { ids: ids, _method: 'DELETE' }})
                            .done(function () { location.reload() })
                    }
                }
            }
            dtButtons.push(deleteButton)
            @endcan

            $.extend(true, $.fn.dataTable.defaults, {
                order: [[ 1, 'desc' ]],
                pageLength: 100,
            });
            $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
                $($.fn.dataTable.tables(true)).DataTable()
                    .columns.adjust();
            });
        })

    </script>
@endsection
