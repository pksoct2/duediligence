@extends('layouts.app')

@section('content')

    <section style="background-color: #1c5c92">
        <div class="container pb-4">
            <div class="row justify-content-start pt-3 pb-2">
                <img src="assets/logo.png" class="img-fluid" style="height:100px;"  alt="">
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <h4 class="text-light pt-2 pb-2">HOW THIS WORKS - </h4>
                    <p class="text-light">1. Preliminary details and screening –
                        The Investee Company shares the preliminary details as per the questionnaire which is followed by an initial evaluation by Dev Mantra. This is shared with the proposed Fund House/Investors;
                    </p>
                    <p class="text-light">2. Sharing details –
                        The time allotted to the Investee Company to share the details is exactly 15 days. On X+15 days the system blocks the Input fields. Subject Experts from Dev Mantra shall review the entire submission and shall update comments on the discrepancies. The Draft Report is published on X +20 days. Any non-submission of data/information is treated as a Non-Compliance. Base documents shall be shared.
                    </p>
                    <p class="text-light">3. Discussion –
                        All discussions in the Interim shall be conducted on Google Video Meet and the recordings of the same shall be preserved for future reference. This includes discussions on business operations, pitch decks, historical matters and business projections;
                    </p>
                    <p class="text-light">4. Deal Structuring –
                        Dev Mantra drafts the various covenants of the Closing Agreements related to the outcome of the Due Diligence and reviews the same with the Deal Lead. As an option, Dev Mantra prepares the prospective Reporting Structures for the investment, the Milestones and the alerts are designed in sync with the Due Diligence Findings.
                    </p>
                </div>
                <div class="col-md-6">

                    @if (session('danger'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{ session('danger') }}
                        </div>
                    @endif
                    <div class="pl-5">
                        <h4 class="pl-5 ml-3 p-1 font-weight-bold text-uppercase text-white">Start Your Due Diligence</h4>

                    </div>
                    <div class="">

                        <form method="POST" action="{{ route('admin.login') }}" >
                            @csrf

                            <div class="form-group row justify-content-center p-1">

                                <div class="col-md-7">
                                    <input id="email" type="email" class="rounded p-4 form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Please Enter Admin Email" required autocomplete="email" autofocus>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row justify-content-center p-1">
                                <div class="col-md-7">
                                    <input id="password" type="password" class="rounded p-4 form-control @error('password') is-invalid @enderror" name="password" placeholder="Please Enter Admin Password" required autocomplete="current-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row justify-content-center">
                                <div class="col-md-7 pl-5">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label text-white" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                    @if (Route::has('remember.request'))
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0 justify-content-center">
                                <div class="col-md-3 ml-1">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block">
                                        {{ __('Login') }}
                                    </button>
                                </div>
                                <div class="col-sm-4 pl-4">
                                    <a class="btn btn-link text-white font-weight-bold btn-block btn-lg" href="{{route('login')}}">For Client Login</a>

                                </div>
                            </div>
                            <div class="form-group row mb-0 justify-content-center">
                                <div class="col-md-7 pl-5 pt-5">
                                    <img src="assets/duediligence.png" class="img-fluid" alt="">
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <footer class="bg-white pb-2 pt-2">
            <div class="container-fluid">
                <div class="row justify-content-center pt-4 pb-4">
                    <h6 class="font-weight-bold ">Email: info@devmantra.com</h6>
                </div>
                <div class="row justify-content-between">
                    <div class="col-sm-3 col-md-3 col-lg-3 pl-5 text-center">
                        <a href="#" class="p-2"><i class="fab fa-facebook-square fa-2x"></i></a>
                        <a href="#" class="p-2"><i class="fab fa-twitter-square fa-2x"></i></a>
                        <a href="#" class="p-2"><i class="fab fa-instagram fa-2x"></i></a>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <ul class="list-unstyled text-center">
                            <li>© 2020 Devmantra Financial Services Pvt. Ltd .</li>
                            <li>All Rights Reserved</li>
                            <li>CIN #: U65900KA2008PTC045458</li>
                        </ul>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3 text-center">
                        <h5 class="font-weight-bold">Mobile: +91 080 42061247</h5>
                    </div>
                </div>
            </div>
        </footer>
    </section>

@endsection
