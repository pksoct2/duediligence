@extends('layouts.user')
@section('content')
    <?php $accountFile = 'accountFile';?>
    <div class="content">

        <div class="row justify-content-center">
            <div class="col-lg-8 col-sm-9 col-md-9">

                <div class="panel panel-default">
                    @if (session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="panel-heading h3 p-2 bg-light font-weight-bold border breadcrumb">
                        Client Account File
                    </div>
                    <div class="panel-body p-2">

                        <form action="{{ route("accountFile") }}" method="POST" enctype="multipart/form-data">
                            @csrf



                            <div class="form-group {{ $errors->has('final_year') ? 'has-error' : '' }}">
                                <label for="final_year">Final Year*</label>
                                <input type="month" id="final_year" name="final_year" class="form-control" value="{{ old('final_year', isset($user) ? $user->final_year : '') }}" required>
                                @if($errors->has('final_year'))
                                    <p class="help-block">
                                        {{ $errors->first('final_year') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>

                            <div class="form-group {{ $errors->has('upload_field') ? 'has-error' : '' }}">

                                <label for="upload_field">upload Field*</label>
                                <input id="upload_field" type="file" value="" class="form-control @error('upload_field') is-invalid @enderror" name="upload_field" >
                                @if($errors->has('upload_field'))
                                    <p class="help-block">
                                        {{ $errors->first('upload_field') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.email_helper') }}
                                </p>

                            </div>

                            {{---------------}}

                            <div>
                                <button class="btn btn-success" type="submit" value="{{ trans('global.save') }}">{{ trans('global.save') }}</button>
                            </div>
                        </form>


                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
