@extends('layouts.user')
@section('content')
    <div class="content">

        <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-default">
                    <div class="panel-heading p-2 font-weight-bold breadcrumb h3">
                        {{ trans('global.show') }} {{ trans('cruds.user.title') }}
                    </div>
                    <div class="panel-body">

                        <div class="form-group">
                            <table class="table table-bordered table-striped">
                                <tbody>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.id') }}
                                    </th>
                                    <td>
                                        {{ $complianceTracker['id'] }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.name') }}
                                    </th>
                                    <td>
                                        {{ $complianceTracker['users_name'] }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.select_month_year') }}
                                    </th>
                                    <td>
                                        {{ $complianceTracker['select_month_year'] }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.due_date') }}
                                    </th>
                                    <td>
                                        {{ $complianceTracker['due_date'] }}
                                    </td>
                                </tr>

                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.category') }}
                                    </th>
                                    <td>
                                        {{ $complianceTracker['category'] }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.details') }}
                                    </th>
                                    <td>
                                        {{ $complianceTracker['details'] }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.compliance_date') }}
                                    </th>
                                    <td>
                                        {{ $complianceTracker['compliance_date'] }}
                                    </td>
                                </tr>

                                <tr>
                                    <th>
                                        {{ trans('cruds.user.fields.documents') }}
                                    </th>
                                    <td>
                                        <button type="button" data-src="/complianceTrackersimg/{{ $complianceTracker['documents'] }}" class="btn btn-outline-secondary bd-example-modal-lg" ><i class="fa-fw fas fa-eye"></i></button>
                                        <a class="btn btn-outline-secondary" href="/complianceTrackersimg/{{ $complianceTracker['documents']}}" download><i class="fa-fw fas fa-download"></i></a>
                                    </td>
                                </tr>

                                </tbody>
                            </table>

                            <a style="margin-top:20px;" class="btn btn-default" href="{{ url()->previous() }}">
                                {{ trans('global.back_to_list') }}
                            </a>
                            {{--                                <a style="margin-top:20px;" class="btn btn-default" href="{{ url()->previous() }}">--}}
                            {{--                                    {{ trans('global.back_to_list') }}--}}
                            {{--                                </a>--}}
                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="modal fade bd-example1-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold text-capitalize text-center" id="exampleModalLabel">User Recent Image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <a id="myAnchor" href="" onclick="openwindow()" download>
                        <img src="" class="newmodalImageAppend img-fluid" height="550px" width="550px">
                    </a>
                </div>
            </div>
        </div>
    </div>

    <script !src="">
        $(".bd-example-modal-lg").on('click', function(e) {
            var self = $(this);
            var imagePath = self.attr("data-src");
            var modal = $(".bd-example1-modal-lg");
            var imageElm = $(".newmodalImageAppend");

            imageElm.attr('src', imagePath);
            modal.modal('show');

        });

    </script>
    <script>
        function openwindow() {
            var imagePath = self.attr("data-src");
            document.getElementById("myAnchor").href = self.attr("data-src");
            document.getElementById("demo").innerHTML = "The link above now goes to www.cnn.com.";
        }
    </script>
@endsection
@section('scripts')
    @parent

@endsection
