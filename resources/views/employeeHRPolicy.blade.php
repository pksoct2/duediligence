@extends('layouts.user')
@section('content')
    <?php $employeeHR = 'employeeHRPolicy';?>
    <div class="content">

        <div class="row justify-content-center">
            <div class="col-lg-8 col-sm-9 col-md-9">

                <div class="panel panel-default">
                    @if (session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="panel-heading h3 p-2 bg-light font-weight-bold border breadcrumb">
                        Employee HR Policy Data File
                    </div>
                    <div class="panel-body p-2">

                        <form action="{{ route("employeeHRPolicySubmit") }}" method="POST" enctype="multipart/form-data">
                            @csrf


                            <div class="form-group {{ $errors->has('employee_h_r_policy') ? 'has-error' : '' }}">

                                <label for="employee_h_r_policy">Employee HR Policy*</label>
                                <input id="employee_h_r_policy" type="file" value="" class="form-control @error('employee_h_r_policy') is-invalid @enderror" name="employee_h_r_policy" >
                                @if($errors->has('employee_h_r_policy'))
                                    <p class="help-block">
                                        {{ $errors->first('employee_h_r_policy') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.email_helper') }}
                                </p>
                                <p class="text-danger">Rename_Name_Dateofupload*</p>

                            </div>

                            {{---------------}}

                            <div>
                                <button class="btn btn-success" type="submit" value="{{ trans('global.save') }}">{{ trans('global.save') }}</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
